﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PricingDomainModel
{
    [Serializable]
    public class ShippingProductionInfo
    {
        public string Doc_No { get; set; }
        public string Shipping_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Type { get; set; }
    }
    [Serializable]
    public class ShippingProductionMasterInfo
    {
        public string Doc_No { get; set; }
        public string Shipping_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Type { get; set; }
        public string TypeRB { get; set; }
        public decimal Total_Cost { get; set; }
        public decimal Total_Weight { get; set; }
        public decimal Average_Price { get; set; }
    }
    [Serializable]
    public class ShippingProductDetails
    {
        public string Company { get; set; }
        public string PackedGrade { get; set; }
        public string CaseNo { get; set; }
        public string Weight { get; set; }
        public string UnitCost { get; set; }
        public string Amount { get; set; }

    }

}