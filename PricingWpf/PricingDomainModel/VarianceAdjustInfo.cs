﻿using System;

namespace PricingDomainModel
{
    public class VarianceAdjustInfo
    {
        public int Id { get; set; }
        public int Crop { get; set; }
        public string PackedGrade { get; set; }
        public string Customer { get; set; }
        public string CustomerGrade { get; set; }
        public string PackingMat { get; set; }
        public decimal RSign { get; set; }
        public decimal RValue { get; set; }
        public string RRemark { get; set; }
        public decimal PSign { get; set; }
        public decimal PValue { get; set; }
        public string PRemark { get; set; }
        public decimal PackSign { get; set; }
        public decimal PackValue { get; set; }
        public string PackRemark { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    [Serializable]
    public class PackedGradeRedryInfo
    {
        public string MinPrice { get; set; }
        public string AvgRedry { get; set; }
        public string Weight { get; set; }
        public int Bales { get; set; }
        public string PackingMat { get; set; }
    }
    [Serializable]
    public class SignInfo
    {
        public string sign { get; set; }
    }
    [Serializable]
    public class VarianceSummaryInfo
    {
        public int Id { get; set; }
        public int Crop { get; set; }
        public string PackedGrade { get; set; }
        public string Customer { get; set; }
        public string CustomerGrade { get; set; }
        public string PackingMat { get; set; }
        public decimal RSumValue { get; set; }
        public decimal PSumValue { get; set; }
        public decimal PackSumValue { get; set; }
    }

}
