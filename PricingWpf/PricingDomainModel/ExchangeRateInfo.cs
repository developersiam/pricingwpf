﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PricingDomainModel
{
    public class ExchangeRateInfo
    {
        public DateTime EffecctiveDatetime { get; set; }
        public string EffectiveDate { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}