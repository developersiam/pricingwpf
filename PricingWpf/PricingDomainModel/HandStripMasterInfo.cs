﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PricingDomainModel
{
    [Serializable]
    public class HandStripMasterInfo
    {
        public string HandStrip_Doc_No { get; set; }
        public string HandStrip_No { get; set; }
        public int Crop { get; set; }
        public DateTime HandStrip_Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
    }

    [Serializable]
    public class MatIsHandStripInfo
    {
        public string HandStripNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string TotalWeight { get; set; }
        public string AveragePrice { get; set; }
        public string TotalAmount { get; set; }
    }

    [Serializable]
    public class HandStripDetails
    {
        public string BaleNo { get; set; }
        public string GreenGrade { get; set; }
        public string Classified { get; set; }
        public string Curer { get; set; }
        public string Weight { get; set; }
        public string UnitPrice { get; set; }
        public string Amount { get; set; }
        public string FlagIO { get; set; }

    }
}