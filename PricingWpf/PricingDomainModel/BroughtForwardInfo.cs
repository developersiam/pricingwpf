﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    public class BroughtForwardInfo
    {
        public string bfno { get; set; }
        public int fromcrop { get; set; }
        public int tocrop { get; set; }
        public bool locked { get; set; }
        public DateTime dtrecord { get; set; }
        public string company { get; set; }
        public string type { get; set; }
        public string season { get; set; }
        public int total_bales { get; set; }
    }

    [Serializable]
    public class OnhandMatInfo
    {
        public int onhand_crop { get; set; }
        public int onhand_bales { get; set; }
        public double onhand_weight { get; set; }
        public double onhand_amt { get; set; }
        public double onhand_amt_unit { get; set; }
    }
    [Serializable]
    public class BroughtFWMatInfo
    {
        public string bfno { get; set; }
        public string rcno { get; set; }
        public int fromcrop { get; set; }
        public int tocrop { get; set; }
        public string bc { get; set; }
        public string type { get; set; }
        public string subtype { get; set; }
        public string supplier { get; set; }
        public string company { get; set; }
        public string green { get; set; }
        public string classify { get; set; }
        public double weight { get; set; }
        public double weightbuy { get; set; }
        public double price { get; set; }
        public DateTime createdate { get; set; }
    }

}
