﻿using System;

namespace PricingDomainModel
{
    public class VarianceAdjustDetailsInfo
    {
        public int VarinaceId { get; set; }
        public string BC { get; set; }
        public string Type { get; set; }
        public int Crop { get; set; }
        public string PackedGrade { get; set; }
        public string Customer { get; set; }
        public string CustomerGrade { get; set; }
        public decimal RAdjustValue { get; set; }
        public decimal PAdjustValue { get; set; }
        public decimal PackedAdjustValue { get; set; }
        public decimal PackedOriginal { get; set; }
        public decimal RedryOriginal { get; set; }
        public decimal PackCostOriginal { get; set; }
        public decimal PackedAdj { get; set; }
        public decimal RedryAdj { get; set; }
        public decimal PackCostAdj { get; set; }
        public decimal PackedCostAdj { get; set; }
        public decimal Weight { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
