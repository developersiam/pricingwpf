﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PricingDomainModel
{
    public class PackingMatInfo
    {
        public int Crop { get; set; }
        public string Packing_Mat { get; set; }
        public decimal? Price { get; set; }
    }

    [Serializable]
    public class PackingInfo
    {
        public string Packing_Mat { get; set; }
    }
}