﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    public class vm_rptrc009
    {
        public string transfer_doc_no { get; set; }
        public DateTime doc_date { get; set; }
        public string transfer_no { get; set; }
        public string crop { get; set; }
        public string company { get; set; }
        public string type { get; set; }
        public string season { get; set; }
        public string green_grade { get; set; }
        public string classified_grade_from { get; set; }
        public string classified_grade { get; set; }
        public string barcode { get; set; }
        public string bale_no { get; set; }
        public decimal weight { get; set; }
        public decimal unit_price { get; set; }
        public decimal amount { get; set; }
    }
}
