﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    public class MatInfo
    {
        public string rcno { get; set; }
        public int crop { get; set; }
        public string type { get; set; }
        public string subtype { get; set; }
        public string company { get; set; }
        public string rcfrom { get; set; }
		public string bc { get; set; }
		public string supplier { get; set; }
		public string green { get; set; }
		public string classify { get; set; }
		public string docno { get; set; }
		public bool issued { get; set; }
		
	}
	
}
