﻿using System;

namespace PricingDomainModels
{
    [Serializable]
    public class ProcessesInfo
    {
        public string C_PROG_ID { get; set; }
        public string C_PROCESS_DOC_NO { get; set; }
        public string C_PROCESS_NO { get; set; }
        public string D_PROCESS_DOC_DATE { get; set; }
        public string N_CROP { get; set; }
        public string C_TYPE { get; set; }
        public string C_SEASON { get; set; }
        public string C_PACKED_GRADE { get; set; }
        public string C_PACK_MAT_CODE { get; set; }
        public string N_PACK_MAT_CHARGE { get; set; }
        public string C_TRAN_CHARGE_CODE { get; set; }
        public string N_TRAN_CHARGE { get; set; }
        public string C_REDRY_CHARGE_CODE { get; set; }
        public string N_REDRY_CHARGE { get; set; }
        public string N_CASE_WEIGHT { get; set; }
        public string N_PACKED_WEIGHT { get; set; }
        public string C_CR_BY { get; set; }
        public string D_CR_DATE { get; set; }
        public string C_UPD_BY { get; set; }
        public string D_UPD_DATE { get; set; }
    }
}