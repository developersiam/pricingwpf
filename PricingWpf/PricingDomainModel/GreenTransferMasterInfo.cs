﻿using System;

namespace PricingDomainModel
{
    [Serializable]
    public class GreenTransferMasterInfo
    {
        public string Transfer_Doc_No { get; set; }
        public string Transfer_No { get; set; }
        public int Crop { get; set; }
        public DateTime Transfer_Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
    }

    [Serializable]
    public class MatIsGreenTransferInfo
    {
        public string TransferNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string TotalWeight { get; set; }
        public string AveragePrice { get; set; }
        public string TotalAmount { get; set; }
    }

    [Serializable]
    public class GreenTransferDetails
    {
        public string BaleNo { get; set; }                            
        public string Supplier { get; set; }                                               
        public string GreenGrade { get; set; } 
        public string FromClassify { get; set; } 
        public string ToClassify { get; set; } 
        public string Weight { get; set; }  
        public string UnitPrice { get; set; }  
        public string Amount { get; set; } 

    }
}