﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    public class vm_rptrc010
    {
        public string input_regrade_doc_no { get; set; }
        public DateTime input_doc_date { get; set; }
        public string input_regrade_no { get; set; }
        public int input_crop { get; set; }
        public string input_company { get; set; }
        public string input_type { get; set; }
        public string input_season { get; set; }
        public string input_green_grade { get; set; }
        public string input_classified_grade { get; set; }
        public string input_barcode { get; set; }
        public string input_bale_no { get; set; }
        public decimal? input_weight { get; set; }
        public decimal? input_unit_price { get; set; }
        public decimal? input_amount { get; set; }
        public string output_barcode { get; set; }
        public string output_bale_no { get; set; }
        public decimal? output_weight { get; set; }
        public decimal? output_unit_price { get; set; }
        public decimal? output_amount { get; set; }
        public string flag_io { get; set; }
        public int Cinput_bales { get; set; }
        public int Coutput_bales { get; set; }
        public decimal Sinput_weight { get; set; }
        public decimal Soutput_weight { get; set; }
        public decimal Sinput_unitprice { get; set; }
        public decimal Soutput_unitprice { get; set; }
        public decimal Sinput_amount { get; set; }
        public decimal Soutput_amount { get; set; }

    }
}
