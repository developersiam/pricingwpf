﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    public class PricingUserInfo
    {
        public string Username { get; set; }
        public string RoleName { get; set; }
    }
}
