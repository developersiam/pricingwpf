﻿using System;

namespace PricingDomainModel
{
    public class vm_Variance_Packed
    {
        public int N_ID_ADJ { get; set; }
        public string C_BARCODE { get; set; }
        public string C_TYPE { get; set; }
        public int N_CROP { get; set; }
        public string C_PACKED_GRADE { get; set; }
        public string C_CUSTOMER_RS { get; set; }
        public string C_PACKED_GRADE_RS { get; set; }
        public decimal N_REDRY_ADJUST_VALUE { get; set; }
        public decimal N_PACKING_ADJUST_VALUE { get; set; }
        public decimal N_PACKED_ADJUST_VALUE { get; set; }
        public decimal N_PACKED_ORIGINAL_COST { get; set; }
        public decimal N_REDRY_ORIGINAL_COST { get; set; }
        public decimal N_PACKING_ORIGINAL_COST { get; set; }
        public decimal N_PACKED_ADJUST_COST { get; set; }
        public decimal N_REDRY_ADJUST_COST { get; set; }
        public decimal N_PACKING_ADJUST_COST { get; set; }
        public decimal N_WEIGHT { get; set; }
        public DateTime D_CR_DATE { get; set; }

    }
}
