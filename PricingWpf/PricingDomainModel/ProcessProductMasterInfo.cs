﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PricingDomainModel
{
    [Serializable]
    public class ProcessProductMasterInfo
    {
        public string Doc_No { get; set; }
        public string ProcessingP_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
    }
    [Serializable]
    public class PDProcessProductMasterInfo
    {
        public string Doc_No { get; set; }
        public string ProcessingR_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public decimal Total_Cost { get; set; }
        public decimal Total_Weight { get; set; }
        public decimal Average_Price { get; set; }
    }
    [Serializable]
    public class ProcessProductDetails
    {
        public string PackedGrade { get; set; }
        public string Weight { get; set; }
        public string UnitPrice { get; set; }
        public string Amount { get; set; }
        public string PackingMaterial { get; set; }
        public string Trans_Charge { get; set; }
        public string ToTalCost { get; set; }

    }
}