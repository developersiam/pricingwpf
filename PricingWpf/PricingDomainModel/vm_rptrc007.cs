﻿using System;

namespace PricingDomainModel
{
    public class vm_rptrc007
    {
        public string Doc_No { get; set; }
        public DateTime Receiving_Doc_Date { get; set; }
        public string Receiving_Doc_No { get; set; }
        public string Buying_Doc_No { get; set; }
        public int Receiving_Crop { get; set; }
        public string Receiving_Comp { get; set; }
        public string Receiving_Type { get; set; }
        public string Receiving_Season { get; set; }
        public string Receiving_Curer { get; set; }
        public string Receiving_Curer_Name { get; set; }
        public string Green_Grade { get; set; }
        public int Bales_no { get; set; }
        public string BuyWeight { get; set; }
        public string BuyUnitPrice { get; set; }
        public string BuyTransprotChrge { get; set; }
        public string BuyTotalUnitPrice { get; set; }
        public string RecWeight { get; set; }
        public string RecUnitPrice { get; set; }
        public string Amount { get; set; }
        public string Classify { get; set; }       
    }
}
