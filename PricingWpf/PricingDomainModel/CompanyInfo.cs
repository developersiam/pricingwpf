﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingDomainModel
{
    [Serializable]
    public class CompanyInfo
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string Dtrecord { get; set; }
        public string User { get; set; }
        public bool FromBuyingSystem { get; set; }
        public bool FromLoadBales { get; set; }
    }
}
