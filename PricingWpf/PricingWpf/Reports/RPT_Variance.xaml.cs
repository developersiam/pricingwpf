﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_Variance : Window
    {
        int _Id;
        public RPT_Variance(int varianceId)
        {
            InitializeComponent();
            _Id = varianceId;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                List<vm_Variance_Packed> rec_detail = new List<vm_Variance_Packed>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetVarianceDetails(_Id).ToList());

                if (rec_detail != null)
                {
                    List<vm_Variance_Packed> t = new List<vm_Variance_Packed>(rec_detail.Select(r => new vm_Variance_Packed
                    {
                        C_BARCODE = r.C_BARCODE,
                        C_TYPE = r.C_TYPE,
                        N_CROP = r.N_CROP,
                        C_PACKED_GRADE = r.C_PACKED_GRADE,
                        C_CUSTOMER_RS = r.C_CUSTOMER_RS,
                        C_PACKED_GRADE_RS = r.C_PACKED_GRADE_RS,
                        N_REDRY_ADJUST_VALUE = r.N_REDRY_ADJUST_VALUE,
                        N_PACKING_ADJUST_VALUE = r.N_PACKING_ADJUST_VALUE,
                        N_PACKED_ADJUST_VALUE = r.N_PACKED_ADJUST_VALUE,
                        N_PACKED_ORIGINAL_COST = r.N_PACKED_ORIGINAL_COST,
                        N_REDRY_ORIGINAL_COST = r.N_REDRY_ORIGINAL_COST,
                        N_PACKING_ORIGINAL_COST = r.N_PACKING_ORIGINAL_COST,
                        N_PACKED_ADJUST_COST = r.N_PACKED_ADJUST_COST,
                        N_REDRY_ADJUST_COST = r.N_REDRY_ADJUST_COST,
                        N_PACKING_ADJUST_COST = r.N_PACKING_ADJUST_COST,
                        N_WEIGHT = r.N_WEIGHT,
                        D_CR_DATE = r.D_CR_DATE
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_Variance_Packed";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rcVarAdj.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
