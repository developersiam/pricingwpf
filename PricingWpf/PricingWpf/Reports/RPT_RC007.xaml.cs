﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_RC007 : Window
    {
        string Curr_rcNo;
        public RPT_RC007(string rc_docno)
        {
            InitializeComponent();
            Curr_rcNo = rc_docno;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                //total_weight = 0;
                //total_price = 0;
                //total_price_supplier = 0;

                ////Get Truck and Buyer info
                //matrc driver_details = BussinessLayerService.matRcBL().GetMatRcInfoByRcNo(Curr_rcNo);


                ////Get all barcode for calculate
                List<vm_rptrc007> rec_detail = new List<vm_rptrc007>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetReceivingDetails(Curr_rcNo).ToList());

                if (rec_detail != null)
                {
                    List<vm_rptrc007> t = new List<vm_rptrc007>(rec_detail.Select(r => new vm_rptrc007
                    {
                        Doc_No = r.Doc_No ,
                        Receiving_Doc_Date = r.Receiving_Doc_Date,
                        Receiving_Doc_No = r.Receiving_Doc_No,
                        Buying_Doc_No = r.Buying_Doc_No,
                        Receiving_Crop = r.Receiving_Crop,
                        Receiving_Comp = r.Receiving_Comp,
                        Receiving_Type = r.Receiving_Type,
                        Receiving_Season = r.Receiving_Season,
                        Receiving_Curer = r.Receiving_Curer,
                        Receiving_Curer_Name = r.Receiving_Curer_Name,
                        Green_Grade = r.Green_Grade,
                        Bales_no = (int)r.Bales_no,
                        BuyWeight = r.BuyWeight,
                        BuyUnitPrice = r.BuyUnitPrice,
                        BuyTransprotChrge = r.BuyTransprotChrge,
                        BuyTotalUnitPrice = r.BuyTotalUnitPrice,
                        RecWeight = r.RecWeight,
                        RecUnitPrice = r.RecUnitPrice,
                        Amount = r.Amount,
                        Classify = r.Classify
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_rptrc007";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rc007.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
