﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_BroughtF : Window
    {
        string _Id;
        public RPT_BroughtF(string bfId)
        {
            InitializeComponent();
            _Id = bfId;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                List<BroughtFWMatInfo> rec_detail = new List<BroughtFWMatInfo>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetBroughtFWMatInfo(_Id).ToList());

                if (rec_detail != null)
                {
                    List<BroughtFWMatInfo> t = new List<BroughtFWMatInfo>(rec_detail.Select(r => new BroughtFWMatInfo
                    {
                        bc = r.bc,
                        rcno = r.rcno,
                        bfno = r.bfno,
                        fromcrop = r.fromcrop,
                        tocrop = r.tocrop,
                        type = r.type,
                        subtype = r.subtype,
                        company = r.company,
                        supplier = r.supplier,
                        green = r.green,
                        classify = r.classify,
                        weight = r.weight,
                        weightbuy = r.weightbuy,
                        price = r.price,
                        createdate = r.createdate
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_BF_Details"; 

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rcBF.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
