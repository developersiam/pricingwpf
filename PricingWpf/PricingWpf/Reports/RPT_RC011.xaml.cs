﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_RC011 : Window
    {
        string Curr_hsNo;
        public RPT_RC011(string hs_docno)
        {
            InitializeComponent();
            Curr_hsNo = hs_docno;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                List<vm_rptrc011> rec_detail = new List<vm_rptrc011>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetHandStripDetails(Curr_hsNo).ToList());

                if (rec_detail != null)
                {
                    List<vm_rptrc011> t = new List<vm_rptrc011>(rec_detail.Select(r => new vm_rptrc011
                    {
                        input_hand_doc_no = r.input_hand_doc_no,
                        input_doc_date = r.input_doc_date,
                        input_hand_no = r.input_hand_no,
                        input_crop = r.input_crop,
                        input_company = r.input_company,
                        input_type = r.input_type,
                        input_season = r.input_season,
                        input_green_grade = r.input_green_grade,
                        input_classified_grade = r.input_classified_grade,
                        input_barcode = r.input_barcode,
                        input_bale_no = r.input_bale_no,
                        input_weight = (r.input_weight == 0 ? 0 : Convert.ToDecimal(r.input_weight)),
                        input_unit_price = r.input_unit_price,
                        input_amount = r.input_amount,
                        output_barcode = r.output_barcode,
                        output_bale_no = r.output_bale_no,
                        output_weight = r.output_weight,
                        output_unit_price = r.output_unit_price,
                        output_amount = r.output_amount,
                        flag_io = r.flag_io
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_rptrc011";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rc011.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
