﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_RC009 : Window
    {
        string Curr_tfNo;
        public RPT_RC009(string tf_docno)
        {
            InitializeComponent();
            Curr_tfNo = tf_docno;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                List<vm_rptrc009> rec_detail = new List<vm_rptrc009>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetGreenTransferDetails(Curr_tfNo).ToList());

                if (rec_detail != null)
                {
                    List<vm_rptrc009> t = new List<vm_rptrc009>(rec_detail.Select(r => new vm_rptrc009
                    {
                        transfer_doc_no = r.transfer_doc_no,
                        doc_date = r.doc_date,
                        transfer_no = r.transfer_no,
                        crop = r.crop,
                        company = r.company,
                        type = r.type,
                        season = r.season,
                        green_grade = r.green_grade,
                        classified_grade_from = r.classified_grade_from,
                        classified_grade = r.classified_grade,
                        barcode = r.barcode,
                        bale_no = r.bale_no,
                        weight = r.weight,
                        unit_price = r.unit_price,
                        amount = r.amount
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_rptrc009";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rc009.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
