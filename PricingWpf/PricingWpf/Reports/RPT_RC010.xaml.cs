﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PricingBusinessLayer;
using PricingDomainModel;

namespace PricingWpf.Reports
{
    public partial class RPT_RC010 : Window
    {
        string Curr_rgNo;
        public RPT_RC010(string rg_docno)
        {
            InitializeComponent();
            Curr_rgNo = rg_docno;
            ReloadReport();
        }
        private void ReloadReport()
        {
            try
            {
                List<vm_rptrc010> rec_detail = new List<vm_rptrc010>();
                rec_detail.AddRange(BusinessLayerService.StoreProcedureBL().GetRegradeDetails(Curr_rgNo).ToList());

                if (rec_detail != null)
                {
                    List<vm_rptrc010> t = new List<vm_rptrc010>(rec_detail.Select(r => new vm_rptrc010
                    {
                        input_regrade_doc_no = r.input_regrade_doc_no,
                        input_doc_date = r.input_doc_date,
                        input_regrade_no = r.input_regrade_no,
                        input_crop = r.input_crop,
                        input_company = r.input_company,
                        input_type = r.input_type,
                        input_season = r.input_season,
                        input_green_grade = r.input_green_grade,
                        input_classified_grade = r.input_classified_grade,
                        input_barcode = r.input_barcode,
                        input_bale_no = r.input_bale_no,
                        input_weight = r.input_weight,
                        input_unit_price = r.input_unit_price,
                        input_amount = r.input_amount,
                        output_barcode = r.output_barcode,
                        output_bale_no = r.output_bale_no,
                        output_weight = r.output_weight,
                        output_unit_price = r.output_unit_price,
                        output_amount = r.output_amount,
                        flag_io = r.flag_io
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_rptrc010";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PricingWpf.Reports.RDLC.rc010.rdlc";
                    ReportViewer.RefreshReport();
                }
             }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
