﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PricingWpf.Form
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        public MainWindow(string _username)
        {
            try
            {
                InitializeComponent();
                _username = _username.ToLower();
                //GreenMenu.Visibility = _username == "nopporn" ? Visibility.Visible : Visibility.Collapsed;
                //ProcessingMenu.Visibility = _username == "nopporn" ? Visibility.Visible : Visibility.Collapsed;
                //ShippingMenu.Visibility = _username == "nopporn" ? Visibility.Visible : Visibility.Collapsed;
                //SetupMenu.Visibility = _username == "nimnim" ? Visibility.Visible : Visibility.Collapsed;
                //ManagementMenu.Visibility = _username == "export" ? Visibility.Visible : Visibility.Collapsed;


                GreenMenu.Visibility = Visibility.Visible;
                ProcessingMenu.Visibility = Visibility.Visible;
                ShippingMenu.Visibility = Visibility.Visible;
                SetupMenu.Visibility = Visibility.Visible;
                ManagementMenu.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceivingMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new GreenLeaf.Receiving());
        }

        private void GreenTransferMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new GreenLeaf.GreenTransfer());
        }

        private void HandstripMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new GreenLeaf.Handstrip());
        }

        private void RegradeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new GreenLeaf.Regrade());
        }

        private void ProductMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Processing.Product());
        }

        private void ByProductMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Processing.ByProduct());
        }

        private void ShipppingProductMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Shipping.ShipppingProduct());
        }

        private void RCTMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Setup.RedryingChargeAndTransport());
        }

        private void TRMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Setup.TransportAndRedryingCode());
        }

        private void PMCMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Setup.PackingMaterialCost());
        }

        private void ExchangeRateMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Setup.ExchangeRate());
        }

        private void ExportValueMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Management.ExportValue());
        }

        private void Report_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Receiving_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.GreenLeaf.Receiving());
        }

        private void ExiteMenu_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void VarianceMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Management.VarianceAdjust());
        }

        private void BroughtMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.GreenLeaf.BroughtForward());
        }
    }
}
