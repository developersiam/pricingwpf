﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Management
{
    public class vm_VarianceValue : ObservableObject
    {
        private int _crop;
        private bool _exportValueType;
        private string _packedGrade;
        private string _Rsign;
        private string _Psign;
        private string _Packsign;
        private string _customerrs;
        private string _graders;
        private string _Rremark;
        private string _Premark;
        private string _Packremark;
        private decimal _RValue;
        private decimal _PValue;
        private decimal _PackValue;
        private string _packingmat;
        private IList<VarianceAdjustInfo> _variancePackedList;
        private IList<PackedInfo> _packedGradeList;
        private IList<PackedGradeCustomerInfo> _customerrsList;
        private IList<PackedGradeCustomerInfo> _gradersList;
        private IList<PackingInfo> _packingmatList;
        private IList<SignInfo> _signList;
        private int _totalRecord;
        private ICommand _saveCommand;
        private ICommand _deleteCommand;
        private ICommand _cropKeyPressCommand;
        private ICommand _detailsCommand;

        private decimal _redryCost;
        //private decimal _packedPrice;
        private decimal _priceCost;
        private int _bales;
        private int _weight;
        private int _id;
        private DateTime _createdDate;
        private decimal _packingmatCost;
        private decimal _nrvCost;

        public vm_VarianceValue()
        {
            _crop = DateTime.Now.Year;
            _exportValueType = true;
        }
        public decimal RedryValue
        {
            get { return _RValue; }
            set { _RValue = value; }
        }
        public string RedrySignValue
        {
            get { return _Rsign; }
            set { _Rsign = value; }
        }
        public decimal PackingValue
        {
            get { return _PValue; }
            set { _PValue = value; }
        }
        public decimal PackValue
        {
            get { return _PackValue; }
            set { _PackValue = value; }
        }
        public string PackingSignValue
        {
            get { return _Psign; }
            set { _Psign = value; }
        }
        public string PackSignValue
        {
            get { return _Packsign; }
            set { _Packsign = value; }
        }
        public string PackRemark
        {
            get { return _Packremark; }
            set { _Packremark = value; }
        }
        public string RRemark
        {
            get { return _Rremark; }
            set { _Rremark = value; }
        }
        public string PRemark
        {
            get { return _Premark; }
            set { _Premark = value; }
        }
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }
        public int Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _exportValueType = true;
                RaisePropertyChangedEvent("ExportValueType");
            }
        }
        public IList<VarianceAdjustInfo> VarianceAdjustList
        {
            get { return _variancePackedList; }
            set { _variancePackedList = value; }
        }
        public ICommand CropKeyPressCommand
        {
            get { return _cropKeyPressCommand ?? (_cropKeyPressCommand = new RelayCommand(CropKeyPress)); }
            set { _cropKeyPressCommand = value; }
        }
        public void CropKeyPress(object e)
        {
            var test = _crop;
            ExportPackedListBinding();
            PackedGradeListBinding();
        }

        public bool ExportValueType
        {
            get { return _exportValueType; }
            set
            {
                _exportValueType = value;

                ExportPackedListBinding();
                PackedGradeListBinding();

            }
        }
        private void ExportPackedListBinding()
        {
            _variancePackedList = PricingService.VarianceAdjustRepository().GetJsonVarianceAdjValue(_crop.ToString()); 

            _totalRecord = _variancePackedList.Count();
            RaisePropertyChangedEvent("SignList");
            RaisePropertyChangedEvent("VarianceAdjustList");
            RaisePropertyChangedEvent("TotalRecord");
        }
        //private void ExportPackedListWithPakced()
        //{
        //    _variancePackedList = PricingService.VarianceAdjustRepository().GetJsonVarianceAdjValue(_crop.ToString()).Where(x => x.PackedGrade == _packedGrade).ToList();

        //    _totalRecord = _variancePackedList.Count();
        //    RaisePropertyChangedEvent("SignList");
        //    RaisePropertyChangedEvent("VarianceAdjustList");
        //    RaisePropertyChangedEvent("TotalRecord");
        //}
        private void PackedGradeListBinding()
        {
            _packedGradeList = PricingService.ExportRepository()
                    .GetPackedGrade(_crop);

            RaisePropertyChangedEvent("PackedGradeList");
        }
        public string PackedGrade
        {
            get { return _packedGrade; }
            set
            {
                _packedGrade = value;
                _redryCost = 0;
                _priceCost = 0;
                _bales = 0;
                _RValue = 0;
                _Rsign = "";
                _PValue = 0;
                _Psign = "";
                _Packsign = "";
                _PackValue = 0;
                _Rremark = "";
                _Premark = "";
                _Packremark = "";
                _packingmatCost = 0;
                _weight = 0;
                _nrvCost = 0;

                RaisePropertyChangedEvent("CustomerrsList");
                RaisePropertyChangedEvent("RedrySignValue");
                RaisePropertyChangedEvent("PackingSignValue");
                RaisePropertyChangedEvent("RedryValue");
                RaisePropertyChangedEvent("PackingValue");
                RaisePropertyChangedEvent("PackSignValue");
                RaisePropertyChangedEvent("PackValue");
            }
        }
        public string Customerrs
        {
            get { return _customerrs; }
            set
            {
                _customerrs = value;

                RaisePropertyChangedEvent("GradersList");
            }
        }

        public string Graders
        {
            get { return _graders; }
            set
            {
                _graders = value;

                RaisePropertyChangedEvent("PackingMatList");

            }
        }
        public string PackingMat
        {
            get { return _packingmat; }
            set
            {
                _packingmat = value;

                RaisePropertyChangedEvent("PackedRedry");
                //RaisePropertyChangedEvent("PackedUnitPrice");
                RaisePropertyChangedEvent("PackedAvailable");
                RaisePropertyChangedEvent("PackedWeight");
                RaisePropertyChangedEvent("Value");
                RaisePropertyChangedEvent("PackingMatCost");
                RaisePropertyChangedEvent("PriceCost");
                RaisePropertyChangedEvent("RedrySignValue");
                RaisePropertyChangedEvent("PackingSignValue");
                RaisePropertyChangedEvent("PackSignValue");
                RaisePropertyChangedEvent("RedryValue");
                RaisePropertyChangedEvent("PackingValue");
                RaisePropertyChangedEvent("PackValue");
                RaisePropertyChangedEvent("NRVCost");
            }
        }
        public IList<PackedInfo> PackedGradeList
        {
            get { return _packedGradeList; }
            set { _packedGradeList = value; }
        }

        public IList<PackedGradeCustomerInfo> CustomerrsList
        {
            get
            {
                {
                    var list = PricingService.ExportRepository()
                        .GetAllPackedGradeCustomer(_crop)
                        .Where(x => x.PackedGrade == _packedGrade)
                        .ToList();

                    _gradersList = list;

                    return list;
                }
            }
            set { _customerrsList = value; }
        }
        public IList<PackingInfo> PackingMatList
        {
            get
            {
                {
                    if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "")
                        return null;

                    var list = PricingService.VarianceAdjustRepository().GetPackingMatByGrade(_customerrs, _graders, _packedGrade).ToList();

                    _packingmatList = list;

                    return list;
                }
            }
            set { _packingmatList = value; }
        }

        public IList<SignInfo> SignList
        {
            get
            {
                {
                    List<string> t = new List<string>();
                    t.Add("-/+");
                    t.Add("-");
                    t.Add("+");
                    List<SignInfo> result = new List<SignInfo>();
                    foreach(var i in t)
                    {
                        result.Add(new SignInfo()
                        {
                            sign = i
                        });
                    }
                    _signList = result;

                    return result;
                }
            }
            set { _signList = value; }
        }
        public IList<PackedGradeCustomerInfo> GradersList
        {
            get
            {
                return _gradersList.Where(x=>x.CustomerName == _customerrs).ToList();
            }
            set { _gradersList = value; }
        }
        public decimal PackedRedry
        {
            get
            {
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "") return 0;

                //decimal OriginalRedry = 0;
                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                if (t != null)
                {

                    _redryCost = Convert.ToDecimal(t.AvgRedry);

                    //OriginalRedry = Convert.ToDecimal(t.AvgRedry);

                    ////Check variance summary by this crop,grade,grader,customer
                    //VarianceSummaryInfo r = PricingService.VarianceAdjustRepository().GetSumVariance(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                    //if (r != null) _redryCost = OriginalRedry==0 ? 0 : OriginalRedry + Convert.ToDecimal(r.RSumValue);
                    //else _redryCost = OriginalRedry;                  
                }
                else _redryCost = 0;

                return _redryCost;
             }
            set { _redryCost = value;}
        }

        public decimal PriceCost
        {
            get
            {
                decimal OriginalPrice = 0;
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "")
                    return _priceCost = 0;
                    
                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                if (t != null)
                {
                    OriginalPrice = Convert.ToDecimal(t.MinPrice)/Convert.ToInt32(t.Bales);
                }
                else OriginalPrice= 0;

                _priceCost = OriginalPrice;
                return _priceCost;
            }
            set { _priceCost = value; }
        }

        public int PackedAvailable
        {
            get
            {
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "")
                    return 0;
                _bales = 0;

                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                if (t != null) _bales = Convert.ToInt32(t.Bales);

                return _bales;
            }
            set { _bales = value; }
        }
        public int PackedWeight
        {
            get
            {
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "")
                    return 0;

                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                if (t != null)
                    return Convert.ToInt32(t.Weight);
                else return 0;
            }
            set { _weight = value; }
        }
        public decimal PackingMatCost
        {
            get
            {
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "") return 0;

                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);

                //decimal OriginalPacking = 0;
                if (t != null)
                {
                    _packingmatCost = Convert.ToDecimal(t.PackingMat);

                    //OriginalPacking = Convert.ToDecimal(t.PackingMat);
                    ////Check variance summary by this crop,grade,grader,customer
                    //VarianceSummaryInfo r = PricingService.VarianceAdjustRepository().GetSumVariance(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                    //if (r != null) _packingmatCost = OriginalPacking == 0 ? 0 :  OriginalPacking + Convert.ToDecimal(r.PSumValue);
                    //else _packingmatCost = OriginalPacking;
                }
                else _packingmatCost = 0;

                return _packingmatCost;

            }
            set { _packingmatCost = value; }
        }
        public decimal NRVCost
        {
            get
            {
                decimal OriginalPrice = 0;
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "" || _packingmat == null || _packingmat == "")
                    return _nrvCost = 0;

                PackedGradeRedryInfo t = PricingService.VarianceAdjustRepository().GetPackedWeight(_crop, _customerrs, _graders, _packedGrade, _packingmat);
                if (t != null)
                {
                    OriginalPrice = (Convert.ToDecimal(t.MinPrice) / Convert.ToInt32(t.Bales))/Convert.ToDecimal(t.Weight);
                }
                else OriginalPrice = 0;

                _nrvCost = OriginalPrice;
                return _nrvCost;
            }
            set { _nrvCost = value; }
        }
        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }
        public void Save(object e)
        {
            try
            {               
                //Check duplicate data
                if (CheckbeforeAdd())
                {
                    ///// Add a packed export value.  ///////
                    //-------------------- Update only Packed issued = 0 -----------------
                    PricingService.VarianceAdjustRepository().AddPackedVarianceAdj(_crop,
                        _customerrs,
                        _graders,
                        _packedGrade,
                        _packingmat,
                        _Rsign,
                        Convert.ToDecimal(_RValue),
                        _Rremark,
                        _Psign,
                        Convert.ToDecimal(_PValue),
                        _Premark,
                        _Packsign,
                        Convert.ToDecimal(_PackValue),
                        _Packremark,
                        SingletonConfiguration.getInstance().Username);

                    ExportPackedListBinding();
                    PackedGradeListBinding();

                    MessageBox.Show("Add data success!", "information", MessageBoxButton.OK, MessageBoxImage.Information);

                    _RValue = 0;
                    _PValue = 0;
                    _PackValue = 0;
                    _redryCost = 0;
                    _priceCost = 0;
                    _bales = 0;
                    _weight = 0;
                    _packingmatCost = 0;
                    _Rsign = "";
                    _Psign = "";
                    _Packsign = "";
                    _Rremark = "";
                    _Premark = "";
                    _Packremark = "";

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }
        public void Delete(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this record? This actions going to get back a packed price (only onhand packed).", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                int count_adj = 0;
                List<VarianceAdjustInfo> vList = _variancePackedList.Where(x => x.PackedGrade == _packedGrade && x.Customer == _customerrs && x.CustomerGrade == _graders).ToList();
                //--------------------There are more than one Adjust for Grade/Grader/Customer ----------------
                count_adj = vList.Count();
                if (count_adj > 1)
                {
                    //compare create date with selected with list

                    
                        return;
                }
                else
                {
                    //-------------------- Restore original cost to only Packed issued = 0 -----------------
                    var item = (VarianceAdjustInfo)e;
                    PricingService.VarianceAdjustRepository()
                        .DeleteAdjust(item.Id);                 


                    MessageBox.Show("Delete data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
                }

                ExportPackedListBinding();
                PackedGradeListBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public ICommand DetailsCommand
        {
            get { return _detailsCommand ?? (_detailsCommand = new RelayCommand(Details)); }
            set { _detailsCommand = value; }
        }
        public void Details(object e)
        {
            try
            {
                VarianceAdjDetails window = new VarianceAdjDetails();
                var vm = new vm_VarianceValueDetails(window);
                window.DataContext = vm;

                var v = (VarianceAdjustInfo)e;

                vm.Crop = v.Crop;
                vm.PackedGrade = v.PackedGrade;
                vm.Customerrs = v.Customer;
                vm.Graders = v.CustomerGrade;
                vm.VarinaceId = v.Id;
                vm.CreatedDate = v.CreatedDate;
                vm.RValue = v.RValue;
                vm.PValue = v.PValue;
                vm.PackValue = v.PackValue;

                window.ShowDialog();

                ExportPackedListBinding();
                PackedGradeListBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public bool CheckbeforeAdd()
        {
            bool pass = true;
                            

                if (_packedGrade == null || _packedGrade == "")
                {
                    MessageBox.Show("Packed grade not found!","Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }
                
                if (_customerrs == null || _customerrs == "")
                {
                    MessageBox.Show("Customer reserved not found!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if (_graders == null || _graders == "")
                {
                    MessageBox.Show("Packed grade reserved not found!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }  
                
                if (_packingmat == null || _packingmat == "")
                {
                    MessageBox.Show("Packing mat not found!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }


                if (_Psign == "" && _Rsign == "" && _PackValue == 0)
                {
                    MessageBox.Show("Please select atleast one sign before save!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if (_Psign == "-/+" && _Rsign == "-/+" && _PackValue == 0)
                {
                    MessageBox.Show("Please select atleast one sign before save!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if ((_RValue <= 0 && _PValue <= 0 && _PackValue <= 0 ))
                {
                    MessageBox.Show("Please input adjustment value more than zero!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if (_Psign == "-" && (_packingmatCost<_PValue))
                {
                    MessageBox.Show("Cannot adjust packing mat cost more than remain cost!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if (_Rsign == "-" && (_redryCost<_RValue))
                {
                    MessageBox.Show("Cannot adjust redrying charge more than remain cost!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if ((_RValue > 0 || _PValue > 0) && _PackValue > 0)
                {
                    MessageBox.Show("Cannot adjust NRV or Redry/Packing mat cost at the same time!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

                if (_bales == 0)
                {
                    MessageBox.Show("There are no available pack for this Grade!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    pass = false;
                }

            return pass;
        }


    }
}
