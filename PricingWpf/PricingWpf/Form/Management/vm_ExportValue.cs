﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Management
{
    public class vm_ExportValue : ObservableObject
    {
        private int _crop;
        private bool _exportValueType;
        private string _packedGrade;
        private string _classifiedGrade;
        private string _customerrs;
        private string _graders;
        private decimal _exportValue;
        private IList<ExportValueInfo> _exportPackedList;
        private IList<ClassifiedExportValueInfo> _exportClassifiedList;
        private IList<ExportValueType> _exportValueTypeList;
        private IList<CropReceiving> _cropList;
        private IList<PackedInfo> _packedGradeList;
        private IList<ClassifiedExportValueInfo> _classifiedGradeList;
        private IList<PackedGradeCustomerInfo> _customerrsList;
        private IList<PackedGradeCustomerInfo> _gradersList;
        private int _totalRecord;
        private ICommand _saveCommand;
        private ICommand _editCommand;
        private ICommand _deleteCommand;
        private ICommand _cropKeyPressCommand;

        private Visibility _visiblePackedGrade;
        private Visibility _visibleClassifiedGrade;
        private decimal _packedUnitPrice;
        private decimal _packedWeight;
        private decimal _classifiedUnitPrice;

        public decimal ClassifiedUnitPrice
        {
            get
            {
                if (_classifiedGrade == null || _classifiedGrade == "")
                    return 0;

                return Convert.ToDecimal(PricingService.ExportRepository()
                    .GetClassifiedUnitPrice(_crop, _classifiedGrade).UnitPrice);
            }
            set { _classifiedUnitPrice = value; }
        }

        public decimal PackedWeight
        {
            get
            {
                if (_customerrs == null || _customerrs == "" || _graders == null || _graders == "")
                    return 0;

                return Convert.ToDecimal(PricingService.ExportRepository()
                  .GetPackedWeight(_crop, _customerrs, _graders, _packedGrade)
                  .AvgWeight);
            }
            set { _packedWeight = value; }
        }

        public decimal PackedUnitPrice
        {
            get
            {
                if (_packedGrade == null || _packedGrade == "")
                    return 0;

                return Convert.ToDecimal(PricingService.ExportRepository()
                    .GetPackedUnitPrice(_crop, _packedGrade)
                    .UnitPrice);
            }
            set { _packedUnitPrice = value; }
        }

        public int Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _exportValueType = true;
                RaisePropertyChangedEvent("ExportValueType");
            }
        }

        public bool ExportValueType
        {
            get { return _exportValueType; }
            set
            {
                _exportValueType = value;

                if (_exportValueType == true)
                {
                    ExportPackedListBinding();
                    PackedGradeListBinding();

                    _visiblePackedGrade = Visibility.Visible;
                    _visibleClassifiedGrade = Visibility.Collapsed;
                }
                else
                {
                    ExportClassifiedListBinding();
                    ClassifiedListBinding();

                    _visiblePackedGrade = Visibility.Collapsed;
                    _visibleClassifiedGrade = Visibility.Visible;
                }

                RaisePropertyChangedEvent("VisiblePackedGrade");
                RaisePropertyChangedEvent("VisibleClassifiedGrade");
            }
        }

        public string PackedGrade
        {
            get { return _packedGrade; }
            set
            {
                _packedGrade = value;
                _packedWeight = 0;
                _exportValue = 0;

                RaisePropertyChangedEvent("CustomerrsList");
                RaisePropertyChangedEvent("PackedUnitPrice");
                RaisePropertyChangedEvent("PackedWeight");
                RaisePropertyChangedEvent("ExportValue");
            }
        }

        public string ClassifiedGrade
        {
            get { return _classifiedGrade; }
            set
            {
                _classifiedGrade = value;
                _exportValue = 0;

                RaisePropertyChangedEvent("ClassifiedUnitPrice");
                RaisePropertyChangedEvent("ExportValue");
            }
        }

        public string Customerrs
        {
            get { return _customerrs; }
            set
            {
                _customerrs = value;

                RaisePropertyChangedEvent("GradersList");
                RaisePropertyChangedEvent("PackedWeight");
            }
        }

        public string Graders
        {
            get { return _graders; }
            set
            {
                _graders = value;

                RaisePropertyChangedEvent("GradersList");
                RaisePropertyChangedEvent("PackedWeight");
            }
        }

        public decimal ExportValue
        {
            get { return _exportValue; }
            set { _exportValue = value; }
        }

        public IList<ExportValueInfo> ExportPackedList
        {
            get { return _exportPackedList; }
            set { _exportPackedList = value; }
        }

        public IList<ClassifiedExportValueInfo> ExportClassifiedList
        {
            get { return _exportClassifiedList; }
            set { _exportClassifiedList = value; }
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public IList<CropReceiving> CropList
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp(); }
            set { _cropList = value; }
        }

        public IList<PackedInfo> PackedGradeList
        {
            get { return _packedGradeList; }
            set { _packedGradeList = value; }
        }

        public IList<ClassifiedExportValueInfo> ClassifiedGradeList
        {
            get { return _classifiedGradeList; }
            set { _classifiedGradeList = value; }
        }

        public IList<PackedGradeCustomerInfo> CustomerrsList
        {
            get
            {
                {
                    var list = PricingService.ExportRepository()
                        .GetAllPackedGradeCustomer(_crop)
                        .Where(x => x.PackedGrade == _packedGrade)
                        .ToList();

                    _gradersList = list;

                    return list;
                }
            }
            set { _customerrsList = value; }
        }

        public IList<PackedGradeCustomerInfo> GradersList
        {
            get
            {
                return _gradersList;
            }
            set { _gradersList = value; }
        }

        public IList<ExportValueType> ExportValueTypeList
        {
            get
            {
                var list = new List<ExportValueType>();

                list.Add(new ExportValueType { Type = true, TypeName = "Packed Grade" });
                list.Add(new ExportValueType { Type = false, TypeName = "Classified Grade" });

                return list;
            }
            set { _exportValueTypeList = value; }
        }

        public Visibility VisibleClassifiedGrade
        {
            get
            {
                if (_exportValueType == false)
                {
                    RaisePropertyChangedEvent("ClassifiedList");
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set { _visibleClassifiedGrade = value; }
        }

        public Visibility VisiblePackedGrade
        {
            get
            {
                if (_exportValueType == true)
                {
                    RaisePropertyChangedEvent("PackedList");
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set { _visiblePackedGrade = value; }
        }

        public ICommand CropKeyPressCommand
        {
            get { return _cropKeyPressCommand ?? (_cropKeyPressCommand = new RelayCommand(CropKeyPress)); }
            set { _cropKeyPressCommand = value; }
        }
        public void CropKeyPress(object e)
        {
            var test = _crop;

            if (_exportValueType == true)
            {
                ExportPackedListBinding();
                PackedGradeListBinding();

                _visiblePackedGrade = Visibility.Visible;
                _visibleClassifiedGrade = Visibility.Collapsed;
            }
            else
            {
                ExportClassifiedListBinding();
                ClassifiedListBinding();

                _visiblePackedGrade = Visibility.Collapsed;
                _visibleClassifiedGrade = Visibility.Visible;
            }

            RaisePropertyChangedEvent("VisiblePackedGrade");
            RaisePropertyChangedEvent("VisibleClassifiedGrade");
        }

        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }
        public void Delete(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this record?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_exportValueType == true)
                {
                    var item = (ExportValueInfo)e;
                    PricingService.ExportRepository()
                        .DeletePacked(item.Crop,
                        item.Customer,
                        item.CustomerGrade,
                        item.PackedGrade);

                    RaisePropertyChangedEvent("PackedList");
                }
                else
                {
                    ///Classified grade edit.
                    ///
                    var item = (ClassifiedExportValueInfo)e;
                    PricingService.ExportRepository()
                        .DeleteClassified(item.Crop,
                        item.ClassifiedGrade);

                    RaisePropertyChangedEvent("ClassifiedList");
                }

                MessageBox.Show("Delete data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand EditCommand
        {
            get { return _editCommand ?? (_editCommand = new RelayCommand(Edit)); }
            set { _editCommand = value; }
        }
        public void Edit(object e)
        {
            try
            {
                ExportValueEdit window = new ExportValueEdit();
                try
                {
                    var vm = new vm_ExportValueEdit(window);
                    window.DataContext = vm;
                    vm.ExportValueType = _exportValueType;

                    if (_exportValueType == true)
                    {
                        ///Packed grade edit.
                        var item = (ExportValueInfo)e;
                        vm.Crop = item.Crop;
                        vm.PackedGrade = item.PackedGrade;
                        vm.Customerrs = item.Customer;
                        vm.Graders = item.CustomerGrade;
                        vm.ExportValue = item.Value;
                    }
                    else
                    {
                        ///Classified grade edit.
                        var item = (ClassifiedExportValueInfo)e;
                        vm.Crop = item.Crop;
                        vm.ClassifiedGrade = item.ClassifiedGrade;
                        vm.ExportValue = item.Value;
                    }

                    window.ShowDialog();
                    if (_exportValueType == true)
                        RaisePropertyChangedEvent("PackedList");
                    else
                        RaisePropertyChangedEvent("ClassifiedList");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    window.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }
        public void Save(object e)
        {
            try
            {
                if (_exportValueType == true)
                {
                    //Check duplicate data
                    //bool duplicate = PricingService.ExportRepository()
                    //    .ExistedPackedExportByCropAndPacked(_crop, _customerrs, _packedGrade, _graders);
                    if (_packedGrade == null || _packedGrade == "")
                        throw new ArgumentException("Packed grade not found!");

                    if (_customerrs == null || _customerrs == "")
                        throw new ArgumentException("Customer reserved not found!");

                    if (_graders == null || _graders == "")
                        throw new ArgumentException("Packed grade reserved not found!");

                    bool duplicate = _exportPackedList
                        .Where(x => x.PackedGrade == _packedGrade &&
                        x.Customer == _customerrs &&
                        x.CustomerGrade == _graders)
                        .Count() > 0 ? true : false;

                    if (duplicate)
                        throw new ArgumentException("Dupplicated!");

                    /// Add a packed export value.
                    PricingService.ExportRepository()
                        .AddPackedExportValue(_crop,
                        _customerrs,
                        _graders,
                        _packedGrade,
                        Convert.ToDecimal(_exportValue),
                        SingletonConfiguration.getInstance().Username);

                    RaisePropertyChangedEvent("ExportPackedList");
                }
                else
                {
                    if (_classifiedGrade == null || _classifiedGrade == "")
                        throw new ArgumentException("Classified grade not found!");

                    bool duplicate = _exportClassifiedList
                        .Where(x => x.Crop == _crop &&
                        x.ClassifiedGrade == _classifiedGrade)
                        .Count() > 0 ? true : false;

                    if (duplicate)
                        throw new ArgumentException("Dupplicated!");

                    /// Add a classified export value.
                    PricingService.ExportRepository()
                        .AddExportValue(_crop,
                        _classifiedGrade,
                        Convert.ToDecimal(_exportValue),
                        SingletonConfiguration.getInstance().Username);

                    RaisePropertyChangedEvent("ExportClassifiedList");
                }

                MessageBox.Show("Add data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);

                _exportValue = 0;
                _packedWeight = 0;
                _packedUnitPrice = 0;
                _classifiedUnitPrice = 0;

                RaisePropertyChangedEvent("ExportValue");
                RaisePropertyChangedEvent("PackedWeight");
                RaisePropertyChangedEvent("PackedUnitPrice");
                RaisePropertyChangedEvent("PackedUnitPrice");
                RaisePropertyChangedEvent("ExportValueType");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public vm_ExportValue()
        {
            _crop = DateTime.Now.Year;
            _exportValueType = false;
        }

        private void ExportPackedListBinding()
        {
            _exportPackedList = PricingService.ExportRepository()
                .GetJsonPackedExportValue(_crop.ToString());

            _totalRecord = _exportPackedList.Count();
            RaisePropertyChangedEvent("ExportPackedList");
            RaisePropertyChangedEvent("TotalRecord");
        }

        private void ExportClassifiedListBinding()
        {
            _exportClassifiedList = PricingService.ExportRepository()
                    .GetClassifiedExportValue(_crop);

            _totalRecord = _exportClassifiedList.Count();
            RaisePropertyChangedEvent("ExportClassifiedList");
            RaisePropertyChangedEvent("TotalRecord");
        }

        private void ClassifiedListBinding()
        {
            _classifiedGradeList = PricingService.ExportRepository()
                    .GetClassifiedGradeList(_crop.ToString());

            RaisePropertyChangedEvent("ClassifiedGradeList");
        }

        private void PackedGradeListBinding()
        {
            _packedGradeList = PricingService.ExportRepository()
                    .GetPackedGrade(_crop);

            RaisePropertyChangedEvent("PackedGradeList");
        }
    }


    public class ExportValueType
    {
        public bool Type { get; set; }
        public string TypeName { get; set; }
    }
}
