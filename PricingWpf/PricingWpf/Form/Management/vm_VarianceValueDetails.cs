﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;

namespace PricingWpf.Form.Management
{
    public class vm_VarianceValueDetails : ObservableObject
    {
        private int _crop;
        private string _packedGrade;
        private string _customerrs;
        private string _graders;
        private decimal _RValue;
        private decimal _PValue;
        private decimal _PackValue;
        private int _totalRecord;
        private DateTime _createdDate;
        private int _id;
        private IList<VarianceAdjustDetailsInfo> _variancePackedList;
        private ICommand _reportCommand;
        public vm_VarianceValueDetails(Window window)
        {
            
        }
        public IList<PricingDomainModel.VarianceAdjustDetailsInfo> VarianceAdjustDetailsList
        {
            get
            {
                var list = PricingService.VarianceAdjustDetailsRepository()
                    .GetJsonVarianceAdjValueDetails(_id);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _variancePackedList = value; }
        }
        private void VarianceDetailsBinding()
        {
            try
            {
                RaisePropertyChangedEvent("VarianceAdjustDetailsList");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        public decimal RValue
        {
            get { return _RValue; }
            set { _RValue = value; }
        }
        public decimal PValue
        {
            get { return _PValue; }
            set { _PValue = value; }
        }
        public decimal PackValue
        {
            get { return _PackValue; }
            set { _PackValue = value; }
        }

        public string Graders
        {
            get { return _graders; }
            set { _graders = value; }
        }

        public string Customerrs
        {
            get { return _customerrs; }
            set { _customerrs = value; }
        }

        public string PackedGrade
        {
            get { return _packedGrade; }
            set { _packedGrade = value; }
        }

        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }
        public int VarinaceId
        {
            get { return _id; }
            set
            {
                _id = value;
                VarianceDetailsBinding();
            }
        }
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        public ICommand ReportCommand
        {
            get { return _reportCommand ?? (_reportCommand = new RelayCommand(Report)); }
            set { _reportCommand = value; }
        }
        public void Report(object e)
        {
            try
            {
                if (_id > 0)
                {
                    Reports.RPT_Variance window = new Reports.RPT_Variance(_id);
                    window.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
