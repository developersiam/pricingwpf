﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Management
{
    public class vm_ExportValueEdit : ObservableObject
    {
        private int _crop;
        private string _packedGrade;
        private string _customerrs;
        private string _graders;
        private string _classifiedGrade;
        private decimal _exportValue;
        private bool _exportValueType;
        private Visibility _visiblePacked;
        private Visibility _visibleClassified;
        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }
        public void Save(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to update this record?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_exportValueType == true)
                {
                    ///Packed grade edit.
                    PricingService.ExportRepository()
                        .EditPacked(_crop,
                        _customerrs,
                        _graders,
                        _packedGrade,
                        Convert.ToDecimal(_exportValue),
                        SingletonConfiguration.getInstance().Username);
                }
                else
                {
                    ///Classified grade edit.
                    PricingService.ExportRepository()
                        .EditClassified(_crop,
                        _classifiedGrade,
                        "C",
                        Convert.ToDecimal(_exportValue),
                        SingletonConfiguration.getInstance().Username);
                }

                MessageBox.Show("Update data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public Visibility VisibleClassified
        {
            get
            {
                return _exportValueType == true ?
                    Visibility.Collapsed : Visibility.Visible;
            }
            set { _visibleClassified = value; }
        }


        public Visibility VisiblePacked
        {
            get
            {
                return _exportValueType == true ?
                    Visibility.Visible : Visibility.Collapsed;
            }
            set { _visiblePacked = value; }
        }


        public bool ExportValueType
        {
            get { return _exportValueType; }
            set { _exportValueType = value; }
        }


        public decimal ExportValue
        {
            get { return _exportValue; }
            set { _exportValue = value; }
        }


        public string ClassifiedGrade
        {
            get { return _classifiedGrade; }
            set { _classifiedGrade = value; }
        }


        public string Graders
        {
            get { return _graders; }
            set { _graders = value; }
        }


        public string Customerrs
        {
            get { return _customerrs; }
            set { _customerrs = value; }
        }


        public string PackedGrade
        {
            get { return _packedGrade; }
            set { _packedGrade = value; }
        }


        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        public vm_ExportValueEdit(Window window)
        {

        }
    }
}
