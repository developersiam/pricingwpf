﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Shipping
{
    public class vm_ShippingProduct : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private IList<ShippingProductionInfo> _dataMasters;
        private string _crop;
        private bool? _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllNewCommand;
        private ICommand _showDetailsCommand;
        private ICommand _showAllCommand;

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public ICommand ShowAllNewCommand
        {
            get { return _showAllNewCommand ?? (_showAllNewCommand = new RelayCommand(ShowAllNew)); }
            set { _showAllNewCommand = value; }
        }
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }
        public bool? IsShowAddNewRecord
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }
        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _isShowAll = true;
                RaisePropertyChangedEvent("DataMasters");
            }
        }
        public IList<ShippingProductionInfo> DataMasters
        {
            get
            {
                var list = new List<ShippingProductionInfo>();
                if (_crop != null)
                {
                    if (_isShowAll == false)
                        list = PricingService.ShippingProductionRepository()
                        .GetNewJsonShippingProduct();
                    else
                    {
                        int iCrop = int.Parse(_crop);
                        list = PricingService.ShippingProductionRepository()
                            .GetJsonShippingProduct(iCrop);
                    }
                }

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _dataMasters = value; }
        }

        public IList<CropReceiving> Crops
        {
            get
            {
                return PricingService.ShippingProductionRepository()
                  .GetProcessingCorp();
            }
            set { _crops = value; }
        }

        public vm_ShippingProduct()
        {
            RaisePropertyChangedEvent("Crops");
        }

        public void ShowAllNew(object e)
        {
            _isShowAll = false;
            RaisePropertyChangedEvent("DataMasters");
        }

        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("DataMasters");
        }

        public void ShowDetails(object e)
        {
            var window = new ShippingDetail();
            try
            {
                var vm = new vm_ShippingDetail(window);
                window.DataContext = vm;
                vm.ShippingNo = e.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                window.Close();
                return;
            }

            window.ShowDialog();
            RaisePropertyChangedEvent("DataMasters");
        }
    }
}
