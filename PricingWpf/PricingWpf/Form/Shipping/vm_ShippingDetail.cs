﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Shipping
{
    public class vm_ShippingDetail : ObservableObject
    {
        private string _shippingNo;
        private string _documentNo;
        private DateTime _documentDate;
        private string _crop;
        private IList<TypeInfo> _typeInfoes;
        private string _type;
        private IList<TypeRBInfo> _typeRBInfoes;
        private string _typeRB;
        private string _typeRBDescription;
        private string _averagePrice;
        private string _totalWeight;
        private string _totalAmount;
        private IList<ShippingProductDetails> _detailsList;
        private int _totalRecord;
        private ShippingDetail _window;
        private ICommand _deleteMasterCommand;
        private ICommand _addMasterCommand;
        private Visibility _addButtonVisibility;
        private Visibility _deleteButtonvisibility;
        private bool _enbleCrop;
        private bool _enbleType;

        public bool EnableType
        {
            get { return string.IsNullOrEmpty(_documentNo); }
            set { _enbleType = value; }
        }

        public bool Enablecrop
        {
            get { return string.IsNullOrEmpty(_documentNo); }
            set { _enbleCrop = value; }
        }

        public Visibility DeleteButtonvisibility
        {
            get { return !string.IsNullOrEmpty(_documentNo) ? Visibility.Visible : Visibility.Collapsed; }
            set { _deleteButtonvisibility = value; }
        }

        public Visibility AddButonVisibility
        {
            get { return string.IsNullOrEmpty(_documentNo) ? Visibility.Visible : Visibility.Collapsed; }
            //get { return Visibility.Visible; }
            set { _addButtonVisibility = value; }
        }

        public ICommand AddMasterCommand
        {
            get { return _addMasterCommand ?? (_addMasterCommand = new RelayCommand(AddMaster)); }
            set { _addMasterCommand = value; }
        }

        public void AddMaster(object e)
        {
            int iCrop = Convert.ToInt32(_crop);
            var user = SingletonConfiguration.getInstance().Username;


            if (MessageBox.Show("Do you want to save this record?", "warning!",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            _documentNo = PricingService.ShippingProductionRepository()
                .GetDocumentNo(iCrop);

            if (_documentNo == null || _documentNo == "")
                throw new ArgumentException("Document no. can not be null");

            PricingService.ShippingProductionRepository()
                .AddShippingProduct(_documentNo,
                _documentDate,
                _shippingNo,
                iCrop,
                _type,
                _typeRB,
                user);

            MessageBox.Show("Update success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            _window.Close();
        }

        public ICommand DeleteMasterCommand
        {
            get { return _deleteMasterCommand ?? (_deleteMasterCommand = new RelayCommand(DeleteMaster)); }
            set { _deleteMasterCommand = value; }
        }

        public void DeleteMaster(object e)
        {

            if (MessageBox.Show("Do you want to delete this record?", "warning!",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            PricingService.ShippingProductionRepository()
                .DeleteShipping(_documentNo);

            MessageBox.Show("Delete success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            _window.Close();
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public IList<TypeInfo> TypeInfoes
        {
            get { return PricingService.ShippingProductionRepository().GetTypeName(); }
            set { _typeInfoes = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string TypeRB
        {
            get { return _typeRB; }
            set { _typeRB = value; }
        }

        public string TypeRBDescription
        {
            get { return _typeRBDescription; }
            set { _typeRBDescription = value; }
        }
        public IList<TypeRBInfo> TypeRBInfoes
        {
            get { return PricingService.ShippingProductionRepository().GetTypeRBName(); }
            set { _typeRBInfoes = value; }
        }

        public string TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = FormatCorrector.FormatNumber(2, value, true); }
        }

        public string TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = FormatCorrector.FormatNumber(2, value, true); }
        }

        public string AveragePrice
        {
            get { return _averagePrice; }
            set { _averagePrice = FormatCorrector.FormatNumber(2, value, true); }
        }

        public string Crop
        {
            get { return _crop; }
            set { _crop = FormatCorrector.FormatNumber(2, value, false); }
        }

        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { _documentDate = value; }
        }

        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }

        public IList<ShippingProductDetails> DetailsList
        {
            get
            {
                var list = new List<ShippingProductDetails>();
                list = PricingService.ShippingProductionRepository().GetJsonShippingDetails(_shippingNo);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _detailsList = value; }
        }

        public string ShippingNo
        {
            get { return _shippingNo; }
            set
            {
                _shippingNo = value;
                ShippingDetailsBinding();
            }
        }

        public vm_ShippingDetail(ShippingDetail window)
        {
            _window = window;
        }

        private void ShippingDetailsBinding()
        {
            try
            {
                var model = PricingService.ShippingProductionRepository()
                    .GetShippingProductCalculate(_shippingNo);

                if (model == null)
                    throw new ArgumentException("Shipping Not Found." +
                        " This shipping no not found, Please select another one to process.");
                
                _documentNo = model.Doc_No;
                _documentDate = model.Doc_Date;
                _crop = string.IsNullOrEmpty(model.Doc_No) ? "" : model.Crop.ToString();
                _type = string.IsNullOrEmpty(model.Doc_No) ? "" : model.Type;
                _typeRB = model.TypeRB;
                _typeRBDescription = model.TypeRB == "R" ? "Product" : "By Product";
                _averagePrice = model.Average_Price.ToString("N2");
                _totalAmount = model.Total_Cost.ToString("N2");
                _totalWeight = model.Total_Weight.ToString("N2");


                _deleteButtonvisibility = model.Doc_No == "" ? Visibility.Collapsed : Visibility.Visible;
                _addButtonVisibility = model.Doc_No == "" ? Visibility.Visible : Visibility.Collapsed;

                RaisePropertyChangedEvent("AddButonVisibility");
                RaisePropertyChangedEvent("DeleteButtonvisibility");
                RaisePropertyChangedEvent("DetailsList");
                RaisePropertyChangedEvent("DocumentNo");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
