﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_BroughtForward : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private string _crop;
        private string _tocrop;
        private IList<TypeInfo> _typeList;
        private string _type;
        private IList<SubTypeInfo> _subtypeList;
        private string _subtype;
        private IList<CompanyInfo> _companyList;
        private string _company;
        private decimal _OnhandAmt;
        private decimal _OnhandAmtUnit;
        private int _Onhandbales;
        private decimal _Onhandweight;


        private bool _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;

        private IList<BroughtForwardInfo> _BFMasters;

        private ICommand _createdCommand;
        private string _rcno;
        private string _bfno;
        

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public void ShowDetails(object e)
        {
            try
            {
                if (e == null) return;

                var selectedItem = (BroughtForwardInfo)e;
                _bfno = selectedItem.bfno;

                if (!string.IsNullOrEmpty(_bfno))
                {
                    Reports.RPT_BroughtF window = new Reports.RPT_BroughtF(_bfno);
                    window.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }


        public bool IsShowAll
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }

        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                //_isShowAll = true;
                BFBinding();
                RaisePropertyChangedEvent("BFMasters");
                RaisePropertyChangedEvent("TypeList");
                ClearContent();
            }
        }
        public void ClearContent()
        {
            _Onhandbales = 0;
            _OnhandAmtUnit = 0;
            _OnhandAmt = 0;
            _Onhandweight = 0;
            RaisePropertyChangedEvent("OnhandBales");
            RaisePropertyChangedEvent("OnhandAmtUnit");
            RaisePropertyChangedEvent("OnhandAmt");
            RaisePropertyChangedEvent("OnhandWeight");
        }
        public string ToCrop
        {
            get { return _tocrop; }
            set { _tocrop = value; }
        }

        public IList<TypeInfo> TypeList
        {
            get { return PricingService.ShippingProductionRepository().GetTypeName(); }
            set { _typeList = value; }
        }

        public string Type
        {
            get { return _type; }
            set 
            { 
                _type = value;
                RaisePropertyChangedEvent("SubTypeList");
            }
        }
        public IList<SubTypeInfo> SubTypeList
        {
            get { return PricingService.BroughtForwardRepository().GetSubTypeName(); }
            set { _subtypeList = value; }
        }

        public string SubType
        {
            get { return _subtype; }
            set
            {
                _subtype = value;
                RaisePropertyChangedEvent("CompanyList");
            }
        }
        public IList<CompanyInfo> CompanyList
        {
            get { return PricingService.BroughtForwardRepository().GetCompanies(); }
            set { _companyList = value; }
        }

        public string Company
        {
            get { return _company; }
            set
            {
                _company = value;

                OnhandMat();
                RaisePropertyChangedEvent("OnhandBales");
                RaisePropertyChangedEvent("OnhandAmtUnit");
                RaisePropertyChangedEvent("OnhandAmt");
                RaisePropertyChangedEvent("OnhandWeight");
            }
        }
        public string Bfno
        {
            get { return _bfno; }
            set
            {
                _bfno = value;
            }
        }

        public int OnhandBales
        {
            get {  return _Onhandbales; }
            set { _Onhandbales = value; }
        }
        public decimal OnhandAmtUnit
        {
            get { return _OnhandAmtUnit; }
            set { _OnhandAmtUnit = value; }
        }

        public decimal OnhandAmt
        {
            get { return _OnhandAmt; }
            set { _OnhandAmt = value;  }
        }
        public decimal OnhandWeight
        {
            get { return _Onhandweight; }
            set { _Onhandweight = value; }
        }

        public void OnhandMat()
        {
            try
            {
                if (_crop == null || string.IsNullOrEmpty(_type) || string.IsNullOrEmpty(_subtype) || string.IsNullOrEmpty(_company)) return;
                _Onhandbales = 0;
                _OnhandAmtUnit = 0;
                _OnhandAmt = 0;
                _Onhandweight = 0;

                OnhandMatInfo t = PricingService.BroughtForwardRepository().GetOnhandMats(Convert.ToInt16(_crop), Convert.ToInt16(_company), _type, _subtype);

                if (t != null)
                {
                    _Onhandbales = Convert.ToInt32(t.onhand_bales);
                    _OnhandAmtUnit = Convert.ToDecimal(t.onhand_amt_unit);
                    _OnhandAmt = Convert.ToDecimal(t.onhand_amt);
                    _Onhandweight = Convert.ToDecimal(t.onhand_weight);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }
        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("GreenTransferMasters");
        }
        public ICommand CreateCommand
        {
            get { return _createdCommand ?? (_createdCommand = new RelayCommand(CreateNewBF)); }
            set { _createdCommand = value; }
        }
        public void CreateNewBF(object e)
        {
            if (string.IsNullOrEmpty(_crop)) throw new ArgumentException("Please fill Crop to Brougth Forward");

            if (_Onhandbales <= 0) throw new ArgumentException("Cannot Brought Forward zero onhand amount!!");

            _rcno = "";
            _bfno = "";
            CreateNewBroughtFW();
            RaisePropertyChangedEvent("ShowAllBFs");

        }

        public void CreateNewBroughtFW()
        {
            try
            {
                string userPS = Helper.SingletonConfiguration.getInstance().Username;
                //Create new matrcno ,matrc
                _rcno = PricingService.BroughtForwardRepository().InsertMatRCNo(Convert.ToInt16(_tocrop), userPS);

                if (string.IsNullOrEmpty(_rcno))
                    throw new ArgumentException("Please contact IT to create new Crop initial data");

                PricingService.BroughtForwardRepository().InsertMatRC(_rcno, Convert.ToInt16(_tocrop), _type, userPS);


                //Crate new matbfno, matbf1
                _bfno = PricingService.BroughtForwardRepository().InsertMatBF(Convert.ToInt16(_crop), Convert.ToInt16(_tocrop), userPS);


                if (string.IsNullOrEmpty(_bfno))
                    throw new ArgumentException("Cannot Add New Brought Forward");

                //Update mat (adding new barcode by adding 'BF' and update crop to _toCrop
                PricingService.BroughtForwardRepository().UpdateAndInsertMat(_rcno, Convert.ToInt16(_tocrop), Convert.ToInt16(_crop), _type, _subtype, Convert.ToInt16(_company), _bfno, userPS);

                //Add Docno
                //AddDocNoBySupplier(_rcno);
                //PricingService.BroughtForwardRepository().UpdateDocnoMat(_tocrop,_rcno, supplier,_subtype, Convert.ToInt16(_company));

                //Checker Locked
                PricingService.BroughtForwardRepository().CheckedLocked(_rcno, userPS);
                //[sp_Receiving_UPD_RCBFinished]
                PricingService.BroughtForwardRepository().FinishReceiving(_rcno);


                BFBinding();
                MessageBox.Show("Add data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        private void AddDocNoBySupplier(string rcno)
        {
            //Group by Supplier then for loop update docno
            //sp_Receiving_SEL_MatByRcNo
            PricingService.BroughtForwardRepository().GetMatByRCno(rcno);


            //PricingService.BroughtForwardRepository().UpdateDocnoMat(_tocrop,_rcno, supplier,_subtype, Convert.ToInt16(_company));
        }

        private void BFBinding()
        {
            try
            {
                var model = PricingService.BroughtForwardRepository()
                                .GetBroughtForwardByCrop(Convert.ToInt16(_crop), Convert.ToInt16(_tocrop));

                if (model == null)
                    throw new ArgumentException("Brought Forward Not Found." +
                        " This BF no not found, Please select another one to process.");

                RaisePropertyChangedEvent("BFMasters");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<BroughtForwardInfo> ShowAllBFs
        {
            get
            {
                var list = new List<BroughtForwardInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }

                var crop = Convert.ToInt16(_crop);

                var bfMaster = PricingService.BroughtForwardRepository().GetBroughtForwardByCrop(crop, Convert.ToInt16(_tocrop));

                list = bfMaster
                    .Select(a => new BroughtForwardInfo()
                    {
                        bfno = a.bfno,
                        tocrop = a.tocrop,
                        fromcrop = a.fromcrop,
                        type = a.type,
                        season = a.season,
                        company = a.company,
                        dtrecord = a.dtrecord
                    })
                    .ToList();

                _totalRecord = list.Count;
                RaisePropertyChangedEvent("TotalRecord");
                 return list;
            }

            set { _BFMasters = value; }
            }

        public IList<BroughtForwardInfo> BFMasters
        {
            get
            {
                var list = new List<BroughtForwardInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }
                var crop = Convert.ToInt16(_crop);
                var BroughtFMaster = PricingService
                    .BroughtForwardRepository()
                    .GetBroughtForwardByCrop(crop, Convert.ToInt16(_tocrop));

                list = BroughtFMaster
                    .Select(a => new BroughtForwardInfo()
                    {
                        bfno = a.bfno,
                        tocrop = a.tocrop,
                        fromcrop = a.fromcrop,
                        type = a.type,
                        season = a.season,
                        company = a.company,
                        dtrecord = a.dtrecord,
                        total_bales = a.total_bales
                    })
                    .ToList();
                

                _totalRecord = list.Count;
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _BFMasters = value; }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp();}
            set { _crops = value; }
        }

        public vm_BroughtForward()
        {
            RaisePropertyChangedEvent("Crops");
            _tocrop = Convert.ToString(DateTime.Now.Year + 1);
        }
        private ICommand _selectedRowCommand;

        public ICommand SelectedRowCommand
        {
            get { return _selectedRowCommand ?? (_selectedRowCommand = new RelayCommand(SelectedRow)); }
            set { _selectedRowCommand = value; }
        }

        private void SelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                var selectedItem = (BroughtForwardInfo)obj;
                _bfno = selectedItem.bfno;

                if (!string.IsNullOrEmpty(_bfno))
                {
                    Reports.RPT_BroughtF window = new Reports.RPT_BroughtF(_bfno);
                    window.ShowDialog();
                }              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
