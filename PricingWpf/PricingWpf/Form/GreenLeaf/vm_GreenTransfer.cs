﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_GreenTransfer : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private IList<GreenTransferMasterInfo> _greenTransferMasters;
        private string _crop;
        private bool _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _showNewAddingCommand;

        public ICommand ShowAllNewCommand
        {
            get { return _showNewAddingCommand ?? (_showNewAddingCommand = new RelayCommand(ShowAllNew)); }
            set { _showNewAddingCommand = value; }
        }
        public void ShowAllNew(object e)
        {
            _isShowAll = false;
            RaisePropertyChangedEvent("GreenTransferMasters");
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public void ShowDetails(object e)
        {
            GreenTransferDetails window = new GreenTransferDetails();
            try
            {
                var vm = new vm_GreenTransferDetails(window);
                window.DataContext = vm;
                vm.TransferNo = e.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                window.Close();
                return;
            }

            window.ShowDialog();
            RaisePropertyChangedEvent("GreenTransferMasters");
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }
        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("GreenTransferMasters");
        }


        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }


        public bool IsShowAll
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }


        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _isShowAll = true;
                RaisePropertyChangedEvent("GreenTransferMasters");
            }
        }


        public IList<GreenTransferMasterInfo> GreenTransferMasters
        {
            get
            {
                var list = new List<GreenTransferMasterInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }

                if (_isShowAll == true)
                    list = PricingService.GreenTransferRepository()
                        .GetJsonGreenTMaster(_crop);
                else
                {
                    var crop = Convert.ToInt16(_crop);
                    var GreenTransferMaster = PricingService
                        .GreenTransferRepository()
                        .GetJsonAddGreenTMaster(crop);

                    var filterCrop = GreenTransferMaster
                        .Where(a => a.Crop == crop)
                        .OrderBy(a => a.Transfer_No)
                        .ToList();

                    list = filterCrop
                        .Select(a => new GreenTransferMasterInfo()
                        {
                            Transfer_Doc_No = a.Transfer_Doc_No,
                            Transfer_No = a.Transfer_No,
                            Transfer_Doc_Date = a.Transfer_Doc_Date,
                            Crop = a.Crop,
                            Type = a.Type,
                            Season = a.Season,
                            Company = a.Company
                        })
                        .ToList();
                }

                _totalRecord = list.Count;
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _greenTransferMasters = value; }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp(); }
            set { _crops = value; }
        }

        public vm_GreenTransfer()
        {
            RaisePropertyChangedEvent("Crops");
        }
    }
}
