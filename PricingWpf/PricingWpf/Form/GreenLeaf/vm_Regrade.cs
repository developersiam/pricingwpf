﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_Regrade : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private IList<RegradeMasterInfo> _regradeMasters;
        private int? _crop;
        private bool _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _showAllNewCommand;

        public ICommand ShowAllNewCommand
        {
            get { return _showAllNewCommand ?? (_showAllNewCommand = new RelayCommand(ShowAllNew)); }
            set { _showAllNewCommand = value; }
        }
        public void ShowAllNew(object e)
        {
            _isShowAll = false;
            RaisePropertyChangedEvent("RegradeMasters");
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public void ShowDetails(object e)
        {
            RegradeDetails window = new RegradeDetails();
            try
            {
                var vm = new vm_RegradeDetails(window);
                window.DataContext = vm;
                vm.RegradeNo = e.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                window.Close();
                return;
            }
            
            window.ShowDialog();
            RaisePropertyChangedEvent("RegradeMasters");
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }
        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("RegradeMasters");
        }


        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }


        public bool IsShowAll
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }


        public int? Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _isShowAll = true;
                RaisePropertyChangedEvent("RegradeMasters");
            }
        }


        public IList<RegradeMasterInfo> RegradeMasters
        {
            get
            {
                var list = new List<RegradeMasterInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }

                if (_isShowAll == true)
                    list = PricingService.RegradeRepository()
                        .GetJsonRegradeMaster(Convert.ToInt16(_crop));
                else
                    list = PricingService.RegradeRepository()
                        .GetJsonAddRegradeMaster(Convert.ToInt16(_crop));

                _totalRecord = list.Count;
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _regradeMasters = value; }
        }


        public IList<CropReceiving> Crops
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp(); }
            set { _crops = value; }
        }

        public vm_Regrade()
        {
            RaisePropertyChangedEvent("Crops");
        }
    }
}
