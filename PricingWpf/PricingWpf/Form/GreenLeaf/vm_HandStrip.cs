﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_HandStrip : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private IList<HandStripMasterInfo> _handStripMasters;
        private int? _crop;
        private bool _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _showAllNewCommand;

        public ICommand ShowAllNewCommand
        {
            get { return _showAllNewCommand ?? (_showAllNewCommand = new RelayCommand(ShowAllNew)); }
            set { _showAllNewCommand = value; }
        }
        public void ShowAllNew(object e)
        {
            _isShowAll = false;
            RaisePropertyChangedEvent("HandStripMasters");
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public void ShowDetails(object e)
        {
            HandStripDetails window = new HandStripDetails();
            try
            {
                var vm = new vm_HandStripDetails(window);
                window.DataContext = vm;
                vm.HandStripNo = e.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                window.Close();
                return;
            }

            window.ShowDialog();
            RaisePropertyChangedEvent("HandStripMasters");
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }
        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("HandStripMasters");
        }


        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }


        public bool IsShowAll
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }


        public int? Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _isShowAll = true;
                RaisePropertyChangedEvent("HandStripMasters");
            }
        }


        public IList<HandStripMasterInfo> HandStripMasters
        {
            get
            {
                var list = new List<HandStripMasterInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }

                if (_isShowAll == true)
                    list = PricingService.HandStripRepository()
                        .GetJsonHandStripMaster(Convert.ToInt16(_crop));
                else
                    list = PricingService.HandStripRepository()
                        .GetJsonAddHandSMaster(Convert.ToInt16(_crop));

                _totalRecord = list.Count;
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _handStripMasters = value; }
        }


        public IList<CropReceiving> Crops
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp(); }
            set { _crops = value; }
        }

        public vm_HandStrip()
        {
            RaisePropertyChangedEvent("Crops");
        }
    }
}
