﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PricingWpf.Form.GreenLeaf
{
    /// <summary>
    /// Interaction logic for RegradeDetails.xaml
    /// </summary>
    public partial class RegradeDetails : Window
    {
        public RegradeDetails()
        {
            InitializeComponent();
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(DocumentNo.Text))
            {
                Reports.RPT_RC010 window = new Reports.RPT_RC010(DocumentNo.Text);
                window.ShowDialog();
            }
        }
    }
}
