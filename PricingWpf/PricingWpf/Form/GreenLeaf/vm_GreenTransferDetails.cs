﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_GreenTransferDetails : ObservableObject
    {
        private string _transferNo;
        private string _documentNo;
        private DateTime _documentDate;
        private int _crop;
        private string _type;
        private string _season;
        private string _companyCode;
        private double _totalAmount;
        private double _totalWeight;
        private double _averagePrice;
        private IList<PricingDomainModel.GreenTransferDetails> _greenTransferDetailsList;
        private ICommand _saveCommand;
        private ICommand _deleteCommand;
        private Visibility _isSaveButtonVisibility;
        private Visibility _isDeleteButtonVisibility;
        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public Visibility IsDeleteButtonVisibility
        {
            get { return _isDeleteButtonVisibility; }
            set { _isDeleteButtonVisibility = value; }
        }


        public Visibility IsSaveButtonVisibility
        {
            get { return _isSaveButtonVisibility; }
            set { _isSaveButtonVisibility = value; }
        }


        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }
        public void Delete(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this record?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.GreenTransferRepository()
                    .DeleteGreenT(_documentNo);

                GreenTransferBinding();
                MessageBox.Show("Delete data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }
        public void Save(object e)
        {
            try
            {
                _documentNo = PricingService.GreenTransferRepository()
                    .GetDocumentNo(_crop);

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.GreenTransferRepository()
                    .AddGreenTransfer(_documentNo,
                    _documentDate,
                    _transferNo,
                    _crop,
                    _companyCode,
                    _type,
                    _season,
                    Helper.SingletonConfiguration.getInstance().Username);

                GreenTransferBinding();
                MessageBox.Show("Add data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public IList<PricingDomainModel.GreenTransferDetails> GreenTransferDetailsList
        {
            get
            {
                var list = new List<PricingDomainModel.GreenTransferDetails>();

                list = PricingService.GreenTransferRepository()
                    .GetJsonGreenTDetails(_transferNo);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _greenTransferDetailsList = value; }
        }


        public double AveragePrice
        {
            get { return _averagePrice; }
            set { _averagePrice = value; }
        }


        public double TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }


        public double TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }


        public string Season
        {
            get { return _season; }
            set { _season = value; }
        }


        public string CompanyCode
        {
            get { return _companyCode; }
            set { _companyCode = value; }
        }


        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }


        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { _documentDate = value; }
        }


        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }


        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }


        public string TransferNo
        {
            get { return _transferNo; }
            set
            {
                _transferNo = value;
                GreenTransferBinding();
            }
        }

        public vm_GreenTransferDetails(Window window)
        {

        }

        private void GreenTransferBinding()
        {
            try
            {
                var model = PricingService.GreenTransferRepository()
                                .GetGreenTCalculate(_transferNo);

                if (model == null)
                    throw new ArgumentException("Green Transfer Not Found." +
                        " This Transfer no not found, Please select another one to process.");

                _documentNo = model.DocumentNo;
                _documentDate = model.DocumentDate;
                _crop = model.Crop;
                _type = model.Type;
                _season = model.Season;
                _companyCode = model.CompanyCode;
                _totalAmount = Convert.ToDouble(model.TotalAmount);
                _totalWeight = Convert.ToDouble(model.TotalWeight);
                _averagePrice = Convert.ToDouble(model.AveragePrice);

                _isDeleteButtonVisibility = model.DocumentNo == "" ? Visibility.Collapsed : Visibility.Visible;
                _isSaveButtonVisibility = model.DocumentNo == "" ? Visibility.Visible : Visibility.Collapsed;

                RaisePropertyChangedEvent("GreenTransferMasters");
                RaisePropertyChangedEvent("DocumentNo");
                RaisePropertyChangedEvent("IsSaveButtonVisibility");
                RaisePropertyChangedEvent("IsDeleteButtonVisibility");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
