﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_ReceivingDetails : ObservableObject
    {
        private string _receivingNo;
        private string _documentNo;
        private string _buyingDocNo;
        private DateTime _documentDate;
        private string _companyCode;
        private int _crop;
        private string _type;
        private string _season;
        private string _curerCode;
        private string _curerName;
        private string _area;
        private string _recordType;
        private string _greenPriceCode;
        private double _weightBuy;
        private double _weightReceived;
        private double _averagePriceBuy;
        private double _averagePriceRec;
        private double _totalAmount;
        private IList<PricingDomainModel.ReceivingDetails> _receivingDetailsList;
        private ICommand _saveCommand;
        private ICommand _deleteCommand;
        private Visibility _isDeleteButtonVisibility;
        private Visibility _isSaveButtonVisibility;
        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public Visibility IsSaveButtonVisibility
        {
            get { return _isSaveButtonVisibility; }
            set { _isSaveButtonVisibility = value; }
        }


        public Visibility IsDeleteButtonVisibility
        {
            get { return _isDeleteButtonVisibility; }
            set { _isDeleteButtonVisibility = value; }
        }


        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }

        public void Delete(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this record?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.ReceivingRepository()
                    .DeleteReceiving(_documentNo);

                ReceivingBinding();
                MessageBox.Show("Delete data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        public void Save(object e)
        {
            try
            {
                _documentNo = PricingService.ReceivingRepository()
                    .GetDocumentNo(_crop);

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.ReceivingRepository()
                    .AddReceiving(_documentNo,
                    _documentDate,
                    _receivingNo,
                    _buyingDocNo,
                    _crop,
                    _companyCode,
                    _type,
                    _season,
                    _curerCode,
                    _curerName,
                    _area,
                    _greenPriceCode,
                    _recordType,
                    Helper.SingletonConfiguration.getInstance().Username);

                ReceivingBinding();
                MessageBox.Show("Add data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public IList<PricingDomainModel.ReceivingDetails> ReceivingDetailsList
        {
            get
            {
                var list = new List<PricingDomainModel.ReceivingDetails>();

                if (_documentNo == "")
                    list = PricingService.ReceivingRepository()
                            .GetJsonAddReceivingDetails(Convert.ToInt32(_receivingNo), _crop);
                else
                    list = PricingService.ReceivingRepository()
                        .GetJsonReceivingDetails(_documentNo);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _receivingDetailsList = value; }
        }


        public double TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }


        public double AveragePriceRec
        {
            get { return _averagePriceRec; }
            set { _averagePriceRec = value; }
        }


        public double AveragePriceBuy
        {
            get { return _averagePriceBuy; }
            set { _averagePriceBuy = value; }
        }


        public double WeightReceived
        {
            get { return _weightReceived; }
            set { _weightReceived = value; }
        }


        public double WeightBuy
        {
            get { return _weightBuy; }
            set { _weightBuy = value; }
        }


        public string GreenPriceCode
        {
            get { return _greenPriceCode; }
            set { _greenPriceCode = value; }
        }


        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }


        public string Area
        {
            get { return _area; }
            set { _area = value; }
        }


        public string CurerName
        {
            get { return _curerName; }
            set { _curerName = value; }
        }


        public string CurerCode
        {
            get { return _curerCode; }
            set { _curerCode = value; }
        }


        public string Season
        {
            get { return _season; }
            set { _season = value; }
        }


        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }


        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }


        public string CompanyCode
        {
            get { return _companyCode; }
            set { _companyCode = value; }
        }


        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { _documentDate = value; }
        }


        public string BuyingDocNo
        {
            get { return _buyingDocNo; }
            set { _buyingDocNo = value; }
        }


        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }


        public string ReceivingNo
        {
            get { return _receivingNo; }
            set
            {
                _receivingNo = value;
                ReceivingBinding();
            }
        }

        public vm_ReceivingDetails(Window window)
        {

        }

        private void ReceivingBinding()
        {
            try
            {
                var model = PricingService.ReceivingRepository()
                        .GetReceivingCalculate(_receivingNo);

                if (model == null)
                    throw new ArgumentException("Receiving doc no not found!");

                _documentNo = model.DocumentNo;
                _buyingDocNo = model.BuyingDocNo;
                _documentDate = model.DocumentDate;
                _companyCode = model.CompanyCode;
                _crop = model.Crop;
                _type = model.Type;
                _season = model.Season;
                _curerCode = model.CurerCode;
                _curerName = model.CurerName;
                _area = model.Area;
                _recordType = model.RecordType;
                _greenPriceCode = model.GreenPriceCode;
                _weightBuy = model.WeightBuy;
                _weightReceived = model.WeightReceived;
                _averagePriceBuy = model.AveragePriceBuy;
                _averagePriceRec = model.AveragePriceRec;
                _totalAmount = model.TotalAmount;

                _isDeleteButtonVisibility = model.DocumentNo == "" ? Visibility.Collapsed : Visibility.Visible;
                _isSaveButtonVisibility = model.DocumentNo == "" ? Visibility.Visible : Visibility.Collapsed;

                RaisePropertyChangedEvent("ReceivingDetailsList");
                RaisePropertyChangedEvent("DocumentNo");
                RaisePropertyChangedEvent("IsDeleteButtonVisibility");
                RaisePropertyChangedEvent("IsSaveButtonVisibility");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
