﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.GreenLeaf
{
    public class vm_Receiving : ObservableObject
    {
        private IList<CropReceiving> _crops;
        private IList<ReceivingMasterInfo> _receivingMasters;
        private string _crop;
        private bool _isShowAll;
        private bool _isShowDetails;
        private int _totalRecord;
        private ICommand _showAllNewCommand;
        private ICommand _showDetailsCommand;
        private ICommand _showAllCommand;

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }
        public void ShowAll(object e)
        {
            _isShowAll = true;
            RaisePropertyChangedEvent("ReceivingMasters");
        }

        public ICommand ShowAllNewCommand
        {
            get { return _showAllNewCommand ?? (_showAllNewCommand = new RelayCommand(ShowAllNew)); }
            set { _showAllNewCommand = value; }
        }
        public void ShowAllNew(object e)
        {
            _isShowAll = false;
            RaisePropertyChangedEvent("ReceivingMasters");
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetails)); }
            set { _showDetailsCommand = value; }
        }
        public void ShowDetails(object e)
        {
            var window = new ReceivingDetails();
            var model = (ReceivingMasterInfo)e;

            try
            {
                var vm = new vm_ReceivingDetails(window);
                window.DataContext = vm;
                vm.ReceivingNo = model.C_REC_NO;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                window.Close();
                //return;

                if (MessageBox.Show("Receiving no. นี้ได้ปลดล็อคไปแล้ว แต่ยังคงค้างอยู่ในระบบ, ต้องการลบ Receiving เลยหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var documentNo = model.C_REC_DOC_NO;
                if (documentNo == null || documentNo == "") 
                    throw new ArgumentException("Document no. can not be null");

                PricingService.ReceivingRepository().DeleteReceiving(documentNo);
                MessageBox.Show("Delete data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            RaisePropertyChangedEvent("ReceivingMasters");
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public bool IsShowDetails
        {
            get { return _isShowDetails; }
            set { _isShowDetails = value; }
        }

        public bool IsShowAddNewRecord
        {
            get { return _isShowAll; }
            set { _isShowAll = value; }
        }

        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                _isShowAll = true;
                RaisePropertyChangedEvent("ReceivingMasters");
            }
        }

        public IList<ReceivingMasterInfo> ReceivingMasters
        {
            get
            {
                var list = new List<ReceivingMasterInfo>();
                if (_crop == null)
                {
                    _totalRecord = 0;
                    RaisePropertyChangedEvent("TotalRecord");
                    return list;
                }

                if (_isShowAll == false)
                    list = PricingService.ReceivingRepository()
                        .GetJsonAddReceivingMaster()
                        .Where(x => x.N_CROP == Convert.ToInt16(_crop))
                        .ToList();
                else
                    list = PricingService.ReceivingRepository()
                        .GetJsonReceivingMaster(_crop);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _receivingMasters = value; }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.ReceivingRepository().GetReceivingCorp(); }
            set { _crops = value; }
        }

        public vm_Receiving()
        {
            RaisePropertyChangedEvent("Crops");
        }

    }
}
