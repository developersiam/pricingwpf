﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_ExchangeRateDetail : ObservableObject
    {
        public ExchangeRateDetail _window { get; set; }
        private ExchangeRateInfo _exchangeRateInfo;
        private ICommand _updateDetailCommand;
        private string _exchangeRate;
        private Visibility _effDatePickerVisibility;
        private Visibility _effTextboxVisibility;
        private DateTime? _effDatePicker;

        public DateTime? EffDatePicker
        {
            get { return _effDatePicker; }
            set { _effDatePicker = value; }
        }

        public Visibility EffTextboxVisibility
        {
            get { return _exchangeRateInfo != null && _exchangeRateInfo.EffectiveDate != null ? Visibility.Visible : Visibility.Collapsed; }
            set { _effTextboxVisibility = value; }
        }

        public Visibility EffDatePickerVisibility
        {
            get { return _exchangeRateInfo == null || _exchangeRateInfo.EffectiveDate == null ? Visibility.Visible : Visibility.Collapsed; }
            set { _effDatePickerVisibility = value; }
        }

        public string ExchangeRate
        {
            get { return _exchangeRate; }
            set
            {
                _exchangeRate = FormatCorrector.FormatNumber(4, value, true);
            }
        }

        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_exchangeRate)) throw new Exception("Exchange rate cannot be empty.");
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var _effDate = _exchangeRateInfo.EffectiveDate;
                decimal exRate = Convert.ToDecimal(_exchangeRate);
                DateTime effDate = string.IsNullOrEmpty(_effDate) ? EffDatePicker.GetValueOrDefault() : Convert.ToDateTime(_effDate);
                //var formatEffDates = effDate.ToString("yyyy-MM-dd HH:mm:ss");

                PricingService.ExchangeRateRepository().SubmitExchangeRate(effDate, exRate, SingletonConfiguration.getInstance().Username);

                MessageBox.Show("Update success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ExchangeRateInfo ExchangeRateInfo
        {
            get { return _exchangeRateInfo; }
            set
            {
                _exchangeRateInfo = value;
                _exchangeRate = _exchangeRateInfo == null ? "" : _exchangeRateInfo.ExchangeRate.ToString();
            }
        }

        public vm_ExchangeRateDetail(ExchangeRateDetail window)
        {
            _window = window;
        }
    }
}
