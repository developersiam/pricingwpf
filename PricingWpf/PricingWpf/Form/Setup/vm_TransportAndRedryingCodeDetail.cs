﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    class vm_TransportAndRedryingCodeDetail : ObservableObject
    {
        private TransportAndRedryingCodeDetail _window;
        private string _packedGrade;
        private string _product;
        private string _type;
        private string _customer;
        private string _form;
        private string _netdef;
        private string _taredef;
        private string _grossdef;
        private string _gep;
        private string _gepBaht;
        private string _redryCode;
        private string _redryCharge;
        private string _transport;
        private int _crop;
        private string _transportCode;
        private string _price;
        private ICommand _updateDetailCommand;
        private ICommand _deleteDetailCommand;
        private IList<PackedGradeInfo> _packedGradeList;
        private IList<TransportationInfo> _transportList;
        private IList<RedryInfo> _redryList;
        private Visibility _visibleRedryCode;
        private Visibility _visibleRedryPrice;
        private Visibility _visiblePrice;

        public IList<TransportationInfo> TransportList
        {
            get
            {
                var list = new List<TransportationInfo>();

                list = PricingService.RedryCodeRepository().GetTransportation(_crop);
                return list;
            }
            set { _transportList = value; }
        }
        public IList<RedryInfo> RedryList
        {
            get
            {
                var list = new List<RedryInfo>();

                list = PricingService.RedryCodeRepository().GetRedrying(_crop);
                return list;
            }
            set
            {
                _redryList = value;
            }
        }
        public IList<PackedGradeInfo> PackedGradeList 
        {
            get
            {
                var list = new List<PackedGradeInfo>();

                list = PricingService.RedryCodeRepository().GetAllPackedGrade(_crop);
                return list;
            }
            set { _packedGradeList = value; }
        }

        public ICommand DeleteDetailCommand
        {
            get { return _deleteDetailCommand ?? (_deleteDetailCommand = new RelayCommand(DeleteDetail)); }
            set { _deleteDetailCommand = value; }
        }

        public void DeleteDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to delete setup?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                //PricingService.RedryCodeRepository().UpdateRedryCode(_crop, _packedGrade, _gep, _gepBaht, _transportCode, _redryCode, _price);
                //MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to Update change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                PricingService.RedryCodeRepository().UpdateRedryCode(_crop, _packedGrade, _gep, _gepBaht, _transportCode, _redryCode, _price);
                MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public string Price
        {
            get { return _price; }
            set { _price = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string TransportCodes
        {
            get { return _transportCode; }
            set
            {
                _transportCode = value;
                var list = PricingService.RedryCodeRepository().GetChargeTransportation(_crop, _transportCode);
                _transport = list.ToString();
                RaisePropertyChangedEvent("TransportCharge");
            }
        }

        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        public string TransportCharge
        {
            get { return _transport; }
            set { _transport = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string RedryCharge
        {
            get { return _redryCharge; }
            set
            {
                _redryCharge = FormatCorrector.FormatNumber(2, value, false);
            }
        }

        public string RedryCode
        {
            get { return _redryCode; }
            set
            {
                _redryCode = value;
                var m = PricingService.RedryCodeRepository().GetChargeRedrying(_crop, _redryCode);
                _redryCharge = m.ToString();

                RaisePropertyChangedEvent("RedryCharge");
            }
        }

        public string GepBaht
        {
            get { return _gepBaht; }
            set { _gepBaht = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string Gep
        {
            get { return _gep; }
            set { _gep = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string GrossDef
        {
            get { return _grossdef; }
            set { _grossdef = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string TareDef
        {
            get { return _taredef; }
            set { _taredef = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string NetDef
        {
            get { return _netdef; }
            set { _netdef = FormatCorrector.FormatNumber(2, value, false); }
        }

        public string Form
        {
            get { return _form; }
            set { _form = value; }
        }

        public string Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Product
        {
            get { return _product; }
            set { _product = value; }
        }

        public string PackedGrade
        {
            get { return _packedGrade; }
            set
            {
                _packedGrade = value;

                var m = PricingService.RedryCodeRepository().GetRedryCodeExisted(_crop, _packedGrade);

                _type = m.Type;
                _customer = m.Customer;
                _form = m.Form;
                _netdef = m.Netdef.ToString();
                _taredef = m.Taredef.ToString();
                _grossdef = m.Grossdef.ToString();
                _gep = m.Gep.ToString();
                _gepBaht = m.GepBaht.ToString();
                _redryCode = m.RedryCode.ToString();
                _redryCharge = m.RedryCharge.ToString();
                _transport = m.Transport.ToString();
                _transportCode = m.TranspostCode;
                _price = m.Price.ToString();

                RaisePropertyChangedEvent("Type");
                RaisePropertyChangedEvent("Customer");
                RaisePropertyChangedEvent("Form");
                RaisePropertyChangedEvent("NetDef");
                RaisePropertyChangedEvent("TareDef");
                RaisePropertyChangedEvent("GrossDef");
                RaisePropertyChangedEvent("Gep");
                RaisePropertyChangedEvent("GepBaht");
            }
        }

        public vm_TransportAndRedryingCodeDetail(TransportAndRedryingCodeDetail window)
        {
            _window = window;
        }
        public Visibility VisibleRedryCode
        {
            get
            {
                return _product == "By Product" ?
                    Visibility.Collapsed : Visibility.Visible;
            }
            set { _visibleRedryCode = value; }
        }
        public Visibility VisibleRedryCharge
        {
            get
            {
                return _product == "By Product" ?
                    Visibility.Collapsed : Visibility.Visible;
            }
            set { _visibleRedryPrice = value; }
        }
        public Visibility VisiblePrice
        {
            get
            {
                return _product == "Product" ?
                    Visibility.Collapsed : Visibility.Visible;
            }
            set { _visiblePrice = value; }
        }
    }
}
