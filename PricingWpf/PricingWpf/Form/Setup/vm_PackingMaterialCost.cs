﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_PackingMaterialCost : ObservableObject
    {
        private int? _crop;
        private IList<CropReceiving> _crops;
        private IList<PackingMatInfo> _packingMaster;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _deleteMasterCommand;
        private ICommand _updateDetailCommand;
        private string _packingMat;
        private string _price;
        private IList<PackingInfo> _packingMatList;
        private Visibility _visiblePackingMat;
        private ICommand _saveMasterCommand;

        public ICommand SaveMasterCommand
        {
            get { return _saveMasterCommand ?? (_saveMasterCommand = new RelayCommand(SaveMaster)); }
            set { _saveMasterCommand = value; }
        }
        public void SaveMaster(object e)
        {
            try
            {
                if (_packingMat == null)
                {
                    MessageBox.Show("Please select Packing Material First!", "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var iCrop = Convert.ToInt32(_crop);
                var user = SingletonConfiguration.getInstance().Username;

                PricingService.PackingMatRepository().SubmitPackingMat(iCrop, _packingMat, Convert.ToDecimal(_price));

                MessageBox.Show("Add new data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                RaisePropertyChangedEvent("PackingMaster");
                RaisePropertyChangedEvent("PackedMaterialList");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var packingMat = e.ToString();
                var model = PricingService.PackingMatRepository().GetPackingMat(Convert.ToInt32(_crop)).SingleOrDefault(s => s.Packing_Mat == packingMat);
                PricingService.PackingMatRepository().SubmitPackingMat(model.Crop, model.Packing_Mat, model.Price.Value);

                RaisePropertyChangedEvent("PackingMaster");
                MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //public ICommand DeleteMasterCommand
        //{
        //    get { return _deleteMasterCommand ?? (_deleteMasterCommand = new RelayCommand(DeleteMaster)); }
        //    set { _deleteMasterCommand = value; }
        //}

        //public void DeleteMaster(object e)
        //{
        //    try
        //    {
        //        if (MessageBox.Show("Do you confirm to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

        //        var packingMat = e.ToString();
        //        var model = PricingService.PackingMatRepository().GetPackingMat(Convert.ToInt32(_crop)).SingleOrDefault(s => s.Packing_Mat == packingMat);
        //        PricingService.PackingMatRepository().DeletePackingMat(model.Crop, model.Packing_Mat, model.Price.Value);

        //        MessageBox.Show("Delete data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
        //        RaisePropertyChangedEvent("PackingMaster");
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        //    }
        //}

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetail)); }
            set { _showDetailsCommand = value; }
        }

        public void ShowDetail(object e)
        {
            var window = new PackingMaterialCostDetail();
            var vm = new vm_PackingMaterialCostDetail(window);
            window.DataContext = vm;

            var packingMat = e.ToString();
            var model = PricingService.PackingMatRepository().GetPackingMat(Convert.ToInt32(_crop)).SingleOrDefault(s => s.Packing_Mat == packingMat);
            vm.PackingMat = model;

            window.ShowDialog();
            RaisePropertyChangedEvent("PackingMaster");
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }

        public void ShowAll(object e)
        {
            RaisePropertyChangedEvent("PackingMaster");
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public IList<PackingMatInfo> PackingMaster
        {
            get
            {
                var packingList = new List<PackingMatInfo>();
                if (_crop == null) return packingList;

                packingList = PricingService.PackingMatRepository().GetPackingMat(Convert.ToInt32(_crop));

                _totalRecord = packingList.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return packingList;
            }
            set { _packingMaster = value; }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.RedryCodeRepository().GetCorp(); }
            set { _crops = value; }
        }

        public int? Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                RaisePropertyChangedEvent("PackingMaster");
                RaisePropertyChangedEvent("PackedMaterialList");
            }
        }
        public string Packing_Mat
        {
            get { return _packingMat; }
            set { _packingMat = value; }
        }
        public string Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public IList<PackingInfo> PackedMaterialList
        {
            get
            {
                return PricingService.PackingMatRepository().GetPacking_Material(Convert.ToInt32(_crop));
            }
            set { _packingMatList = value; }
        }
        public Visibility VisiblePackingMat
        {
            get
            {
                return Visibility.Collapsed;
                //if (_exportValueType == true)
                //{
                //    RaisePropertyChangedEvent("PackedList");
                //    return Visibility.Visible;
                //}
                //else
                //{
                //    return Visibility.Collapsed;
                //}
            }
            set { _visiblePackingMat = value; }
        }

    }
}
