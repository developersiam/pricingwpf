﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_PackingMaterialCostDetail : ObservableObject
    {
        public PackingMaterialCostDetail _window { get; set; }
        private PackingMatInfo _packingMat;
        private ICommand _updateDetailCommand;
        private string _price;

        public string Price
        {
            get { return _price; }
            set { _price = FormatCorrector.FormatNumber(2, value, false); }
        }


        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var model = _packingMat;
                model.Price = Convert.ToDecimal(_price);
                PricingService.PackingMatRepository().SubmitPackingMat(model.Crop, model.Packing_Mat, model.Price.Value);

                MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                //_window.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public PackingMatInfo PackingMat
        {
            get { return _packingMat; }
            set
            {
                _packingMat = value;
                _price = _packingMat.Price == null ? "" : _packingMat.Price.ToString();
            }
        }

        public vm_PackingMaterialCostDetail(PackingMaterialCostDetail window)
        {
            _window = window;
        }
    }
}
