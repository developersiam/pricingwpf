﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_ExchangeRate : ObservableObject
    {
        private DateTime? _effectiveDate;
        private IList<ExchangeRateInfo> _exchangeRateMaster;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _deleteMasterCommand;
        private ICommand _updateDetailCommand;
        private ICommand _addMasterCommand;

        public ICommand AddMasterCommand
        {
            get { return _addMasterCommand ?? (_addMasterCommand = new RelayCommand(AddMaster)); }
            set { _addMasterCommand = value; }
        }

        public void AddMaster(object e)
        {
            var window = new ExchangeRateDetail();
            var vm = new vm_ExchangeRateDetail(window);
            window.DataContext = vm;

            var model = new ExchangeRateInfo();
            vm.ExchangeRateInfo = model;

            window.ShowDialog();
            RaisePropertyChangedEvent("ExchangeRateMaster");
        }


        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var effectiveDate = Convert.ToDateTime(e.ToString());
                var formatEffDate = effectiveDate.ToString("dd/MM/yyyy");
                var model = PricingService.ExchangeRateRepository().GetExchangeRate(effectiveDate).SingleOrDefault(s => s.EffectiveDate == formatEffDate);
                PricingService.ExchangeRateRepository().SubmitExchangeRate(effectiveDate, model.ExchangeRate, SingletonConfiguration.getInstance().Username);

                MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                RaisePropertyChangedEvent("ExchangeRateMaster");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand DeleteMasterCommand
        {
            get { return _deleteMasterCommand ?? (_deleteMasterCommand = new RelayCommand(DeleteMaster)); }
            set { _deleteMasterCommand = value; }
        }

        public void DeleteMaster(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var effectiveDate = Convert.ToDateTime(e.ToString());
                var model = PricingService.ExchangeRateRepository().GetExchangeRate(effectiveDate).SingleOrDefault(s => s.EffecctiveDatetime == effectiveDate);
                if (model == null) throw new Exception(e.ToString() + " information not found");
                PricingService.ExchangeRateRepository().DeleteExchangeRate(model.EffecctiveDatetime, model.ExchangeRate);

                MessageBox.Show("Delete data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                RaisePropertyChangedEvent("ExchangeRateMaster");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetail)); }
            set { _showDetailsCommand = value; }
        }

        public void ShowDetail(object e)
        {
            try
            {
                var window = new ExchangeRateDetail();
                var vm = new vm_ExchangeRateDetail(window);
                window.DataContext = vm;
                
                var effectiveDate = Convert.ToDateTime(e.ToString());
                var model = PricingService.ExchangeRateRepository().GetExchangeRate(effectiveDate).SingleOrDefault(s => s.EffecctiveDatetime == effectiveDate);
                if (model == null) throw new Exception(e.ToString() + " information not found");
                vm.ExchangeRateInfo = model;

                window.ShowDialog();
                RaisePropertyChangedEvent("ExchangeRateMaster");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }

        public void ShowAll(object e)
        {
            RaisePropertyChangedEvent("ExchangeRateMaster");
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public IList<ExchangeRateInfo> ExchangeRateMaster
        {
            get
            {
                var exchangeRateList = new List<ExchangeRateInfo>();
                if (_effectiveDate == null) return exchangeRateList;

                exchangeRateList = PricingService.ExchangeRateRepository().GetExchangeRate(_effectiveDate.Value);

                _totalRecord = exchangeRateList.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return exchangeRateList;
            }
            set { _exchangeRateMaster = value; }
        }

        public DateTime? EffectiveDate
        {
            get { return _effectiveDate; }
            set
            {
                _effectiveDate = value;
                RaisePropertyChangedEvent("ExchangeRateMaster");
            }
        }

    }
}
