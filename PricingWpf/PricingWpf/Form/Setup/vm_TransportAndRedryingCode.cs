﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_TransportAndRedryingCode : ObservableObject
    {
        private string _crop;
        private IList<CropReceiving> _crops;
        private int _type;
        private IList<SelectType> _types;
        private IList<RedryCodeInfo> _redryCodeMaster;
        private int _totalRecord;
        private ICommand _showAllCommand;
        private ICommand _showDetailsCommand;
        private ICommand _deleteMasterCommand;
        private ICommand _addNewCommand;


        public ICommand AddNewCommand
        {
            get { return _addNewCommand ?? (_addNewCommand = new RelayCommand(AddNew)); }
            set { _addNewCommand = value; }
        }
        public void AddNew(object e)
        {
            var window = new TransportAndRedryingCodeDetail();
            var vm = new vm_TransportAndRedryingCodeDetail(window);
            window.DataContext = vm;

            vm.Crop = Convert.ToInt32(_crop);
            vm.Product = _type == 0 ? "Product" : "By Product";
            //vm.PackedGrade = e.ToString();

            window.ShowDialog();
            RaisePropertyChangedEvent("RedryCodeMaster");
        }

        public ICommand DeleteMasterCommand
        {
            get { return _deleteMasterCommand ?? (_deleteMasterCommand = new RelayCommand(DeleteMaster)); }
            set { _deleteMasterCommand = value; }
        }

        public void DeleteMaster(object e)
        {
            throw new NotImplementedException();
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetail)); }
            set { _showDetailsCommand = value; }
        }

        public void ShowDetail(object e)
        {
            var window = new TransportAndRedryingCodeDetail();
            var vm = new vm_TransportAndRedryingCodeDetail(window);
            window.DataContext = vm;

            vm.Crop = Convert.ToInt32(_crop);
            vm.Product = _type == 0 ? "Product" : "By Product";
            vm.PackedGrade = e.ToString();

            window.ShowDialog();
            RaisePropertyChangedEvent("RedryCodeMaster");
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }

        public void ShowAll(object e)
        {
            RaisePropertyChangedEvent("RedryCodeMaster");
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public IList<RedryCodeInfo> RedryCodeMaster
        {
            get
            {
                var redryCodeList = new List<RedryCodeInfo>();
                if(_crop == null) return redryCodeList;
                if (_type == 0) redryCodeList = PricingService.RedryCodeRepository().GetJsonRedryCodeMaster(_crop);
                else if (_type == 1) redryCodeList = PricingService.RedryCodeRepository().GetJsonRedryCodeByProduct(_crop);

                _totalRecord = redryCodeList.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return redryCodeList;
            }
            set { _redryCodeMaster = value; }
        }

        public IList<SelectType> Types
        {
            get
            {
                var list = new List<SelectType>();
                list.Add(new SelectType { typeID = 0, description = "Product" });
                list.Add(new SelectType { typeID = 1, description = "By Product" });
                return list;
            }
            set { _types = value; }
        }

        public int Type
        {
            get { return _type; }
            set
            {
                _type = value;
                RaisePropertyChangedEvent("RedryCodeMaster");
            }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.RedryCodeRepository().GetCorp(); }
            set { _crops = value; }
        }

        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                RaisePropertyChangedEvent("RedryCodeMaster");
            }
        }

    }
}
