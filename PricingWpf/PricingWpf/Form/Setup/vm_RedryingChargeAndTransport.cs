﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_RedryingChargeAndTransport : ObservableObject
    {
        private IList<CropReceiving> _crops;
        public string _crop;
        private IList<SelectType> _types;
        public int? _type;
        private ICommand _showAllCommand;
        private IList<RedryChargeInfo> _redryMasters;
        private IList<TransportChargeInfo> _transMasters;
        private int _totalRecord;
        private Visibility _redryMasterVisibility;
        private Visibility _transMasterVisibility;
        private ICommand _showDetailsCommand;
        private ICommand _deleteMasterCommand;
        private ICommand _saveMasterCommand;
        private string _code;
        private string _description;
        private string _unitCost;


        public ICommand SaveMasterCommand
        {
            get { return _saveMasterCommand ?? (_saveMasterCommand = new RelayCommand(SaveMaster)); }
            set { _saveMasterCommand = value; }
        }
        public void SaveMaster(object e)
        {
            try
            {
                var iCrop = Convert.ToInt32(_crop);
                //var code = e.ToString();
                
                var type = _type == 0 ? "redry" : "transport";
                var user = SingletonConfiguration.getInstance().Username;

                PricingService.RedryChargeRepository().SubmitRedryTransport(iCrop, _code, type, _description, Convert.ToDecimal(_unitCost), user);
                
                MessageBox.Show("Add new data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                RaisePropertyChangedEvent("RedryMasters");
                RaisePropertyChangedEvent("TransMasters");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public ICommand DeleteMasterCommand
        {
            get { return _deleteMasterCommand ?? (_deleteMasterCommand = new RelayCommand(DeleteMaster)); }
            set { _deleteMasterCommand = value; }
        }

        public void DeleteMaster(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var iCrop = Convert.ToInt32(_crop);
                var code = e.ToString();
                var type = _type == 0 ? "redry" : "transport";
                PricingService.RedryChargeRepository().DeleteRedryCharge(iCrop, code, type);

                MessageBox.Show("Delete data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                RaisePropertyChangedEvent("RedryMasters");
                RaisePropertyChangedEvent("TransMasters");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public ICommand ShowDetailsCommand
        {
            get { return _showDetailsCommand ?? (_showDetailsCommand = new RelayCommand(ShowDetail)); }
            set { _showDetailsCommand = value; }
        }

        public void ShowDetail(object e)
        {
            var window = new RedryingChargeAndTransportDetail();
            var vm = new vm_RedryingChargeAndTransportDetail(window);
            window.DataContext = vm;

            vm.Crop = _crop;
            if (_type == 0) vm.RedryChargeCode = e.ToString();
            if (_type == 1) vm.TransChargeCode = e.ToString();
            //vm.ShippingNo = e.ToString();
            window.ShowDialog();
            RaisePropertyChangedEvent("RedryMasters");
            RaisePropertyChangedEvent("TransMasters");
        }

        public Visibility TransMasterVisibility
        {
            get { return _type == 1 ? Visibility.Visible : Visibility.Collapsed; }
            set { _transMasterVisibility = value; }
        }

        public Visibility RedryMasterVisibility
        {
            get { return _type == 0 ? Visibility.Visible : Visibility.Collapsed; }
            set { _redryMasterVisibility = value; }
        }

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        public string Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                RaisePropertyChangedEvent("RedryMasters");
                RaisePropertyChangedEvent("TransMasters");
            }
        }

        public IList<CropReceiving> Crops
        {
            get { return PricingService.RedryChargeRepository().GetCorp(); }
            set { _crops = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = value; }
        }

        public int? Type
        {
            get { return _type; }
            set
            {
                _type = value;
                RaisePropertyChangedEvent("RedryMasters");
                RaisePropertyChangedEvent("TransMasters");
            }
        }

        public IList<SelectType> Types
        {
            get
            {
                var list = new List<SelectType>();
                list.Add(new SelectType { typeID = 0, description = "Redrying Charge" });
                list.Add(new SelectType { typeID = 1, description = "Transportation" });
                return list;
            }
            set { _types = value; }
        }

        public ICommand ShowAllCommand
        {
            get { return _showAllCommand ?? (_showAllCommand = new RelayCommand(ShowAll)); }
            set { _showAllCommand = value; }
        }

        public void ShowAll(object e)
        {
            RaisePropertyChangedEvent("RedryMasters");
            RaisePropertyChangedEvent("TransMasters");
        }

        public IList<RedryChargeInfo> RedryMasters
        {
            get
            {
                var redryList = PricingService.RedryChargeRepository().GetRedry_M(Convert.ToInt32(_crop));
                _totalRecord = redryList.Count();
                RaisePropertyChangedEvent("TotalRecord");
                RaisePropertyChangedEvent("RedryMasterVisibility");
                return redryList;
            }
            set { _redryMasters = value; }
        }

        public IList<TransportChargeInfo> TransMasters
        {
            get
            {
                var transList = PricingService.RedryChargeRepository().GetTransport_M(Convert.ToInt32(_crop));
                _totalRecord = transList.Count();
                RaisePropertyChangedEvent("TotalRecord");
                RaisePropertyChangedEvent("TransMasterVisibility");
                return transList;
            }
            set { _transMasters = value; }
        }
    }

    public class SelectType
    {
        public int typeID { get; set; }
        public string description { get; set; }
    }

}
