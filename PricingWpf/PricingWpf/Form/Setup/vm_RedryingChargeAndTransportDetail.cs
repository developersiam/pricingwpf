﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Setup
{
    public class vm_RedryingChargeAndTransportDetail : ObservableObject
    {
        private string _redryChargeCode;
        private string _transChargeCode;
        private string _crop;
        private string _code;
        private string _description;
        private string _unitCost;
        private string _type;
        private ICommand _updateDetailCommand;
        private RedryingChargeAndTransportDetail _window;

        public ICommand UpdateDetailCommand
        {
            get { return _updateDetailCommand ?? (_updateDetailCommand = new RelayCommand(UpdateDetail)); }
            set { _updateDetailCommand = value; }
        }

        public void UpdateDetail(object e)
        {
            try
            {
                if (MessageBox.Show("Do you confirm to save change?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var iCrop = Convert.ToInt32(_crop);
                var charge = Convert.ToDecimal(_unitCost);
                var user = SingletonConfiguration.getInstance().Username;
                if (_type == "redry") PricingService.RedryChargeRepository().UpdateRedrying(iCrop, _code, _type, _description, charge, user);
                else { PricingService.RedryChargeRepository().UpdateTransportation(iCrop, _code, _type, _description, charge, user); }

                MessageBox.Show("Update data success!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = FormatCorrector.FormatNumber(2, value, false); }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }


        public string Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }


        public string TransChargeCode
        {
            get { return _transChargeCode; }
            set
            {
                _transChargeCode = value;

                var model = PricingService.RedryChargeRepository().GetTransport_M(Convert.ToInt32(_crop)).SingleOrDefault(s => s.Trans_Charge_Code == _transChargeCode);

                _type = "transport";
                _code = value;
                _description = model.Trans_Charge_Description;
                _unitCost = model.Charge.ToString();
            }
        }


        public string RedryChargeCode
        {
            get { return _redryChargeCode; }
            set
            {
                _redryChargeCode = value;
                
                var model = PricingService.RedryChargeRepository().GetRedry_M(Convert.ToInt32(_crop)).SingleOrDefault(s => s.Redry_Charge_Code == _redryChargeCode);

                _type = "redry";
                _code = value;
                _description = model.Redry_Charge_Description;
                _unitCost = model.Charge.ToString();
            }
        }


        public vm_RedryingChargeAndTransportDetail(RedryingChargeAndTransportDetail window)
        {
            _window = window;
        }
    }
}
