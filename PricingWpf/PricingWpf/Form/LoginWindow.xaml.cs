﻿using PricingBusinessLayer;
using PricingWpf.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PricingWpf.Form
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            UsernameTextbox.Focus();
        }

        private void UsernameTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            var username = UsernameTextbox.Text;
            UsernameTextbox.Text = FormatCorrector.FirstCapital(username);
        }

        private void LoginTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) LoginProcess();
        }

        private void PasswordTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordTextbox.Password = string.Empty;
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            //Login();
            LoginProcess();
        }

        private void Login()
        {
            try
            {
                var username = FormatCorrector.FirstCapital(UsernameTextbox.Text);
                if (username == null) UsernameTextbox.Focus();
                var password = PasswordTextbox.Password;
                if (password == null) PasswordTextbox.Focus();
                if (username != null && password != null)
                {
                    var userinfo = PricingService.AccountRepository().GetAuthentification(username, password);

                    if (userinfo != null)
                    {
                        var _singleton = SingletonConfiguration.getInstance();
                        _singleton.Username = FormatCorrector.FirstCapital(username);
                        _singleton.LoggedIn = DateTime.Now;

                        MainWindow w = new MainWindow(username);
                        w.Title = string.Format("Pricing - [{0}]", _singleton.Username);
                        w.ShowDialog();
                    }
                    else throw new Exception("ไม่สามารถเข้าใช้งานได้");
                }
                else throw new Exception("Please enter username and password.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            UsernameTextbox.Text = string.Empty;
            PasswordTextbox.Password = string.Empty;
        }

        private void UsernameTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            UsernameTextbox.Text = string.Empty;
        }

        private void LoginProcess()
        {
            try
            {
                if (UsernameTextbox.Text.Length <= 0)
                {
                    MessageBoxHelper.Warning("โปรดกรอกชื่อผู้ใช้");
                    return;
                }
                if (PasswordTextbox.Password.Length <= 0)
                {
                    MessageBoxHelper.Warning("โปรดกรอกรหัสผ่าน");
                    return;
                }

                if (UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextbox.Text) == false)
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ โปรดตรวจสอบกับทางแผนกไอที");

                if (UserAccountHelper.ActiveDirectoryAuthenticate(UsernameTextbox.Text, PasswordTextbox.Password) == false)
                    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                var userAccount = PricingService.AccountRepository().GetPricingAuthentification();

                var RoleAccess = userAccount.Select(x => x.Username == UsernameTextbox.Text);
                if (RoleAccess == null)
                    throw new ArgumentException("บัญชีผู้ใช้งานของท่านยังไม่ได้ลงทะเบียนเข้าใช้งานระบบ โปรดติดต่อแผนกไอทีเพื่อทำการเพิ่มสิทธิ์การใช้งานระบบ");

                /// store login information.
                var _singleton = SingletonConfiguration.getInstance();
                _singleton.Username = FormatCorrector.FirstCapital(UsernameTextbox.Text);
                _singleton.LoggedIn = DateTime.Now;

                MainWindow w = new MainWindow(UsernameTextbox.Text);
                w.Title = string.Format("Pricing - [{0}]", _singleton.Username);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }
    }
}
