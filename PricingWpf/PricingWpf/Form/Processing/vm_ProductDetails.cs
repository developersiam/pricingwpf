﻿using PricingBusinessLayer;
using PricingDomainModel;
using PricingWpf.Helper;
using PricingWpf.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PricingWpf.Form.Processing
{
    public class vm_ProductDetails : ObservableObject
    {
        private string _processingR_No;
        private string _documentNo;
        private DateTime _documentDate;
        private int _crop;
        private string _type;
        private string _season;
        private string _packedGrade;
        private decimal _totalWeight;
        private string _packedWeight;
        private string _redryCharge;
        private string _packMat;
        private string _transportation;
        private decimal _totalAmount;
        private string _weight;
        private decimal _averagePrice;
        private IList<ProcessReadyDetails> _productDetailsList;
        private ICommand _saveCommand;
        private ICommand _deleteCommand;
        private Visibility _isSaveButtonVisibility;
        private Visibility _isDeleteButtonVisibility;
        private int _totalRecord;


        public string Transportation
        {
            get { return _transportation; }
            set { _transportation = FormatCorrector.FormatNumber(2, value, true); }
        }


        public string PackMat
        {
            get { return _packMat; }
            set { _packMat = FormatCorrector.FormatNumber(2, value, true); }
        }


        public string RedryCharge
        {
            get { return _redryCharge; }
            set { _redryCharge = FormatCorrector.FormatNumber(2, value, true); }
        }


        public string PackedWeight
        {
            get { return _packedWeight; }
            set { _packedWeight = FormatCorrector.FormatNumber(2, value, true); }
        }


        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }


        public string Weight
        {
            get { return _weight; }
            set { _weight = FormatCorrector.FormatNumber(2, value, true); }
        }

        public decimal AveragePrice
        {
            get { return _averagePrice; }
            set { _averagePrice = value; }
        }


        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }


        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }


        public Visibility IsDeleteButtonVisibility
        {
            get { return _isDeleteButtonVisibility; }
            set { _isDeleteButtonVisibility = value; }
        }


        public Visibility IsSaveButtonVisibility
        {
            get { return _isSaveButtonVisibility; }
            set { _isSaveButtonVisibility = value; }
        }


        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }
        public void Delete(object e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this record?", "warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.ProcessReadyRepository()
                    .DeleteProcessReady(_documentNo);

                ProductDetailsBinding();
                MessageBox.Show("Delete data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }
        public void Save(object e)
        {
            try
            {
                _documentNo = PricingService.ProcessReadyRepository()
                    .GetDocumentNo(_crop);

                if (_documentNo == null || _documentNo == "")
                    throw new ArgumentException("Document no. can not be null");

                PricingService.ProcessReadyRepository()
                    .AddProcessReady(_documentNo,
                    _documentDate,
                    _processingR_No,
                    _crop,
                    _type,
                    _season,
                    _packedGrade,
                    "",
                    Convert.ToDecimal(_packMat),
                    "",
                   Convert.ToDecimal(_transportation),
                    "",
                    Convert.ToDecimal(_redryCharge),
                    Convert.ToDecimal(_weight),
                   Convert.ToDecimal(_packedWeight),
                    SingletonConfiguration.getInstance().Username);

                ProductDetailsBinding();
                MessageBox.Show("Add data success!", "info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public IList<ProcessReadyDetails> ProductDetailsList
        {
            get
            {
                var list = PricingService.ProcessReadyRepository()
                    .GetJsonProcessRDetails(_processingR_No);

                _totalRecord = list.Count();
                RaisePropertyChangedEvent("TotalRecord");
                return list;
            }
            set { _productDetailsList = value; }
        }


        public string Season
        {
            get { return _season; }
            set { _season = value; }
        }


        public string PackedGrade
        {
            get { return _packedGrade; }
            set { _packedGrade = value; }
        }


        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }


        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { _documentDate = value; }
        }


        public int Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }


        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }

        public string ProcessingR_No
        {
            get { return _processingR_No; }
            set
            {
                _processingR_No = value;
                ProductDetailsBinding();
            }
        }

        public vm_ProductDetails(Window window)
        {

        }

        private void ProductDetailsBinding()
        {
            try
            {
                var model = PricingService.ProcessReadyRepository()
                        .GetProcessReadyCalculate(_processingR_No);

                if (model == null)
                    throw new ArgumentException("Processing Product Not Found." +
                        " This Processing Product no not found, Please select another one to process.");

                _documentNo = model.Doc_No;
                _documentDate = model.Doc_Date;
                _crop = model.Crop;
                _type = model.Type;
                _season = model.Season;
                _packedGrade = model.Packed_Grade;
                _packedWeight = model.Packed_Weight.ToString();
                _weight = model.Weight.ToString();
                _redryCharge = model.Redry_Charge.ToString();
                _packMat = model.Pack_Mat.ToString();
                _transportation = model.Transportation.ToString();
                _totalAmount = model.Total_Amt;
                _totalWeight = model.Total_Weight;
                _averagePrice = model.Average_Price;

                _isDeleteButtonVisibility = model.Doc_No == "" ? Visibility.Collapsed : Visibility.Visible;
                _isSaveButtonVisibility = model.Doc_No == "" ? Visibility.Visible : Visibility.Collapsed;

                RaisePropertyChangedEvent("ProductDetailsList");
                RaisePropertyChangedEvent("DocumentNo");
                RaisePropertyChangedEvent("IsSaveButtonVisibility");
                RaisePropertyChangedEvent("IsDeleteButtonVisibility");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
