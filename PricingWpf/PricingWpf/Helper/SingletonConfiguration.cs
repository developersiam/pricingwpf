﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingWpf.Helper
{
    class SingletonConfiguration
    {
        private static SingletonConfiguration instance = new SingletonConfiguration();

        public string Username { get; set; }
        public DateTime LoggedIn { get; set; }
        public string permissionLevel { get; set; }
        public string singleton_employee_ID { get; set; }

        private SingletonConfiguration() { }

        public static SingletonConfiguration getInstance()
        {
            return instance;
        }
    }
}
