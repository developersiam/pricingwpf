﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PricingWpf.Helper
{
    public static class FormatCorrector
    {
        public static string FirstCapital(string inputString)
        {
            inputString = inputString.Trim();
            inputString = inputString.ToLower();
            if (string.IsNullOrEmpty(inputString)) return string.Empty;
            // convert to char array of the string
            char[] letters = inputString.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }

        public static string FormatNumber(int decimalDigit, string inputString, bool Nullable)
        {
            // Digit 0 for Integer type
            inputString = inputString.Trim();
            Regex regex = new Regex(@"^[0-9]+(\.[0-9]{1,6})?$");
            Match match = regex.Match(inputString);
            if (match.Success)
            {
                decimal x = decimal.Parse(inputString);
                x = Math.Round(x, decimalDigit);
                return x.ToString();
            }
            else if (Nullable) return null;
            else return "0";
        }
    }
}
