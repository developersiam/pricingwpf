﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace PricingBusinessLayer
{
    public enum SORT_ORDER
    {
        ASCENDING = 0,
        DESCENDING = 1
    }
    public class GenericDataRepository : IDisposable
    {        
        public void Dispose()
        {
            throw new NotImplementedException();
        }
        protected Dictionary<string, string>[] ExecuteCommand(SqlCommand command, string[] columns)
        {
            try
            {
                var connection = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (var conn = new SqlConnection(connection))
                {
                    List<Dictionary<string, string>> results = new List<Dictionary<string, string>>();
                    command.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var row = new Dictionary<string, string>();
                            foreach (var c in columns)
                            {
                                row.Add(c, reader[c].ToString());
                            }
                            results.Add(row);
                        }
                    }
                    conn.Close();
                    return results.ToArray();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected List<T> ExecuteCommandAsType<T>(SqlCommand cmd, List<Dictionary<string, object>> rawResult = null) where T : new()
        {
            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            var _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var conn = new SqlConnection(_connectionString))
            {
                cmd.Connection = conn;
                cmd.CommandTimeout = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["BackendConnectionTimeout"]);
                conn.Open();

                var reader = cmd.ExecuteReader();
                var columns = new List<string>();

                for (int i = 0; i < reader.FieldCount; i++)
                    columns.Add(reader.GetName(i));

                while (reader.Read())
                {
                    var row = new Dictionary<string, object>();
                    for (int i = 0; i < columns.Count; i++)
                        row.Add(columns[i], reader[columns[i]]);
                    results.Add(row);
                }

                reader.Close();

                conn.Close();
            }
            if (rawResult != null)
                rawResult = results;
            return FillResultToObject<T>(results);
        }
        private List<T> FillResultToObject<T>(List<Dictionary<string, object>> results) where T : new()
        {
            List<T> modelList = new List<T>();

            T model = new T();
            if (model.GetType().IsClass)
            {
                foreach (var result in results)
                {
                    foreach (var prop in model.GetType().GetProperties())
                    {
                        if (result.ContainsKey(prop.Name))
                        {
                            Type t = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                            try
                            {
                                object safeValue;
                                if (t.GetTypeInfo().IsEnum)
                                    safeValue = result[prop.Name] == DBNull.Value ? null : Enum.Parse(t, ((int)result[prop.Name]).ToString());
                                else
                                    safeValue = result[prop.Name] == DBNull.Value ? null : Convert.ChangeType(result[prop.Name], t);
                                if (t == typeof(DateTime))
                                {
                                    safeValue = ((DateTime)safeValue).ToUniversalTime();
                                }
                                prop.SetValue(model, safeValue);
                            }
                            catch { }
                        }
                    }
                    modelList.Add(model);
                    model = new T();
                }
            }
            else
            {
                foreach (var result in results)
                {
                    var value = result.FirstOrDefault().Value;
                    object safeValue = value == DBNull.Value ? null : Convert.ChangeType(value, model.GetType());
                    modelList.Add((T)safeValue);
                }
            }

            return modelList;
        }
    }
}