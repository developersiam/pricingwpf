﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using PricingDomainModel;
using System.Data;

namespace PricingBusinessLayer
{
    public class ReportRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public List<vm_rptrc007> GetReceivingReport(string rc_docno)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("SP_RPT_RC007") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@c_rec_doc_no", rc_docno));
                var data = ExecuteCommand(command, new string[] { "Doc_No", "Receiving_Doc_Date", "Receiving_Doc_No", "Buying_Doc_No", "Receiving_Crop", "Receiving_Comp",
                                        "Receiving_Type", "Receiving_Season", "Receiving_Curer","Receiving_Curer_Name", "Green_Grade", "Bales_no",
                                         "BuyWeight", "BuyUnitPrice", "BuyTransprotChrge","BuyTotalUnitPrice", "RecWeight", "RecUnitPrice", "Amount", "Classify"});
                if (!data.Any())
                    return null;

                List<vm_rptrc007> lstRC = new List<vm_rptrc007>();
                foreach (var r in data)
                {
                    lstRC.Add(new vm_rptrc007()
                    {
                        Doc_No = r["Doc_No"],
                        Receiving_Doc_Date = DateTime.Parse(r["Receiving_Doc_Date"]),
                        Receiving_Doc_No = r["Receiving_Doc_No"],
                        Buying_Doc_No = r["Buying_Doc_No"],
                        Receiving_Crop = Int16.Parse(r["Receiving_Crop"]),
                        Receiving_Comp = r["Receiving_Comp"],
                        Receiving_Type = r["Receiving_Type"],
                        Receiving_Season = r["Receiving_Season"],
                        Receiving_Curer = r["Receiving_Curer"],
                        Receiving_Curer_Name = r["Receiving_Curer_Name"],
                        Green_Grade = r["Green_Grade"],
                        Bales_no = Int16.Parse(r["Bales_no"]),
                        BuyWeight = decimal.Parse(r["BuyWeight"]).ToString("#,##0.00"),
                        BuyUnitPrice = decimal.Parse(r["BuyUnitPrice"]).ToString("#,##0.00"),
                        BuyTransprotChrge = decimal.Parse(r["BuyTransprotChrge"]).ToString("#,##0.00"),
                        BuyTotalUnitPrice = decimal.Parse(r["BuyTotalUnitPrice"]).ToString("#,##0.00"),
                        RecWeight = decimal.Parse(r["RecWeight"]).ToString("#,##0.00"),
                        RecUnitPrice = decimal.Parse(r["RecUnitPrice"]).ToString("#,##0.00"),
                        Amount = decimal.Parse(r["Amount"]).ToString("#,##0.00"),
                        Classify = r["Classify"]
                    });
                }
                return lstRC;
            }
        }
        public List<vm_rptrc009> GetGreenTransferReport(string tf_docno)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("SP_RPT_RC009") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@c_transfer_doc_no", tf_docno));
                var data = ExecuteCommand(command, new string[] { "transfer_doc_no", "doc_date", "transfer_no", "crop", "company", "type",
                                        "season", "green_grade", "classified_grade_from","classified_grade", "barcode", "bale_no",
                                         "weight", "unit_price", "amount"});
                if (!data.Any())
                    return null;

                List<vm_rptrc009> lsttf = new List<vm_rptrc009>();
                foreach (var r in data)
                {
                    lsttf.Add(new vm_rptrc009()
                    {
                        transfer_doc_no = r["transfer_doc_no"],
                        doc_date = DateTime.Parse(r["doc_date"]),
                        transfer_no = r["transfer_no"],
                        crop = r["crop"],
                        company = r["company"],
                        type = r["type"],
                        season = r["season"],
                        green_grade = r["green_grade"],
                        classified_grade_from = r["classified_grade_from"],
                        classified_grade = r["classified_grade"],
                        barcode = r["barcode"],
                        bale_no = r["bale_no"],
                        weight = decimal.Parse(r["weight"]),
                        unit_price = decimal.Parse(r["unit_price"]),
                        amount = decimal.Parse(r["amount"])
                    });
                }
                return lsttf;
            }
        }
        public List<vm_rptrc011> GetHandstripReport(string hs_docno)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {

                con.Open();
                var command = new SqlCommand("SP_RPT_RC011") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@c_hand_doc_no", hs_docno));
                var data = ExecuteCommand(command, new string[] { "input_hand_doc_no", "input_doc_date", "input_hand_no", "input_crop", "input_company", "input_type",
                                        "input_season", "input_green_grade", "input_classified_grade","input_barcode", "input_bale_no", "input_weight","input_unit_price", "input_amount",
                                         "output_barcode", "output_bale_no", "output_weight","output_unit_price", "output_amount","flag_io"});
                if (!data.Any())
                    return null;

                List<vm_rptrc011> lsttf = new List<vm_rptrc011>();
                foreach (var r in data)
                {
                    lsttf.Add(new vm_rptrc011()
                    {
                        input_hand_doc_no = r["input_hand_doc_no"],
                        input_doc_date = DateTime.Parse(r["input_doc_date"]),
                        input_hand_no = r["input_hand_no"],
                        input_crop = int.Parse(r["input_crop"]),
                        input_company = r["input_company"],
                        input_type = r["input_type"],
                        input_season = r["input_season"],
                        input_green_grade = r["input_green_grade"],
                        input_classified_grade = r["input_classified_grade"],
                        input_barcode = r["input_barcode"],
                        input_bale_no = r["input_bale_no"],
                        input_weight = r["input_weight"] == "" ? 0 : Convert.ToDecimal(r["input_weight"]),
                        input_unit_price = r["input_unit_price"] == "" ? 0 : Convert.ToDecimal(r["input_unit_price"]),
                        input_amount = r["input_amount"] == "" ? 0 : Convert.ToDecimal(r["input_amount"]),
                        output_barcode = r["output_barcode"],
                        output_bale_no = r["output_bale_no"],
                        output_weight = r["output_weight"] == "" ? 0 : Convert.ToDecimal(r["output_weight"]),
                        output_unit_price = r["output_unit_price"] == "" ? 0 : Convert.ToDecimal(r["output_unit_price"]),
                        output_amount = r["output_amount"] == "" ? 0 : Convert.ToDecimal(r["output_amount"]),
                        flag_io = r["flag_io"]
                    });
                }
                return lsttf;
            }
        }
        public List<vm_rptrc010> GetRegradeReport(string rg_docno)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {

                con.Open();
                var command = new SqlCommand("SP_RPT_RC010") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@c_regrade_doc_no", rg_docno));
                var data = ExecuteCommand(command, new string[] { "input_regrade_doc_no", "input_doc_date", "input_regrade_no", "input_crop", "input_company", "input_type",
                                        "input_season", "input_green_grade", "input_classified_grade","input_barcode", "input_bale_no", "input_weight","input_unit_price", "input_amount",
                                         "output_barcode", "output_bale_no", "output_weight","output_unit_price", "output_amount","flag_io"});
                if (!data.Any())
                    return null;

                List<vm_rptrc010> lsttf = new List<vm_rptrc010>();
                foreach (var r in data)
                {

                    lsttf.Add(new vm_rptrc010()
                    {
                        input_regrade_doc_no = r["input_regrade_doc_no"],
                        input_doc_date = DateTime.Parse(r["input_doc_date"]),
                        input_regrade_no = r["input_regrade_no"],
                        input_crop = int.Parse(r["input_crop"]),
                        input_company = r["input_company"],
                        input_type = r["input_type"],
                        input_season = r["input_season"],
                        input_green_grade = r["input_green_grade"],
                        input_classified_grade = r["input_classified_grade"],
                        input_barcode = r["input_barcode"],
                        input_bale_no = r["input_bale_no"],
                        input_weight = r["input_weight"] == "" ? 0 : Convert.ToDecimal(r["input_weight"]),
                        input_unit_price = r["input_unit_price"] == "" ? 0 : Convert.ToDecimal(r["input_unit_price"]),
                        input_amount = r["input_amount"] == "" ? 0 : Convert.ToDecimal(r["input_amount"]),
                        output_barcode = r["output_barcode"],
                        output_bale_no = r["output_bale_no"],
                        output_weight = r["output_weight"] == "" ? 0 : Convert.ToDecimal(r["output_weight"]),
                        output_unit_price = r["output_unit_price"] == "" ? 0 : Convert.ToDecimal(r["output_unit_price"]),
                        output_amount = r["output_amount"] == "" ? 0 : Convert.ToDecimal(r["output_amount"]),
                        flag_io = r["flag_io"]
                    });
                }
                return lsttf;
            }
        }
        public List<vm_Variance_Packed> GetVarianceDetails(int id)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {

                con.Open();
                var command = new SqlCommand("sp_SelAll_PS_VARIANCE_ADJ_PACKED") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@N_ID", id));
                var data = ExecuteCommand(command, new string[] { "C_BARCODE", "C_TYPE", "N_CROP", "C_PACKED_GRADE", "C_CUSTOMER_RS", "C_PACKED_GRADE_RS", "N_REDRY_ADJUST_VALUE", "N_PACKING_ADJUST_VALUE", "N_PACKED_ADJUST_VALUE", "N_PACKED_ORIGINAL_COST", "N_REDRY_ORIGINAL_COST", "N_PACKING_ORIGINAL_COST", "N_PACKED_ADJUST_COST", "N_REDRY_ADJUST_COST", "N_PACKING_ADJUST_COST", "N_WEIGHT", "C_CR_BY", "D_CR_DATE", "C_PROG_ID" });
                if (!data.Any())
                    return null;

                List<vm_Variance_Packed> lsttf = new List<vm_Variance_Packed>();
                foreach (var r in data)
                {

                    lsttf.Add(new vm_Variance_Packed()
                    {
                        C_BARCODE = r["C_BARCODE"],
                        C_TYPE = r["C_TYPE"],
                        N_CROP = Int16.Parse(r["N_CROP"]),
                        C_PACKED_GRADE = r["C_PACKED_GRADE"],
                        C_CUSTOMER_RS = r["C_CUSTOMER_RS"],
                        C_PACKED_GRADE_RS = r["C_PACKED_GRADE_RS"],
                        N_REDRY_ADJUST_VALUE = Decimal.Parse(r["N_REDRY_ADJUST_VALUE"]),
                        N_PACKING_ADJUST_VALUE = Decimal.Parse(r["N_PACKING_ADJUST_VALUE"]),
                        N_PACKED_ADJUST_VALUE = decimal.Parse(r["N_PACKED_ADJUST_VALUE"]),
                        N_PACKED_ORIGINAL_COST = Decimal.Parse(r["N_PACKED_ORIGINAL_COST"]),
                        N_REDRY_ORIGINAL_COST = String.IsNullOrEmpty(r["N_REDRY_ORIGINAL_COST"]) ? 0 : Decimal.Parse(r["N_REDRY_ORIGINAL_COST"]),
                        N_PACKING_ORIGINAL_COST = Decimal.Parse(r["N_PACKING_ORIGINAL_COST"]),
                        N_PACKED_ADJUST_COST = Decimal.Parse(r["N_PACKED_ADJUST_COST"]),
                        N_REDRY_ADJUST_COST = Decimal.Parse(r["N_REDRY_ADJUST_COST"]),
                        N_PACKING_ADJUST_COST = Decimal.Parse(r["N_PACKING_ADJUST_COST"]),
                        N_WEIGHT = Decimal.Parse(r["N_WEIGHT"]),
                        D_CR_DATE = DateTime.Parse(r["D_CR_DATE"])
                    });
                }
                return lsttf;
            }
        }
        public List<BroughtFWMatInfo> GetBroughtFDetails(string id)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("sp_PS_Get_BroughtForwardDetailByID") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@bfno", id));
                var data = ExecuteCommand(command, new string[] { "bfno", "rcno", "fromcrop", "tocrop", "bc", "type", "subtype", "supplier", "company", "green", "classify", "weight", "weightbuy", "price", "createdate" });
                if (!data.Any())
                    return null;

                List<BroughtFWMatInfo> lsttf = new List<BroughtFWMatInfo>();
                foreach (var r in data)
                {

                    lsttf.Add(new BroughtFWMatInfo()
                    {
                        bfno = r["bfno"],
                        rcno = r["rcno"],
                        fromcrop = Int16.Parse(r["fromcrop"]),
                        tocrop = Int16.Parse(r["tocrop"]),
                        bc = r["bc"],
                        type = r["type"],
                        subtype = r["subtype"],
                        supplier = r["supplier"],
                        company = r["company"],
                        green = r["green"],
                        classify = r["classify"],
                        weight = double.Parse(r["weight"]),
                        weightbuy = double.Parse(r["weightbuy"]),
                        price = double.Parse(r["price"]),
                        createdate = DateTime.Parse(r["createdate"])
                    });
                }
                return lsttf;
            }
        }
    }
}