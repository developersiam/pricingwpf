﻿using PricingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PricingBusinessLayer.Reports;

namespace PricingBusinessLayer.Reports
{
    public interface IStoreProcedureBL
    {
        List<vm_rptrc007> GetReceivingDetails(string docno);
        List<vm_rptrc009> GetGreenTransferDetails(string tfno);
        List<vm_rptrc011> GetHandStripDetails(string hsno);
        List<vm_rptrc010> GetRegradeDetails(string rgno);
        List<vm_Variance_Packed> GetVarianceDetails(int id);
        List<BroughtFWMatInfo> GetBroughtFWMatInfo(string bfno);
    }
    public class StoreProcedureBL : IStoreProcedureBL
    {
        public List<vm_rptrc007> GetReceivingDetails(string docno)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetReceivingReport(docno);
        }
        public List<vm_rptrc009> GetGreenTransferDetails(string docno)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetGreenTransferReport(docno);
        }
        public List<vm_rptrc011> GetHandStripDetails(string docno)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetHandstripReport(docno);
        }
        public List<vm_rptrc010> GetRegradeDetails(string docno)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetRegradeReport(docno);
        }
        public List<vm_Variance_Packed> GetVarianceDetails(int id)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetVarianceDetails(id);
        }
        public List<BroughtFWMatInfo> GetBroughtFWMatInfo(string bfno)
        {
            ReportRepository sp = new ReportRepository();
            return sp.GetBroughtFDetails(bfno);
        }

    }
}
