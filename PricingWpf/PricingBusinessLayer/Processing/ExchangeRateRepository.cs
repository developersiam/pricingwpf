﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PricingDomainModel;

namespace PricingBusinessLayer.Processing
{
    public class ExchangeRateRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        SqlConnection _connection;
        public ExchangeRateRepository()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            _connection.Open();
        }

        public static DateTime DateStringToDate(string date)
        {
            if (string.IsNullOrEmpty(date))
                return DateTime.Now.AddDays(-7);

            var sDate = date.Split('/');
            return new DateTime(Convert.ToInt32(sDate[2]), Convert.ToInt32(sDate[1]), Convert.ToInt32(sDate[0]));
        }

        public List<ExchangeRateInfo> GetExchangeRate(DateTime effDate)
        {
            var cmd = new SqlCommand("sp_PS_Get_ExchangeRateByDate") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@effDate", effDate));

            var data = ExecuteCommand(cmd, new string[] { "D_EFFECTIVE_DATE", "N_EXCHANGE_RATE" });
            List<ExchangeRateInfo> lstExchangeRateInfo = new List<ExchangeRateInfo>();
            foreach (var p in data)
            {
                var D_EFFECTIVE_DATE = p["D_EFFECTIVE_DATE"].ToString();
                lstExchangeRateInfo.Add(new ExchangeRateInfo()
                {
                    //EffectiveDate = DateTime.Parse(D_EFFECTIVE_DATE).ToString("dd/MM/yyyy"),
                    EffectiveDate = D_EFFECTIVE_DATE,
                    ExchangeRate = decimal.Parse(p["N_EXCHANGE_RATE"]),
                    //New property added 10/01/2021
                    EffecctiveDatetime = DateTime.Parse(D_EFFECTIVE_DATE)
                });
            }
            return lstExchangeRateInfo;
        }
        //------------------------ Submit  ---------------------
        public void SubmitExchangeRate(DateTime EffDate, decimal Rate, string User)
        {
            using (var cmd = new SqlCommand("sp_PS_Insert_ExchangeRate"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EffDate", EffDate);
                cmd.Parameters.AddWithValue("@rate", Rate);
                cmd.Parameters.AddWithValue("@user", User);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        //--------------------- Delete ----------------------
        public bool DeleteExchangeRate(DateTime EffDate, decimal Rate)
        {
            using (var cmd = new SqlCommand("sp_PS_Del_ExchangeRate", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EffDate", EffDate);
                cmd.Parameters.AddWithValue("@rate", Rate);

                SqlParameter count_result = cmd.Parameters.Add("@ExistedRate", SqlDbType.Int);
                count_result.Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQ‌​uery();
                _connection.Close();

                if ((int)cmd.Parameters["@ExistedRate"].Value > 0)
                    return true;
                return false;
            }
        }

    }
}