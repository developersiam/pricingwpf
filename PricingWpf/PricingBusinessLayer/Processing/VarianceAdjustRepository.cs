﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PricingDomainModel;

namespace PricingBusinessLayer.Processing
{
    public class VarianceAdjustRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Rvariance = 0;
        decimal Pvariance = 0;
        decimal Packvariance = 0;
        decimal redry = 0;
        decimal price = 0;
        decimal weight = 0;
        decimal packing_mat = 0;
        decimal s_sign = -1;
        decimal p_sign = -1;
        decimal pack_sign = -1;
        decimal SumRedry = 0;
        decimal SumPacking = 0;
        int adjID = 0;
        int bales = 0;

        public PackedGradeRedryInfo GetPackedWeight(int crop,
            string customerrs,
            string graders,
            string packedGrade,
            string packingMat)
        {
            var command = new SqlCommand("sp_Sel_AVG_REDRY_PRICE_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", customerrs));
            command.Parameters.Add(new SqlParameter("@C_GRADE_RS", graders));
            command.Parameters.Add(new SqlParameter("@C_GRADE", packedGrade));
            command.Parameters.Add(new SqlParameter("@C_PACKINGMAT", packingMat));

            var results = ExecuteCommand(command, new string[] { "redry", "packingmat", "price", "weight", "bales" });
            if (!results.Any())
                return null;

            foreach (var r in results)
            {
                if (decimal.TryParse(r["redry"], out redry))
                    redry = Decimal.Parse(r["redry"]);
                else
                    redry = 0;

                if (decimal.TryParse(r["price"], out price))
                    price = Decimal.Parse(r["price"]);
                else
                    price = 0;

                if (int.TryParse(r["bales"], out bales))
                    bales = int.Parse(r["bales"]);
                else
                    bales = 0;

                if (decimal.TryParse(r["weight"], out weight))
                    weight = decimal.Parse(r["weight"]);
                else
                    weight = 0;

                if (decimal.TryParse(r["packingmat"], out packing_mat))
                    packing_mat = decimal.Parse(r["packingmat"]);
                else
                    packing_mat = 0;
            }
            return new PackedGradeRedryInfo()
            {
                AvgRedry = redry.ToString("#,##0.00"),
                MinPrice = price.ToString("#,##0.00"),
                Weight = weight.ToString("#,##0"),
                Bales = bales,
                PackingMat = packing_mat.ToString("#,##0.00")
            };
        }

        public void AddPackedVarianceAdj(int crop,
                string customerrs,
                string graders,
                string grade,
                string packingMat,
                string Rsign,
                decimal RValue,
                string Rremark,
                string Psign,
                decimal PValue,
                string Premark,
                string Packsign,
                decimal PackValue,
                string Packremark,
                string user)
        {
            if (Rsign == "+")
                s_sign = 1;
            else if (Rsign == "-")
                s_sign = -1;
            else
                s_sign = 0;

            if (Psign == "+")
                p_sign = 1;
            else if (Psign == "-")
                p_sign = -1;
            else
                p_sign = 0;

            //if (Packsign == "+")
            //    pack_sign = 1;
            //else if (Packsign == "-")
            //    pack_sign = -1;
            //else
                pack_sign = 1;


            using (var cmd = new SqlCommand("sp_Ins_PS_B_VARIANCE_ADJ_VAL"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", crop);
                cmd.Parameters.AddWithValue("@C_CUSTOMER_RS", customerrs);
                cmd.Parameters.AddWithValue("@C_PACKED_GRADE_RS", graders);
                cmd.Parameters.AddWithValue("@C_PACKED_GRADE", grade);
                cmd.Parameters.AddWithValue("@C_PACKING_MAT", packingMat);
                cmd.Parameters.AddWithValue("@C_REDRY_SIGH", s_sign);
                cmd.Parameters.AddWithValue("@N_REDRY_ADJUST_VALUE", RValue);
                cmd.Parameters.AddWithValue("@C_REDRY_REMARK", Rremark);
                cmd.Parameters.AddWithValue("@C_PACKING_SIGH", p_sign);
                cmd.Parameters.AddWithValue("@N_PACKING_ADJUST_VALUE", PValue);
                cmd.Parameters.AddWithValue("@C_PACKING_REMARK", Premark);
                cmd.Parameters.AddWithValue("@C_PACKED_SIGH", pack_sign);
                cmd.Parameters.AddWithValue("@N_PACKED_ADJUST_VALUE", PackValue);
                cmd.Parameters.AddWithValue("@C_PACKED_REMARK", Packremark);
                cmd.Parameters.AddWithValue("@C_CR_BY", user);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", user);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmVarianceAdjust");
                ExecuteCommand(cmd, new string[] { });
            }
        }
        //----------------------- Get List of Packed Export Value Main ----------------------------
        public List<VarianceAdjustInfo> GetJsonVarianceAdjValue(string Crop)
        {
            var command = new SqlCommand("sp_SelAll_PS_VARIANCE_ADJ_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "N_ID_ADJ", "N_CROP", "C_PACKED_GRADE", "C_CUSTOMER_RS", "C_PACKED_GRADE_RS", "C_PACKING_MAT", "N_REDRY_ADJUST_VALUE", "C_REDRY_SIGH", "C_REDRY_REMARK", "N_PACK_ADJUST_VALUE", "C_PACK_SIGH", "C_PACK_REMARK", "C_PACKING_SIGH", "N_PACKING_ADJUST_VALUE", "C_PACKING_REMARK", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
            List<VarianceAdjustInfo> lstPacked = new List<VarianceAdjustInfo>();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["N_REDRY_ADJUST_VALUE"], out Rvariance))
                    Rvariance = Decimal.Parse(r["N_REDRY_ADJUST_VALUE"]);
                else Rvariance = 0;

                if (decimal.TryParse(r["N_PACK_ADJUST_VALUE"], out Pvariance))
                    Pvariance = Decimal.Parse(r["N_PACK_ADJUST_VALUE"]);
                else Pvariance = 0;

                if (decimal.TryParse(r["N_PACKING_ADJUST_VALUE"], out Pvariance))
                    Packvariance = Decimal.Parse(r["N_PACKING_ADJUST_VALUE"]);
                else Packvariance = 0;

                lstPacked.Add(new VarianceAdjustInfo()
                {
                    Id = Int16.Parse(r["N_ID_ADJ"]),
                    Crop = Int16.Parse(r["N_CROP"]),
                    PackedGrade = r["C_PACKED_GRADE"],
                    Customer = r["C_CUSTOMER_RS"],
                    CustomerGrade = r["C_PACKED_GRADE_RS"],
                    PackingMat = r["C_PACKING_MAT"],
                    RValue = string.IsNullOrEmpty(r["C_REDRY_SIGH"]) ? 0 : Rvariance * Decimal.Parse(r["C_REDRY_SIGH"]),
                    RSign = Decimal.Parse(r["C_REDRY_SIGH"]),
                    RRemark = r["C_REDRY_REMARK"],
                    PValue = string.IsNullOrEmpty(r["C_PACK_SIGH"]) ? 0 : Pvariance * Decimal.Parse(r["C_PACK_SIGH"]),
                    PSign = Decimal.Parse(r["C_PACK_SIGH"]),
                    PRemark = r["C_PACK_REMARK"],
                    PackValue = string.IsNullOrEmpty(r["C_PACKING_SIGH"]) ? 0 : Packvariance * Decimal.Parse(r["C_PACKING_SIGH"]),
                    PackSign = Decimal.Parse(r["C_PACKING_SIGH"]),
                    PackRemark = r["C_PACKING_REMARK"],
                    CreatedDate = DateTime.Parse(r["D_CR_DATE"])
                });
            }
            return lstPacked;
        }
        public bool DeleteAdjust(int id)
        {
            var command = new SqlCommand("sp_Del_PS_VARIANCE_ADJ") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_ID", id));
            var result = ExecuteCommand(command, new string[] { });
            return true;

        }
        public List<PackingInfo> GetPackingMatByGrade(string customerrs,
            string graders,
            string packedGrade)
        {
            var command = new SqlCommand("sp_PS_Get_PackingMatByGradeCustomer") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", customerrs));
            command.Parameters.Add(new SqlParameter("@C_GRADE_RS", graders));
            command.Parameters.Add(new SqlParameter("@C_GRADE", packedGrade));

            var data = ExecuteCommand(command, new string[] { "packingmat" });

            List<PackingInfo> lstPackingMat = new List<PackingInfo>();

            if (data.Count() != 0) //return lstPackingMat;
            {
                foreach (var p in data)
                {
                    lstPackingMat.Add(new PackingInfo()
                    {
                        Packing_Mat = p["packingmat"]
                    });
                }
            }
            
            return lstPackingMat;
        }

        public VarianceSummaryInfo GetSumVariance(int crop,
            string customerrs,
            string graders,
            string packedGrade,
            string packingMat)
        {

            var command = new SqlCommand("sp_Sel_SUMMARY_VARIANCE_ADJUST") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", customerrs));
            command.Parameters.Add(new SqlParameter("@C_GRADE_RS", graders));
            command.Parameters.Add(new SqlParameter("@C_GRADE", packedGrade));
            command.Parameters.Add(new SqlParameter("@C_PACKINGMAT", packingMat));

            var data = ExecuteCommand(command, new string[] { "N_CROP", "C_PACKED_GRADE", "C_CUSTOMER_RS", "C_PACKED_GRADE_RS", "C_PACKING_MAT","sumRedry", "sumPacking" });

            if (!data.Any()) return null;

            VarianceSummaryInfo lstSumVariance = new VarianceSummaryInfo();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["sumRedry"], out SumRedry))
                    SumRedry = Decimal.Parse(r["sumRedry"]);
                else SumRedry = 0;

                if (decimal.TryParse(r["sumPacking"], out SumPacking))
                    SumPacking = Decimal.Parse(r["sumPacking"]);
                else SumPacking = 0;

            }
            return new VarianceSummaryInfo()
                {
                    Id = 0,
                    Crop = crop,
                    PackedGrade = packedGrade,
                    Customer = customerrs,
                    CustomerGrade = graders,
                    PackingMat =packingMat,
                    PSumValue = SumPacking,
                    RSumValue = SumRedry
                };
            }


        }
    }

