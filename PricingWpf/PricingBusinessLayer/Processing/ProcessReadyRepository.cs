﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using PricingDomainModel;

namespace PricingBusinessLayer.Processing
{
    public class ProcessReadyRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Total_Amount = 0;
        decimal Total_Weight = 0;
        decimal Average_Price = 0;
        decimal R_Weight = 0;
        decimal P_Weight = 0;
        decimal Transp = 0;
        decimal Redry = 0;
        decimal Pack_M = 0;

        public List<CropReceiving> GetProcessingCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CropPackedGrade") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }

    //----------------------- Get List of Processing Main ----------------------------
        public List<ProcessReadyMasterInfo> GetJsonProcessReady(int Crop)
        {
            //decimal amount = 0;
            //decimal weight = 0;
            var command = new SqlCommand("sp_PS_Get_ProcessReadyByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@Crop", Crop));
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Weight", "Packed_Weight", "Season", "Transportation", "Packed_Grade", "Pack_Mat", "Redry_Charge", "ProcessingR_No" });
            List<ProcessReadyMasterInfo> lstRadey = new List<ProcessReadyMasterInfo>();

            foreach (var r in data)
            {
                //Get Total amount
                Total_Amount = 0;
                Total_Weight = 0;
                Average_Price = 0;
                R_Weight = 0;
                P_Weight = 0;
                Transp = 0;
                Redry = 0;
                Pack_M = 0;

                //Update amt into List
                if (decimal.TryParse(r["Weight"], out R_Weight))
                    R_Weight = Math.Round(decimal.Parse(r["Weight"]),2);
                else R_Weight = 0;
                if (decimal.TryParse(r["Packed_Weight"], out P_Weight))
                    P_Weight = Math.Round(decimal.Parse(r["Packed_Weight"]),2);
                else P_Weight = 0;
                if (decimal.TryParse(r["Transportation"], out Transp))
                    Transp = Math.Round(decimal.Parse(r["Transportation"]),2);
                else Transp = 0;
                if (decimal.TryParse(r["Redry_Charge"], out Redry))
                    Redry = Math.Round(decimal.Parse(r["Redry_Charge"]),2);
                else Redry = 0;
                if (decimal.TryParse(r["Pack_Mat"], out Pack_M))
                    Pack_M = Math.Round(decimal.Parse(r["Pack_Mat"]),2);
                else Pack_M = 0;

                lstRadey.Add(new ProcessReadyMasterInfo()
                {
                    Doc_No = r["Doc_No"],
                    ProcessingR_No = r["ProcessingR_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = r["Type"],
                    Season = r["Season"],
                    Weight = R_Weight,
                    Packed_Weight = P_Weight,
                    Transportation = Transp,
                    Packed_Grade = r["Packed_Grade"],
                    Pack_Mat = Pack_M,
                    Redry_Charge = Redry
                });
            }
            return lstRadey;
        }

        //Get New Processing Ready
        public List<ProcessReadyMasterInfo> GetNewJsonProcessReady()
        {
            var command = new SqlCommand("sp_PS_Get_ProcessReadyNotDone") { CommandType = CommandType.StoredProcedure };
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Weight", "Packed_Weight", "Season", "Transportation", "Packed_Grade", "Pack_Mat", "Redry_Charge", "ProcessingR_No" });
            List<ProcessReadyMasterInfo> lstRadey = new List<ProcessReadyMasterInfo>();

            foreach (var r in data)
            {
                //Get Total amount
                Total_Amount = 0;
                Total_Weight = 0;
                Average_Price = 0;
                R_Weight = 0;
                P_Weight = 0;
                Transp = 0;
                Redry = 0;
                Pack_M = 0;

                if (decimal.TryParse(r["Weight"], out R_Weight))
                    R_Weight = Math.Round(decimal.Parse(r["Weight"]),2);
                else R_Weight = 0;
                if (decimal.TryParse(r["Packed_Weight"], out P_Weight))
                    P_Weight = Math.Round(decimal.Parse(r["Packed_Weight"]), 2);
                else P_Weight = 0;
                if (decimal.TryParse(r["Transportation"], out Transp))
                    Transp = Math.Round(decimal.Parse(r["Transportation"]), 2);
                else Transp = 0;
                if (decimal.TryParse(r["Redry_Charge"], out Redry))
                    Redry = Math.Round(decimal.Parse(r["Redry_Charge"]), 2);
                else Redry = 0;
                if (decimal.TryParse(r["Pack_Mat"], out Pack_M))
                    Pack_M = Math.Round(decimal.Parse(r["Pack_Mat"]), 2);
                else Pack_M = 0;

                lstRadey.Add(new ProcessReadyMasterInfo()
                {
                    Doc_No = r["Doc_No"],
                    ProcessingR_No = r["ProcessingR_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = r["Type"],
                    Season = r["Season"],
                    Weight = R_Weight,
                    Packed_Weight = P_Weight,
                    Transportation = Transp,
                    Packed_Grade = r["Packed_Grade"],
                    Pack_Mat = Pack_M,
                    Redry_Charge = Redry
                });
            }
            return lstRadey;
        }
        //---------------------------- Process Ready Details -----------------------------------------------------
        public PDProcessReadyMasterInfo GetProcessReadyCalculate(string ProcessNo)
        {
            decimal amount = 0;
            decimal weight = 0;
            Total_Amount = 0;
            Total_Weight = 0;
            Average_Price = 0;
            R_Weight = 0;
            P_Weight = 0;
            Transp = 0;
            Redry = 0;
            Pack_M = 0;

            var command = new SqlCommand("sp_PS_Get_PdSetupProcessReadyByProcessNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var gt = ExecuteCommand(command, new string[] { "Crop", "Type", "Subtype", "PackedGrade", "Weight", "PackedWeight", "Redry", "PackingmatCost", "Transport", "ProcessNo", "DocumentNo", "DocDate" });
            if (!gt.Any())
                return null;

            //Get Total amount
            var cmd = new SqlCommand("sp_Sel_PROCESS_MAT") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var data_amt = ExecuteCommand(cmd, new string[] { "Bales No", "Company", "Green Grade", "Classify", "Packed Grade", "Weight", "Unit Price", "Amount", "GEP", "GEP%", "pdno", "type", "subtype", "Total Unit", "Flag" });

            foreach (var amt in data_amt)
            {
                if (amt["Flag"] == "O")
                {
                    if (decimal.TryParse(amt["Amount"], out amount))
                        amount = Decimal.Parse(amt["Amount"]);
                    else amount = 0;
                    if (decimal.TryParse(amt["Weight"], out weight))
                        weight = Decimal.Parse(amt["Weight"]);
                    else weight = 0;

                    Total_Amount += amount;
                    Total_Weight += weight;
                }
            }
            Average_Price = (Total_Amount / Total_Weight);

            if (decimal.TryParse(gt[0]["Weight"], out R_Weight))
                R_Weight = Math.Round(decimal.Parse(gt[0]["Weight"]), 2);
            else R_Weight = 0;
            if (decimal.TryParse(gt[0]["PackedWeight"], out P_Weight))
                P_Weight = Math.Round(decimal.Parse(gt[0]["PackedWeight"]), 2);
            else P_Weight = 0;
            if (decimal.TryParse(gt[0]["Transport"], out Transp))
                Transp = Math.Round(decimal.Parse(gt[0]["Transport"]), 2);
            else Transp = 0;
            if (decimal.TryParse(gt[0]["Redry"], out Redry))
                Redry = Math.Round(decimal.Parse(gt[0]["Redry"]),2);
            else Redry = 0;
            if (decimal.TryParse(gt[0]["PackingmatCost"], out Pack_M))
                Pack_M = Math.Round(decimal.Parse(gt[0]["PackingmatCost"]),2);
            else Pack_M = 0;

            return new PDProcessReadyMasterInfo()
            {
                Crop = Int16.Parse(gt[0]["Crop"]),
                Type = gt[0]["Type"],
                Season = gt[0]["Subtype"],
                Packed_Grade = gt[0]["PackedGrade"],
                Weight = R_Weight,
                Packed_Weight = P_Weight,
                Redry_Charge = Redry,
                Pack_Mat = Pack_M,
                Transportation = Transp,
                ProcessingR_No = gt[0]["ProcessNo"],
                Doc_No = gt[0]["DocumentNo"],
                Doc_Date = DateTime.Parse(gt[0]["DocDate"].ToString()),
                Total_Amt = Math.Round(Total_Amount,2),
                Total_Weight = Math.Round(Total_Weight, 2),
                Average_Price = Math.Round(Average_Price, 2)
            };
        }
        public List<ProcessReadyDetails> GetJsonProcessRDetails(string ProcessNo)
        {
            decimal amount = 0;
            decimal totalUnit = 0;
            decimal UnitPrice = 0;
            var command = new SqlCommand("sp_Sel_PROCESS_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var results = ExecuteCommand(command, new string[] { "bc","Bales No", "Company", "Green Grade", "classify", "Packed Grade", "Weight", "Unit Price", "Amount", "GEP", "GEP%", "pdno", "type", "subtype", "Total Unit", "Flag" });
            List<ProcessReadyDetails> lstPD = new List<ProcessReadyDetails>();
            foreach (var r in results)
            {
                if (r["Total Unit"] == "")
                    totalUnit = 0;
                else totalUnit = Math.Round(decimal.Parse(r["Total Unit"]), 2);
                if (r["Amount"] == "")
                    amount = 0;
                else amount = Math.Round(decimal.Parse(r["Amount"]), 2);
                if (r["Unit Price"] == "")
                    UnitPrice = 0;
                else UnitPrice = Math.Round(decimal.Parse(r["Unit Price"]), 2);

                lstPD.Add(new ProcessReadyDetails()
                {
                    BaleNo = r["Bales No"],
                    GreenGrade = r["Green Grade"],
                    PackedGrade = r["Packed Grade"],
                    Weight = Convert.ToString(Math.Round(double.Parse(r["Weight"]), 2)),
                    UnitPrice = Convert.ToString(UnitPrice),
                    GEP = r["GEP"],
                    GEP_Per = r["GEP%"],
                    ToTalUnit = Convert.ToString(totalUnit),
                    Amount = amount.ToString("N2"),
                    Flag = r["Flag"]
                });
            }
            return lstPD;
        }
        //----------- Delete ------------------
        public void DeleteProcessReady(string PRDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_PROCESS_READY_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_PROCESS_DOC_NO", PRDocNo));
            ExecuteCommand(command, new string[] { });
        }

        //----------- Adding New ProcessReady -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "PDF"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "PDF"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }
        public void AddProcessReady(string DocNo, DateTime DocDate, string PRNo, int Crop, string Type, string Season, string PackedGrade, string PackedMat, decimal PackedMatCharge, string TransCode, decimal TransCharge, string RedryCode, decimal RedryCharge, decimal CWeight, decimal PWeight ,string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_PROCESS_READY_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_PROCESS_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_PROCESS_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_PROCESS_NO", PRNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_PACKED_GRADE", PackedGrade);
                cmd.Parameters.AddWithValue("@C_PACK_MAT_CODE", PackedMat);
                cmd.Parameters.AddWithValue("@N_PACK_MAT_CHARGE", PackedMatCharge);
                cmd.Parameters.AddWithValue("@C_TRAN_CHARGE_CODE", TransCode);
                cmd.Parameters.AddWithValue("@N_TRAN_CHARGE", TransCharge);
                cmd.Parameters.AddWithValue("@C_REDRY_CHARGE_CODE", RedryCode);
                cmd.Parameters.AddWithValue("@N_REDRY_CHARGE", RedryCharge);
                cmd.Parameters.AddWithValue("@N_CASE_WEIGHT", CWeight);
                cmd.Parameters.AddWithValue("@N_PACKED_WEIGHT", PWeight);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "frmProduct");
                ExecuteCommand(cmd, new string[] { });
            }
        }

    }

}