﻿using PricingDomainModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingBusinessLayer.Processing
{
    public class VarianceAdjustDetailsRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Rvariance = 0;
        decimal Pvariance = 0;
        decimal OriginalR = 0;
        decimal Packedvariance = 0;

        //----------------------- Get List of Packed Export Value Main ----------------------------
        public List<VarianceAdjustDetailsInfo> GetJsonVarianceAdjValueDetails(int Id)
        {
            var command = new SqlCommand("sp_SelAll_PS_VARIANCE_ADJ_PACKED") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_ID", Id));
            var data = ExecuteCommand(command, new string[] { "C_BARCODE", "C_TYPE", "N_CROP", "C_PACKED_GRADE", "C_CUSTOMER_RS", "C_PACKED_GRADE_RS", "N_REDRY_ADJUST_VALUE", "N_PACKING_ADJUST_VALUE", "N_PACKED_ADJUST_VALUE", "N_PACKED_ORIGINAL_COST", "N_REDRY_ORIGINAL_COST", "N_PACKING_ORIGINAL_COST", "N_PACKED_ADJUST_COST", "N_REDRY_ADJUST_COST", "N_PACKING_ADJUST_COST", "N_WEIGHT",  "C_CR_BY", "D_CR_DATE", "C_PROG_ID" });
            List<VarianceAdjustDetailsInfo> lstPacked = new List<VarianceAdjustDetailsInfo>();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["N_REDRY_ADJUST_VALUE"], out Rvariance))
                    Rvariance = decimal.Parse(r["N_REDRY_ADJUST_VALUE"]);
                else Rvariance = 0;

                if (decimal.TryParse(r["N_PACKING_ADJUST_VALUE"], out Pvariance))
                    Pvariance = decimal.Parse(r["N_PACKING_ADJUST_VALUE"]);
                else Pvariance = 0;

                if (decimal.TryParse(r["N_REDRY_ORIGINAL_COST"], out OriginalR))
                    OriginalR = decimal.Parse(r["N_REDRY_ORIGINAL_COST"]);
                else OriginalR = 0;

                if (decimal.TryParse(r["N_PACKED_ADJUST_VALUE"], out Packedvariance))
                    Packedvariance = decimal.Parse(r["N_PACKED_ADJUST_VALUE"]);
                else Packedvariance = 0;

                lstPacked.Add(new VarianceAdjustDetailsInfo()
                {
                    BC = r["C_BARCODE"],
                    Type = r["C_TYPE"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    PackedGrade = r["C_PACKED_GRADE"],
                    Customer = r["C_CUSTOMER_RS"],
                    CustomerGrade = r["C_PACKED_GRADE_RS"],
                    RAdjustValue= Rvariance,
                    PAdjustValue = Pvariance,
                    PackedAdjustValue = Packedvariance,
                    PackedOriginal = Decimal.Parse(r["N_PACKED_ORIGINAL_COST"]),
                    RedryOriginal = OriginalR,
                    PackCostOriginal = Decimal.Parse(r["N_PACKING_ORIGINAL_COST"]),
                    PackedAdj = Decimal.Parse(r["N_PACKED_ADJUST_COST"]),
                    RedryAdj = Decimal.Parse(r["N_REDRY_ADJUST_COST"]),
                    PackCostAdj = Decimal.Parse(r["N_PACKING_ADJUST_COST"]),
                    PackedCostAdj = Packedvariance * Decimal.Parse(r["N_WEIGHT"]),
                    Weight = Decimal.Parse(r["N_WEIGHT"]),
                    CreatedDate = DateTime.Parse(r["D_CR_DATE"])
                });
            }
            return lstPacked;
        }

    }
}
