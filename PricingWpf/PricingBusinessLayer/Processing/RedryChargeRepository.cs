﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PricingDomainModel;

namespace PricingBusinessLayer.Processing
{
    public class RedryChargeRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }


        public List<RedryChargeInfo> GetRedry_M(int Crop)
        {
            var command = new SqlCommand("sp_SelAll_PS_B_REDRYING_CHARGE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));

            var data = ExecuteCommand(command, new string[] { "N_CROP", "C_REDRY_CHARGE_CODE", "C_CHARGE_DESC", "N_CHARGE", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
            List<RedryChargeInfo> lstRedryInfo = new List<RedryChargeInfo>();
            foreach (var p in data)
            {
                lstRedryInfo.Add(new RedryChargeInfo()
                {
                    Crop = Int16.Parse(p["N_CROP"]),
                    Redry_Charge_Code = p["C_REDRY_CHARGE_CODE"],
                    Redry_Charge_Description = p["C_CHARGE_DESC"],
                    Charge = decimal.Parse(p["N_CHARGE"])
                });
            }
            return lstRedryInfo;
        }
        public List<TransportChargeInfo> GetTransport_M(int Crop)
        {
            var command = new SqlCommand("sp_SelAll_PS_B_TRANSPORT_CHARGE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
            var data = ExecuteCommand(command, new string[] { "N_CROP", "C_TRAN_CHARGE_CODE", "C_TRAN_CHARGE_DESC", "N_CHARGE", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
            List<TransportChargeInfo> lstTransportInfo = new List<TransportChargeInfo>();
            foreach (var p in data)
            {
                lstTransportInfo.Add(new TransportChargeInfo()
                {
                    Crop = Int16.Parse(p["N_CROP"]),
                    Trans_Charge_Code = p["C_TRAN_CHARGE_CODE"],
                    Trans_Charge_Description = p["C_TRAN_CHARGE_DESC"],
                    Charge = decimal.Parse(p["N_CHARGE"])
                });
            }
            return lstTransportInfo;
 
        }

        public bool ExistedRedryTransport(int Crop, string Code, string type)
        {
            var command = new SqlCommand("sp_PS_Get_RedryingTransportation") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));
            command.Parameters.Add(new SqlParameter("@code", Code));
            command.Parameters.Add(new SqlParameter("@Type", type));
            var result = ExecuteCommand(command, new string[] { });
            if (!result.Any())
                return false;
            return true;
        }

        //----------- Delete  ------------------
        public bool DeleteRedryCharge(int Crop, string Code, string type)
        {
            //Check existed before delete
            if (!ExistedRedryTransport(Crop, Code, type))
                return false;

            SqlCommand command;
            if (type == "redry")
            {
                command = new SqlCommand("sp_Del_PS_B_REDRYING_CHARGE") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
                command.Parameters.Add(new SqlParameter("@C_REDRY_CHARGE_CODE", Code));
            }
            else
            {
                command = new SqlCommand("sp_Del_PS_B_TRANSPORT_CHARGE") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
                command.Parameters.Add(new SqlParameter("@C_TRAN_CHARGE_CODE", Code));
            }
            var result = ExecuteCommand(command, new string[] { });
            return true;
        }
        //------------------------ Submit  ---------------------
        public void SubmitRedryTransport(int Crop, string Code, string type, string description, decimal Charge, string User)
        {
            //Check existed then Add new or Update
            if (!ExistedRedryTransport(Crop, Code, type))
            {
                if (type == "redry")
                    AddRedrying(Crop, Code, type, description, Charge, User);
                else
                    AddTransportation(Crop, Code, type, description, Charge, User);
            }
            else
            {
                if (type == "redry")
                    UpdateRedrying(Crop, Code, type, description, Charge, User);
                else
                    UpdateTransportation(Crop, Code, type, description, Charge, User);
            }

        }
        public void AddRedrying(int Crop, string Code, string type, string description, decimal Charge, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_B_REDRYING_CHARGE"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_REDRY_CHARGE_CODE", Code);
                cmd.Parameters.AddWithValue("@C_CHARGE_DESC", description);
                cmd.Parameters.AddWithValue("@N_CHARGE", Charge);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmRedrying");
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void AddTransportation(int Crop, string Code, string type, string description, decimal Charge, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_B_TRANSPORT_CHARGE"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_TRAN_CHARGE_CODE", Code);
                cmd.Parameters.AddWithValue("@C_TRAN_CHARGE_DESC", description);
                cmd.Parameters.AddWithValue("@N_CHARGE", Charge);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmRedrying");
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void UpdateRedrying(int Crop, string Code, string type, string description, decimal Charge, string User)
        {
            using (var cmd = new SqlCommand("sp_Upd_PS_B_REDRYING_CHARGE"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_REDRY_CHARGE_CODE", Code);
                cmd.Parameters.AddWithValue("@C_CHARGE_DESC", description);
                cmd.Parameters.AddWithValue("@N_CHARGE", Charge);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmRedrying");
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void UpdateTransportation(int Crop, string Code, string type, string description, decimal Charge, string User)
        {
            using (var cmd = new SqlCommand("sp_Upd_PS_B_TRANSPORT_CHARGE"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_TRAN_CHARGE_CODE", Code);
                cmd.Parameters.AddWithValue("@C_TRAN_CHARGE_DESC", description);
                cmd.Parameters.AddWithValue("@N_CHARGE", Charge);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmRedrying");
                ExecuteCommand(cmd, new string[] { });
            }
        }
    }
}