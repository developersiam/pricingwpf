﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PricingDomainModel;

namespace PricingBusinessLayer.Processing
{
    public class ExportRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal export = 0;
        decimal unit = 0;
        decimal avg_weight = 0;

        public List<PackedGradeCustomerInfo> GetAllPackedGradeCustomer(int crop)
        {
            var command = new SqlCommand("sp_PS_Get_PackedGradeCustomer") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@CROP", crop));

            var results = ExecuteCommand(command, new string[] { "PackedGrade", "Customer", "CustomerGrade" });
            if (!results.Any())
                return new List<PackedGradeCustomerInfo>();

            return results.Select(c => new PackedGradeCustomerInfo()
            {
                PackedGrade = c["PackedGrade"],
                CustomerName = c["Customer"],
                CustomerGrade = c["CustomerGrade"]
                //UnitPrice = c["UnitPrice"],
                //AvgWeight = c["weight"]

            }).ToList();
        }

        public List<PackedInfo> GetPackedGrade(int Crop)
        {
            var command = new SqlCommand("sp_SelAll_GRADE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));

            var results = ExecuteCommand(command, new string[] { "grade" });
            if (!results.Any())
                return new List<PackedInfo>();

            return results.Select(c => new PackedInfo()
            {
                PackedGrade = c["grade"]
            }).ToList();
        }

        public List<CustomerInfo> GetCustomer(int Crop, string PackedGrade)
        {
            var command = new SqlCommand("sp_Sel_CUSTOMER_RS") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
            command.Parameters.Add(new SqlParameter("@C_GRADE", PackedGrade));

            var results = ExecuteCommand(command, new string[] { "CUSTOMERRS" });
            if (!results.Any())
                return new List<CustomerInfo>();

            return results.Select(c => new CustomerInfo()
            {
                CustomerName = c["CUSTOMERRS"]
            }).ToList();
        }

        public List<GraderInfo> GetCustomerGrade(int Crop, string PackedGrade, string Customerrs)
        {
            var command = new SqlCommand("sp_Sel_PACKED_GRADE_RS") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE", PackedGrade));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", Customerrs));

            var results = ExecuteCommand(command, new string[] { "graders" });
            if (!results.Any())
                return new List<GraderInfo>();

            return results.Select(c => new GraderInfo()
            {
                CustomerGrade = c["graders"]
            }).ToList();
        }
        //----------------------- Get List of Packed Export Value Main ----------------------------
        public List<ExportValueInfo> GetJsonPackedExportValue(string Crop)
        {
            var command = new SqlCommand("sp_SelAll_PS_PACKED_EXP_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "N_CROP", "C_PACKED_GRADE", "C_CUSTOMER_RS", "C_PACKED_GRADE_RS", "N_EXPORT_VALUE", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
            List<ExportValueInfo> lstPacked = new List<ExportValueInfo>();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["N_EXPORT_VALUE"], out export))
                    export = Decimal.Parse(r["N_EXPORT_VALUE"]);
                else export = 0;

                lstPacked.Add(new ExportValueInfo()
                {
                    Crop = Int16.Parse(r["N_CROP"]),
                    PackedGrade = r["C_PACKED_GRADE"],
                    Customer = r["C_CUSTOMER_RS"],
                    CustomerGrade = r["C_PACKED_GRADE_RS"],
                    Value = export
                });
            }
            return lstPacked;
        }

        public List<ClassifiedExportValueInfo> GetClassifiedGradeList(string Crop)
        {
            var command = new SqlCommand("sp_SelAll_CLASSIFY") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_CROP", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "classify" });
            List<ClassifiedExportValueInfo> returnList = new List<ClassifiedExportValueInfo>();

            foreach (var r in data)
            {
                returnList.Add(new ClassifiedExportValueInfo()
                {
                    ClassifiedGrade = r["classify"]
                });
            }
            return returnList;
        }

        public List<ClassifiedExportValueInfo> GetClassifiedExportValue(int Crop)
        {
            var returnList = new List<ClassifiedExportValueInfo>();
            var command = new SqlCommand("sp_SelAll_PS_B_EXPORT_VALUE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_CROP", Int16.Parse(Crop.ToString())));
            var data = ExecuteCommand(command, new string[] {
                    "C_CROP",
                    "C_CLASSIFIED_GRADE",
                    "N_EXPORT_VALUE",
                    "C_CR_BY",
                    "D_CR_DATE",
                    "C_UPD_BY",
                    "D_UPD_DATE",
                    "C_PROG_ID" });

            foreach (var r in data)
            {
                if (decimal.TryParse(r["N_EXPORT_VALUE"], out export))
                    export = Decimal.Parse(r["N_EXPORT_VALUE"]);
                else export = 0;

                returnList.Add(new ClassifiedExportValueInfo()
                {
                    Crop = Int16.Parse(r["C_CROP"]),
                    ClassifiedGrade = r["C_CLASSIFIED_GRADE"],
                    Value = export,
                });
            }

            return returnList;
        }

        public List<ClassifiedExportValueInfo> GetJsonExportValue(string Crop)
        {
            var command = new SqlCommand("sp_SelAll_PS_B_EXPORT_VALUE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_CROP", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "C_CROP", "C_CLASSIFIED_GRADE", "N_EXPORT_VALUE", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
            List<ClassifiedExportValueInfo> lstClassified = new List<ClassifiedExportValueInfo>();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["N_EXPORT_VALUE"], out export))
                    export = Decimal.Parse(r["N_EXPORT_VALUE"]);
                else export = 0;

                lstClassified.Add(new ClassifiedExportValueInfo()
                {
                    Crop = Int16.Parse(r["C_CROP"]),
                    ClassifiedGrade = r["C_CLASSIFIED_GRADE"],
                    Value = export
                });
            }
            return lstClassified;
        }

        public PackedGradeUnitWeightInfo GetClassifiedUnitPrice(int crop, string classified)
        {
            var command = new SqlCommand("sp_Sel_SUM_UNITPRICE_ONHAND_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CLASSIFY", classified));

            var results = ExecuteCommand(command, new string[] { "unitprice" });
            if (!results.Any())
                return null;

            foreach (var r in results)
            {
                if (decimal.TryParse(r["unitprice"], out unit))
                    unit = Decimal.Parse(r["unitprice"]);
                else unit = 0;
            }
            return new PackedGradeUnitWeightInfo()
            {
                UnitPrice = unit.ToString("#,##0.00")
            };
        }

        public PackedGradeUnitWeightInfo GetPackedUnitPrice(int crop, string packedGrade)
        {
            var command = new SqlCommand("sp_Sel_SUM_UNITPRICE_ONHAND_PD") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE", packedGrade));

            var results = ExecuteCommand(command, new string[] { "unitprice" });
            if (!results.Any())
                return null;

            foreach (var r in results)
            {
                if (decimal.TryParse(r["unitprice"], out unit))
                    unit = Decimal.Parse(r["unitprice"]);
                else unit = 0;
            }
            return new PackedGradeUnitWeightInfo()
            {
                UnitPrice = unit.ToString("#,##0.00")
            };
        }

        public PackedGradeUnitWeightInfo GetPackedWeight(int crop,
            string customerrs,
            string graders,
            string packedGrade)
        {
            var command = new SqlCommand("sp_Sel_SUM_WEIGHT_EXP_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", customerrs));
            command.Parameters.Add(new SqlParameter("@C_GRADE_RS", graders));
            command.Parameters.Add(new SqlParameter("@C_GRADE", packedGrade));

            var results = ExecuteCommand(command, new string[] { "Weight" });
            if (!results.Any())
                return null;

            foreach (var r in results)
            {
                if (decimal.TryParse(r["Weight"], out avg_weight))
                    avg_weight = Decimal.Parse(r["Weight"]);
                else
                    avg_weight = 0;
            }
            return new PackedGradeUnitWeightInfo()
            {
                AvgWeight = avg_weight.ToString("#,##0.00")
            };
        }

        //-------------------------------------- Insert new Export Value----------------------
        //Check Duplicate
        public bool ExistedPackedExportByCropAndPacked(int Crop,
            string Customer, 
            string Grade,
            string CustomerGrade)
        {
            var command = new SqlCommand("sp_PS_Get_ExportValue") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));
            command.Parameters.Add(new SqlParameter("@packedGrade", Grade));
            command.Parameters.Add(new SqlParameter("@customer", Customer));
            command.Parameters.Add(new SqlParameter("@customerPacked", CustomerGrade));
            var data = ExecuteCommand(command, new string[] { "N_CROP", "C_CUSTOMER_RS", "C_PACKED_GRADE", "C_PACKED_GRADE_RS", "N_EXPORT_VALUE" });
            if (!data.Any())
                return false;
            return true;
        }

        public bool IsClassifiedGradeDupplicated(int crop, string classifiedGrade)
        {
            if (GetClassifiedExportValue(crop)
                .Where(x => x.ClassifiedGrade == classifiedGrade)
                .Count() > 0)
                return true;
            else
                return false;
        }

        public void AddExportValue(int crop,
            string classifiedGrade,
            decimal exportValue,
            string user)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_B_EXPORT_VALUE"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", crop);
                cmd.Parameters.AddWithValue("@C_CLASSIFIED_GRADE", classifiedGrade);
                cmd.Parameters.AddWithValue("@C_GRADE_FLAG_CP", "C");
                cmd.Parameters.AddWithValue("@N_EXPORT_VALUE", exportValue);
                cmd.Parameters.AddWithValue("@C_CR_BY", user);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", user);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmExportValue");
                ExecuteCommand(cmd, new string[] { });
            }
        }

        public void AddPackedExportValue(int crop,
            string customerrs,
            string graders,
            string grade,
            decimal exportValue,
            string user)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_B_PACKED_EXP_VAL"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@N_CROP", crop);
                cmd.Parameters.AddWithValue("@C_CUSTOMER_RS", customerrs);
                cmd.Parameters.AddWithValue("@C_PACKED_GRADE_RS", graders);
                cmd.Parameters.AddWithValue("@C_PACKED_GRADE", grade);
                cmd.Parameters.AddWithValue("@N_EXPORT_VALUE", exportValue);
                cmd.Parameters.AddWithValue("@C_CR_BY", user);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", user);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmExportValue");
                ExecuteCommand(cmd, new string[] { });
            }
        }

        public bool DeletePacked(int Crop, string Customer, string CustomerPacked, string PackedGrade)
        {
            var command = new SqlCommand("sp_Del_PS_B_PACKED_EXP_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", Customer));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE_RS", CustomerPacked));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE", PackedGrade));
            var result = ExecuteCommand(command, new string[] { });
            return true;

        }

        public bool DeleteClassified(int Crop, string classifiedGrade)
        {
            var command = new SqlCommand("sp_Del_PS_B_EXPORT_VALUE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", Crop));
            command.Parameters.Add(new SqlParameter("@C_CLASSIFIED_GRADE", classifiedGrade));
            command.Parameters.Add(new SqlParameter("@C_GRADE_FLAG_CP", "C"));
            var result = ExecuteCommand(command, new string[] { });
            return true;

        }

        public bool EditClassified(int crop,
            string classifiedGrade,
            string gradeFlagCp,
            decimal exportValue,
            string user)
        {

            var command = new SqlCommand("sp_Upd_PS_B_EXPORT_VALUE") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CLASSIFIED_GRADE", classifiedGrade));
            command.Parameters.Add(new SqlParameter("@C_GRADE_FLAG_CP", gradeFlagCp));
            command.Parameters.Add(new SqlParameter("@N_EXPORT_VALUE", exportValue));
            command.Parameters.Add(new SqlParameter("@C_CR_BY", user));
            command.Parameters.Add(new SqlParameter("@D_CR_DATE", DateTime.Now));
            command.Parameters.Add(new SqlParameter("@C_UPD_BY", user));
            command.Parameters.Add(new SqlParameter("@D_UPD_DATE", DateTime.Now));
            command.Parameters.Add(new SqlParameter("@C_PROG_ID", "FrmExportValue"));
            var result = ExecuteCommand(command, new string[] { });
            return true;

        }

        public bool EditPacked(int crop,
            string customerrs,
            string graders,
            string grade,
            decimal exportValue,
            string user)
        {

            var command = new SqlCommand("sp_Upd_PS_B_PACKED_EXP_VAL") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_CUSTOMER_RS", customerrs));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE_RS", graders));
            command.Parameters.Add(new SqlParameter("@C_PACKED_GRADE", grade));
            command.Parameters.Add(new SqlParameter("@N_EXPORT_VALUE", exportValue));
            command.Parameters.Add(new SqlParameter("@C_CR_BY", user));
            command.Parameters.Add(new SqlParameter("@D_CR_DATE", DateTime.Now));
            command.Parameters.Add(new SqlParameter("@C_UPD_BY", user));
            command.Parameters.Add(new SqlParameter("@D_UPD_DATE", DateTime.Now));
            command.Parameters.Add(new SqlParameter("@C_PROG_ID", "FrmExportValue"));
            var result = ExecuteCommand(command, new string[] { });
            return true;

        }
    }
}