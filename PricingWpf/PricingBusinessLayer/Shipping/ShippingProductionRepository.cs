﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using PricingDomainModel;

namespace PricingBusinessLayer.Shipping
{
    public class ShippingProductionRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Total_Cost = 0;
        decimal Total_Weight = 0;
        decimal Average_Price = 0;
        public List<CropReceiving> GetProcessingCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CropProcess") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }

        public List<TypeInfo> GetTypeName()
        {
            var command = new SqlCommand("sp_SelAll_TYPE") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "type", "desc" });
            if (!results.Any())
                return new List<TypeInfo>();

            return results.Select(c => new TypeInfo()
            {
                TypeName = c["type"]
            }).ToList();
        }
        public List<TypeRBInfo> GetTypeRBName()
        {
            var command = new SqlCommand("sp_SelAll_TYPERB") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "type", "desc" });
            if (!results.Any())
                return new List<TypeRBInfo>();

            return results.Select(c => new TypeRBInfo()
            {
                TypeName = c["type"]
                ,
                Description = c["desc"]
            }).ToList();
        }
        //----------------------- Get List of Shipping Production Main ----------------------------
        public List<ShippingProductionInfo> GetJsonShippingProduct(int Crop)
        {
            var command = new SqlCommand("sp_PS_Get_ShippingProductByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@Crop", Crop));
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Season", "TypeRB", "Shipping_No" });
            List<ShippingProductionInfo> lstshipping = new List<ShippingProductionInfo>();

            foreach (var r in data)
            {

                lstshipping.Add(new ShippingProductionInfo()
                {
                    Doc_No = r["Doc_No"],
                    Shipping_No = r["Shipping_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = r["Type"]
                });
            }
            return lstshipping;
        }

        //Get New Processing Ready
        public List<ShippingProductionInfo> GetNewJsonShippingProduct()
        {
            string New_DocNo = "";
            string New_type = "";
            var command = new SqlCommand("sp_PS_Get_ShippingProductNotDone") { CommandType = CommandType.StoredProcedure };
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Season", "Shipping_No" });
            List<ShippingProductionInfo> lstShipping = new List<ShippingProductionInfo>();

            foreach (var r in data)
            {
                if (r["Doc_No"] == "")
                    New_DocNo = "";
                else
                    New_DocNo = r["Doc_No"];

                if (r["Type"] == "")
                    New_type = "";
                else
                    New_type = r["Type"];

                lstShipping.Add(new ShippingProductionInfo()
                {
                    Doc_No = New_DocNo,
                    Shipping_No = r["Shipping_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = New_type
                });
            }
            return lstShipping;
        }

        //---------------------------- Shipping Product Details -----------------------------------------------------
        public ShippingProductionMasterInfo GetShippingProductCalculate(string ShippingNo)
        {
            decimal cost = 0;
            decimal weight = 0;
            Total_Cost = 0;
            Total_Weight = 0;
            Average_Price = 0;
            //string tmp_RB;

            var command = new SqlCommand("sp_PS_Get_PdShippingProductByShippingNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@ShippingNo", ShippingNo));

            var gt = ExecuteCommand(command, new string[] { "DocNo", "DocDate", "Crop", "Type", "TypeRB", "ShippingNo" });
            if (!gt.Any())
                return null;

            //Get Total amount
            var cmd = new SqlCommand("sp_Sel_SHIPPING_MAT") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@ShippingNo", ShippingNo));

            var data_amt = ExecuteCommand(cmd, new string[] { "Company", "Packed Grade", "Case No", "Weight", "Unit Cost", "Amount", "bc" });

            foreach (var amt in data_amt)
            {
                if (decimal.TryParse(amt["Amount"], out cost))
                    cost = Decimal.Parse(amt["Amount"]);
                else cost = 0;
                if (decimal.TryParse(amt["Weight"], out weight))
                    weight = Decimal.Parse(amt["Weight"]);
                else weight = 0;

                Total_Cost = Total_Cost + cost;
                Total_Weight = Total_Weight + weight;
            }
            Average_Price = (Total_Cost / Total_Weight);

            //if (gt[0]["TypeRB"] == "R")
            //    tmp_RB = "Product";
            //tmp_RB = "By Product";

            return new ShippingProductionMasterInfo()
            {
                Crop = Int16.Parse(gt[0]["Crop"]),
                Type = gt[0]["Type"],
                TypeRB = gt[0]["TypeRB"],
                Shipping_No = gt[0]["ShippingNo"],
                Doc_No = gt[0]["DocNo"],
                Doc_Date = DateTime.Parse(gt[0]["DocDate"].ToString()),
                Total_Cost = Math.Round(Total_Cost, 2),
                Total_Weight = Math.Round(Total_Weight, 2),
                Average_Price = Math.Round(Average_Price, 2)
            };
        }

        public List<ShippingProductDetails> GetJsonShippingDetails(string ShippingNo)
        {
            try
            {
                var command = new SqlCommand("sp_Sel_SHIPPING_MAT") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@ShippingNo", ShippingNo));

                var results = ExecuteCommand(command, new string[] { "Company", "Packed Grade", "Case No", "Weight", "Unit Cost", "Amount", "bc" });
                List<ShippingProductDetails> lstPP = new List<ShippingProductDetails>();
                foreach (var r in results)
                {
                    lstPP.Add(new ShippingProductDetails()
                    {
                        Company = r["Company"],
                        PackedGrade = r["Packed Grade"],
                        CaseNo = r["Case No"],
                        Weight = Math.Round(double.Parse(r["Weight"]), 2).ToString("N2"),
                        UnitCost = r["Unit Cost"],
                        Amount = Math.Round(double.Parse(r["Amount"]), 2).ToString("N2")
                    });
                }
                return lstPP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //----------- Adding New Shipping Product -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "SHP"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "SHP"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }

        public void AddShippingProduct(string DocNo, DateTime DocDate, string ShippingNo, int Crop, string Type, string TypeRB, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_SHIP_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_SHIP_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_SHIP_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_SHIP_NO", ShippingNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", "");
                cmd.Parameters.AddWithValue("@C_SHIP_TYPE_RB", TypeRB);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmShipping");
                ExecuteCommand(cmd, new string[] { });
            }
        }

        //----------- Delete Shipping
        public void DeleteShipping(string ShippingNo)
        {
            SqlCommand command;
            command = new SqlCommand("sp_Del_PS_SHIP_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_SHIP_DOC_NO", ShippingNo));
            var result = ExecuteCommand(command, new string[] { });
        }
    }
}