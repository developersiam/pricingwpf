﻿using PricingBusinessLayer.Account;
using PricingBusinessLayer.GreenLeaf;
using PricingBusinessLayer.Processing;
using PricingBusinessLayer.Reports;
using PricingBusinessLayer.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingBusinessLayer
{
    public static class PricingService
    {
        public static ReceivingRepository ReceivingRepository()
        {
            ReceivingRepository obj = new ReceivingRepository();
            return obj;
        }

        public static GreenTransferRepository GreenTransferRepository()
        {
            GreenTransferRepository obj = new GreenTransferRepository();
            return obj;
        }

        public static HandStripRepository HandStripRepository()
        {
            HandStripRepository obj = new HandStripRepository();
            return obj;
        }

        public static RegradeRepository RegradeRepository()
        {
            RegradeRepository obj = new RegradeRepository();
            return obj;
        }

        public static AccountRepository AccountRepository()
        {
            AccountRepository obj = new AccountRepository();
            return obj;
        }
		
        public static ShippingProductionRepository ShippingProductionRepository()
        {
            ShippingProductionRepository repo = new ShippingProductionRepository();
            return repo;
        }

        public static RedryCodeRepository RedryCodeRepository()
        {
            RedryCodeRepository repo = new RedryCodeRepository();
            return repo;
        }

        public static RedryChargeRepository RedryChargeRepository()
        {
            RedryChargeRepository repo = new RedryChargeRepository();
            return repo;
        }

        public static IStoreProcedureBL StoreProcedureBL()
        {
            IStoreProcedureBL obj = new StoreProcedureBL();
			return obj;
		}
        public static ProcessProductRepository ProcessProductRepository()
        {
            ProcessProductRepository obj = new ProcessProductRepository();
            return obj;
        }

        public static ProcessReadyRepository ProcessReadyRepository()
        {
            ProcessReadyRepository obj = new ProcessReadyRepository();
            return obj;
        }

        public static ExportRepository ExportRepository()
        {
            ExportRepository obj = new ExportRepository();
            return obj;
        }

        public static PackingMatRepository PackingMatRepository()
        {
            PackingMatRepository obj = new PackingMatRepository();
            return obj;
        }

        public static ExchangeRateRepository ExchangeRateRepository()
        {
            ExchangeRateRepository obj = new ExchangeRateRepository();
            return obj;
        }
        public static VarianceAdjustRepository VarianceAdjustRepository()
        {
            VarianceAdjustRepository obj = new VarianceAdjustRepository();
            return obj;
        }
        public static VarianceAdjustDetailsRepository VarianceAdjustDetailsRepository()
        {
            VarianceAdjustDetailsRepository obj = new VarianceAdjustDetailsRepository();
            return obj;
        }
        public static BroughtForwardRepository BroughtForwardRepository()
        {
            BroughtForwardRepository obj = new BroughtForwardRepository();
            return obj;
        }
    }
}
