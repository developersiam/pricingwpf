﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;
using PricingDomainModel;

namespace PricingBusinessLayer.GreenLeaf
{
    public class GreenTransferRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        //----------------------- Get List of GreenTransfer Main ----------------------------
        public List<GreenTransferMasterInfo> GetJsonGreenTMaster(string Crop)
        {
            var command = new SqlCommand("sp_PS_Get_GreenTransferByCorp") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));

            var data = ExecuteCommand(command, new string[] { "C_TRANSFER_NO", "C_TRAN_DOC_NO", "D_TRAN_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<GreenTransferMasterInfo> lstGreenM = new List<GreenTransferMasterInfo>();

            foreach (var r in data )
            {
                lstGreenM.Add(new GreenTransferMasterInfo()
                {
                    Transfer_Doc_No = r["C_TRAN_DOC_NO"],
                    Transfer_No = r["C_TRANSFER_NO"],
                    Transfer_Doc_Date = DateTime.Parse(r["D_TRAN_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstGreenM;
        }

        //Get New Green Transfer
        public List<GreenTransferMasterInfo> GetJsonAddGreenTMaster(int Crop)
        {
            var command = new SqlCommand("sp_PS_Get_GreenTransferMasterNotDone") { CommandType = CommandType.StoredProcedure };

            var data = ExecuteCommand(command, new string[] { "C_TRANSFER_NO", "C_TRAN_DOC_NO", "D_TRAN_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<GreenTransferMasterInfo> lstGreenM = new List<GreenTransferMasterInfo>();
            foreach (var r in data)
            {
                lstGreenM.Add(new GreenTransferMasterInfo()
                {
                    Transfer_Doc_No = r["C_TRAN_DOC_NO"],
                    Transfer_No = r["C_TRANSFER_NO"],
                    Transfer_Doc_Date = DateTime.Parse(r["D_TRAN_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstGreenM;
        }
        //---------------------------- Green Transfer Details -----------------------------------------------------
        public MatIsGreenTransferInfo GetGreenTCalculate(string TFNo)
        {
            var command = new SqlCommand("sp_PS_Get_MatISGreenTransferByTFNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@TFNO", TFNo));

            var gt = ExecuteCommand(command, new string[] { "TransferNo", "DocumentNo", "DocumentDate", "CompanyCode", "Crop", "Type", "Season", "TotalWeight", "AveragePrice", "TotalAmount" });
            if (!gt.Any())
                return null;
            return new MatIsGreenTransferInfo()
            {
                TransferNo = gt[0]["TransferNo"],
                DocumentNo = gt[0]["DocumentNo"],
                DocumentDate = DateTime.Parse(gt[0]["DocumentDate"].ToString()),
                CompanyCode = gt[0]["CompanyCode"],
                Crop = Int16.Parse(gt[0]["Crop"]),
                Type = gt[0]["Type"],
                Season = gt[0]["Season"],
                TotalWeight = Double.Parse(gt[0]["TotalWeight"]).ToString("#,##0.00"),
                AveragePrice = Double.Parse(gt[0]["AveragePrice"]).ToString("#,##0.00"),
                TotalAmount = Double.Parse(gt[0]["TotalAmount"]).ToString("#,##0.00")
            };
        }
        public List<GreenTransferDetails> GetJsonGreenTDetails(string TFNo)
        {
            var command = new SqlCommand("sp_Sel_GREEN_TRANS_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@TFNO", TFNo));

            var results = ExecuteCommand(command, new string[] { "Bale No", "Supplier", "Green Grade", "From Classify", "To Classify", "Weight", "Unit Price", "Amount", "bc", "tfno", "crop", "company", "type", "subtype" });
            List<GreenTransferDetails> lstGreenTD = new List<GreenTransferDetails>();
            foreach (var r in results)
            {
                var m = new GreenTransferDetails();
                m.BaleNo = r["Bale No"];
                m.Supplier = r["Supplier"];
                m.GreenGrade = r["Green Grade"];
                m.FromClassify = r["From Classify"];
                m.ToClassify = r["To Classify"];
                m.UnitPrice = Convert.ToString(Math.Round(double.Parse(r["Unit Price"]), 2));
                var _weight = Math.Round(double.Parse(r["Weight"]), 2);
                m.Weight = _weight.ToString("N2");
                var _amount = Math.Round(double.Parse(r["Amount"]), 2);
                m.Amount = _amount.ToString("N2");
                lstGreenTD.Add(m);
            }
            return lstGreenTD;
        }
        //----------- Delete Green Transfer ------------------
        public void DeleteGreenT(string TFDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_GREEN_TRANSFER_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_TRAN_DOC_NO", TFDocNo));
            ExecuteCommand(command, new string[] { });
        }
        //----------- Adding New Green Transfer -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "TRG"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "TRG"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }
        public void AddGreenTransfer(string DocNo, DateTime DocDate, string TFNo, int Crop, string Company, string Type, string Season, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_GREEN_TRANSFER_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_TRAN_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_TRAN_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_TRANSFER_NO", TFNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_COMPANY", Company);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "frmGreenTransfer");
                ExecuteCommand(cmd, new string[] { });
            }
        }

    }
}