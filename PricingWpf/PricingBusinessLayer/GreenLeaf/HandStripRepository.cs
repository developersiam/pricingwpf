﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using PricingDomainModel;

namespace PricingBusinessLayer.GreenLeaf
{
    public class HandStripRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        //----------------------- Get List of Hand Strip Main ----------------------------
        public List<HandStripMasterInfo> GetJsonHandStripMaster(int crop)
        {
            var command = new SqlCommand("sp_PS_Get_HandStripByCorp") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", crop));

            var data = ExecuteCommand(command, new string[] { "C_HAND_STRIP_NO", "C_HAND_DOC_NO", "D_HAND_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<HandStripMasterInfo> lstHandM = new List<HandStripMasterInfo>();

            foreach (var r in data)
            {
                lstHandM.Add(new HandStripMasterInfo()
                {
                    HandStrip_Doc_No = r["C_HAND_DOC_NO"],
                    HandStrip_No = r["C_HAND_STRIP_NO"],
                    HandStrip_Doc_Date = DateTime.Parse(r["D_HAND_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstHandM;
        }
        //Get New Hand Strip
        public List<HandStripMasterInfo> GetJsonAddHandSMaster(int crop)
        {
            var command = new SqlCommand("sp_PS_Get_HandStripMasterNotDone") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", crop));

            var data = ExecuteCommand(command, new string[] { "C_HAND_STRIP_NO", "C_HAND_DOC_NO", "D_HAND_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<HandStripMasterInfo> lstHandM = new List<HandStripMasterInfo>();
            foreach (var r in data)
            {
                lstHandM.Add(new HandStripMasterInfo()
                {
                    HandStrip_Doc_No = r["C_HAND_DOC_NO"],
                    HandStrip_No = r["C_HAND_STRIP_NO"],
                    HandStrip_Doc_Date = DateTime.Parse(r["D_HAND_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstHandM;
        }

        //---------------------------- Green Transfer Details -----------------------------------------------------
        public MatIsHandStripInfo GetHandSCalculate(string HSNo)
        {
            var command = new SqlCommand("sp_PS_Get_MatISHandStripByHSNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@HSNO", HSNo));

            var gt = ExecuteCommand(command, new string[] { "HandStripNo", "DocumentNo", "DocumentDate", "CompanyCode", "Crop", "Type", "Season", "TotalWeight", "AveragePrice", "TotalAmount" });
            if (!gt.Any())
                return null;
            return new MatIsHandStripInfo()
            {
                HandStripNo = gt[0]["HandStripNo"],
                DocumentNo = gt[0]["DocumentNo"],
                DocumentDate = DateTime.Parse(gt[0]["DocumentDate"].ToString()),
                CompanyCode = gt[0]["CompanyCode"],
                Crop = Int16.Parse(gt[0]["Crop"]),
                Type = gt[0]["Type"],
                Season = gt[0]["Season"],
                TotalWeight = Double.Parse(gt[0]["TotalWeight"]).ToString("#,##0.00"),
                AveragePrice = Double.Parse(gt[0]["AveragePrice"]).ToString("#,##0.00"),
                TotalAmount = Double.Parse(gt[0]["TotalAmount"]).ToString("#,##0.00")
            };
        }
        public List<HandStripDetails> GetJsonHandSDetails(string HSNo)
        {
            var command = new SqlCommand("sp_Sel_HAND_STRIP_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@HSNO", HSNo));

            var results = ExecuteCommand(command, new string[] { "Bale No", "Curer", "Green Grade", "Classified Grade", "FLAG_IO", "Weight", "Unit Price", "Amount", "bc", "hsno", "rcno", "docno", "crop", "company", "type", "subtype", "PriceCode" , "FCV", "isno"});
            List<HandStripDetails> lstHandSD = new List<HandStripDetails>();
            foreach (var r in results)
            {
                lstHandSD.Add(new HandStripDetails()
                {
                    BaleNo = r["Bale No"],
                    Curer = r["Curer"],
                    GreenGrade = r["Green Grade"],
                    Classified = r["Classified Grade"],
                    FlagIO = r["FLAG_IO"],
                    UnitPrice = Convert.ToString(Math.Round(double.Parse(r["Unit Price"]), 2)),
                    Weight = Convert.ToString(Math.Round(double.Parse(r["Weight"]), 2)),
                    Amount = (Math.Round(double.Parse(r["Amount"]), 2)).ToString("N2")
                });
            }
            return lstHandSD;
        }

        //----------- Delete Hand Strip ------------------
        public void DeleteHandS(string HSDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_HAND_STRIP_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_HAND_DOC_NO", HSDocNo));
            ExecuteCommand(command, new string[] { });
        }
        //----------- Adding New Hand Strip -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "HND"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "HND"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }
        public void AddHandStrip(string DocNo, DateTime DocDate, string HSNo, int Crop, string Company, string Type, string Season, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_HAND_STRIP_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_HAND_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_HAND_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_HAND_STRIP_NO", HSNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_COMPANY", Company);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmHandStrip");
                ExecuteCommand(cmd, new string[] { });
            }
        }


    }
}