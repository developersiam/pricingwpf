﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;
using PricingDomainModel;

namespace PricingBusinessLayer.GreenLeaf
{
    public class ReceivingRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //Get Receiving Master Details
        public ReceivingInfo GetReceiving(int crop)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("sp_PS_Get_ReceivingByCorp")
                { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@crop", crop));

                var data = ExecuteCommand(command, new string[] { "C_REC_DOC_NO", "C_REC_NO", "N_CROP" });
                if (!data.Any())
                    return null;
                return new ReceivingInfo()
                {
                    C_REC_DOC_NO = data[0]["C_REC_DOC_NO"],
                    C_REC_NO = data[0]["C_REC_NO"],
                    N_CROP = Convert.ToInt16(data[0]["N_CROP"])
                };
            }
        }

        public List<CropReceiving> GetReceivingCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        public List<ReceivingInfo> GetReceivingDocument()
        {            
            var command = new SqlCommand("sp_PS_Get_ReceivingAndDocument") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "C_REC_DOC_NO", "C_REC_NO", "N_CROP" });
            if (!results.Any())
                return new List<ReceivingInfo>();
            return results.Select(r => new ReceivingInfo()
            {
                C_REC_DOC_NO = r["C_REC_DOC_NO"],
                C_REC_NO = r["C_REC_NO"],
                N_CROP = Convert.ToInt16(r["N_CROP"])
            }).ToList();
        }

        //----------------------- Show List on Receiving Main ----------------------------
        public List<ReceivingMasterInfo> GetJsonReceivingMaster(string Crop)
        {
            var command = new SqlCommand("sp_PS_Get_ReceivingByCorp") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));

            var data = ExecuteCommand(command, new string[] { "C_REC_DOC_NO", "C_REC_NO", "C_BUY_DOC_NO", "D_REC_DOC_DATE", "N_CROP", "C_COMPANY", "C_TYPE", "C_SEASON", "C_CURER_CODE", "C_CURER_NAME", "C_AREA", "C_GREEN_PRICE_CODE", "C_REC_TYPE_RB", "C_PROG_ID" });
            List<ReceivingMasterInfo> lstReceivingM = new List<ReceivingMasterInfo>();
            foreach(var r in data)
            {
                lstReceivingM.Add(new ReceivingMasterInfo()
                {
                    C_REC_DOC_NO = r["C_REC_DOC_NO"],
                    C_REC_NO = r["C_REC_NO"],
                    N_CROP = Int16.Parse(r["N_CROP"]),
                    D_REC_DOC_DATE = DateTime.Parse(r["D_REC_DOC_DATE"]),
                    C_BUY_DOC_NO = r["C_BUY_DOC_NO"],
                    C_COMPANY = r["C_COMPANY"],
                    C_TYPE = r["C_TYPE"],
                    C_SEASON = r["C_SEASON"],
                    C_CURER_CODE = r["C_CURER_CODE"],
                    C_CURER_NAME = r["C_CURER_NAME"],
                    C_AREA = r["C_AREA"],
                    C_GREEN_PRICE_CODE = r["C_GREEN_PRICE_CODE"],
                    C_REC_TYPE_RB = r["C_REC_TYPE_RB"],
                    C_PROG_ID = r["C_PROG_ID"]
                });
            }
            return lstReceivingM;
        }
        public List<ReceivingMasterInfo> GetJsonAddReceivingMaster()
        {
            var command = new SqlCommand("sp_PS_Get_ReceivingMasterNotDone") { CommandType = CommandType.StoredProcedure };

            var data = ExecuteCommand(command, new string[] { "DocumentNo", "ReceivingNo", "BuyingDocNo", "DocumentDate", "Crop", "Company", "Type", "Season", "CurerCode", "CurerName", "Area", "PriceCode", "WeightBuy", "WeightReceived", "AveragePriceBuy", "AveragePriceRec", "TotalAmount" });
            List<ReceivingMasterInfo> lstReceivingM = new List<ReceivingMasterInfo>();
            foreach (var r in data)
            {
                lstReceivingM.Add(new ReceivingMasterInfo()
                {
                    C_REC_DOC_NO = r["DocumentNo"],
                    C_REC_NO = r["ReceivingNo"],
                    N_CROP = Int16.Parse(r["Crop"]),
                    D_REC_DOC_DATE = DateTime.Parse(r["DocumentDate"]),
                    C_BUY_DOC_NO = r["BuyingDocNo"],
                    C_COMPANY = r["Company"],
                    C_TYPE = r["Type"],
                    C_SEASON = r["Season"],
                    C_CURER_CODE = r["CurerCode"],
                    C_CURER_NAME = r["CurerName"],
                    C_AREA = r["Area"],
                    C_GREEN_PRICE_CODE = r["PriceCode"],
                    C_REC_TYPE_RB = "",
                    C_PROG_ID = ""
                });
            }
            return lstReceivingM;
        }
        //-------------------End Show List on Receiving Main ----------------------------

        //------------ Show Receiving Details -----------------------
        public MatReceivingInfo GetReceivingCalculate(string RecNo)
        {
            var command = new SqlCommand("sp_PS_Get_MatReceivingByRecNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@DOCNO", int.Parse(RecNo)));

            var mr = ExecuteCommand(command, new string[] { "ReceivingNo", "DocumentNo", "BuyingDocNo", "DocumentDate", "CompanyCode", "Crop", "Type", "Season", "CurerCode", "CurerName", "Area", "RecordType", "GreenPriceCode", "WeightBuy", "WeightReceived", "AveragePriceBuy", "AveragePriceRec", "TotalAmount" });
            if (!mr.Any())
                return null;
            return new MatReceivingInfo()
            {
                ReceivingNo = mr[0]["ReceivingNo"],
                DocumentNo = mr[0]["DocumentNo"],
                BuyingDocNo = mr[0]["BuyingDocNo"],
                DocumentDate = DateTime.Parse(mr[0]["DocumentDate"].ToString()),
                CompanyCode = mr[0]["CompanyCode"],
                Crop = Int16.Parse(mr[0]["Crop"]),
                Type = mr[0]["Type"],
                Season = mr[0]["Season"],
                CurerCode = mr[0]["CurerCode"],
                CurerName = mr[0]["CurerName"],
                Area = mr[0]["Area"],
                RecordType = mr[0]["RecordType"],
                GreenPriceCode = mr[0]["GreenPriceCode"],
                WeightBuy = double.Parse(Convert.ToDouble(mr[0]["WeightBuy"]).ToString("#,##0.00")),
                WeightReceived = double.Parse(Convert.ToDouble(mr[0]["WeightReceived"]).ToString("#,##0.00")),
                AveragePriceBuy = double.Parse(Convert.ToDouble(mr[0]["AveragePriceBuy"]).ToString("#,##0.00")),
                AveragePriceRec = double.Parse(Convert.ToDouble(mr[0]["AveragePriceRec"]).ToString("#,##0.00")),
                TotalAmount = double.Parse(Convert.ToDouble(mr[0]["TotalAmount"]).ToString("#,##0.00"))
            };
        }

        public List<ReceivingDetails> GetJsonReceivingDetails(string RecDocNo)
        {
            var command = new SqlCommand("sp_Sel_PS_RECEIVING_DWC_REC_DOC_NO") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_REC_DOC_NO", RecDocNo));

            var results = ExecuteCommand(command, new string[] { "C_BALE_NO", "C_GREEN_GRADE", "C_CLASSIFIED_GRADE", "N_BUY_WEIGHT", "N_WEIGHT", "N_UNIT_PRICE", "N_REC_UNIT_PRICE", "N_T_CHARGE", "N_TOTAL_UNIT_PRICE", "N_AMOUNT", "C_FCV_CLASS", "C_PROG_ID", "C_REC_DOC_NO", "C_BARCODE", "C_CR_BY", "D_CR_DATE", "D_UPD_DATE", "C_UPD_BY" });
            List<ReceivingDetails> lstReceivingD = new List<ReceivingDetails>();
            foreach(var r in results)
            {
                lstReceivingD.Add(new ReceivingDetails()
                {
                    BaleNo = r["C_BALE_NO"],
                    GreenGrade = r["C_GREEN_GRADE"],
                    ClassifiedGrade = r["C_CLASSIFIED_GRADE"],
                    BuyWeight = r["N_BUY_WEIGHT"],
                    Weight = r["N_WEIGHT"],
                    UnitPrice = r["N_UNIT_PRICE"],
                    TCharge = r["N_T_CHARGE"],
                    TotalUnitPrice = r["N_TOTAL_UNIT_PRICE"],
                    Amount = Math.Round(double.Parse(r["N_AMOUNT"]), 2).ToString("N2"),
                    FCVClass = r["C_FCV_CLASS"]
                });
            }
            return lstReceivingD;         
        }
        public List<ReceivingDetails> GetJsonAddReceivingDetails(int RecDocNo, int Crop)
        {
            var command = new SqlCommand("sp_Sel_PS_V_MAT_RECEIVEWDOC_NO") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@DOCNO", RecDocNo));
            command.Parameters.Add(new SqlParameter("@CROP", Crop));

            var results = ExecuteCommand(command, new string[] { "crop", "rcno", "docno", "bc", "Bale No", "company", "supplier", "type", "subtype", "Buying Grade", "Classified Grade", "PriceCode", "BuyWeight", "RecWeight", "Unit Price", "Trans Charge", "New Unit Price", "Amount", "FCV" });
            List<ReceivingDetails> lstReceivingD = new List<ReceivingDetails>();
            foreach (var r in results)
            {
                lstReceivingD.Add(new ReceivingDetails()
                {
                    BaleNo = r["Bale No"],
                    GreenGrade = r["Buying Grade"],
                    ClassifiedGrade = r["Classified Grade"],
                    BuyWeight = Convert.ToString(Math.Round(double.Parse(r["BuyWeight"]),2)),
                    Weight = Convert.ToString(Math.Round(double.Parse(r["RecWeight"]),2)),
                    UnitPrice = Convert.ToString(Math.Round(double.Parse(r["Unit Price"]),2)),
                    TCharge = r["Trans Charge"],
                    TotalUnitPrice = Convert.ToString(Math.Round(double.Parse(r["New Unit Price"]),2)),
                    Amount = Math.Round(double.Parse(r["Amount"]),2).ToString("N2"), 
                    FCVClass = r["FCV"]
                });
            }
            return lstReceivingD;
        }
        //------------------ End Show Receiving Details -----------------------
        public void DeleteReceiving(string RecDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_RECEIVING_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_REC_DOC_NO", RecDocNo));
            ExecuteCommand(command, new string [] { });
        }

        //Get New Receiving Document No
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "RCV"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "RCV"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else               
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
                return DocumentNo;           
        }
        public void AddReceiving(string RecDocNo, DateTime RecDocDate, string RecNo, string BuyDocNo, int Crop, string Company, string Type, string Season, string CurerCode, string CurerName, string Area, string GreenPriceCode, string RecordType, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_RECEIVING_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_REC_DOC_NO", RecDocNo);
                cmd.Parameters.AddWithValue("@D_REC_DOC_DATE", RecDocDate);
                cmd.Parameters.AddWithValue("@C_REC_NO", RecNo);
                cmd.Parameters.AddWithValue("@C_BUY_DOC_NO", BuyDocNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_COMPANY", Company);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_CURER_CODE", CurerCode);
                cmd.Parameters.AddWithValue("@C_CURER_NAME", CurerName);
                cmd.Parameters.AddWithValue("@C_AREA", Area);
                cmd.Parameters.AddWithValue("@C_GREEN_PRICE_CODE", GreenPriceCode);
                cmd.Parameters.AddWithValue("@C_REC_TYPE_RB", RecordType);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "frmReceiving");
                ExecuteCommand(cmd, new string[] {});
            }
        }
        //public MatReceivingInfo GetReceivingCalculateForAdding(int DocNo)
        //{
        //    var command = new SqlCommand("sp_PS_GetReceivingForAdd") { CommandType = CommandType.StoredProcedure };
        //    command.Parameters.Add(new SqlParameter("@DOCNO", DocNo));

        //    var mr = ExecuteCommand(command, new string[] { "ReceivingNo", "Company", "Crop", "Type", "Season", "CurerCode", "CurerName", "Area", "GreenPriceCode", "BuyWeight", "RecWeight", "AveragePriceBuy", "AveragePriceRec", "Amount" });
        //    if (!mr.Any())
        //        return null;
        //    return new MatReceivingInfo()
        //    {
        //        ReceivingNo = mr[0]["ReceivingNo"],
        //        //DocumentNo = mr[0]["DocumentNo"],
        //        //BuyingDocNo = mr[0]["BuyingDocNo"],
        //        DocumentDate = DateTime.Today,
        //        CompanyCode = mr[0]["Company"],
        //        Crop = Int16.Parse(mr[0]["Crop"]),
        //        Type = mr[0]["Type"],
        //        Season = mr[0]["Season"],
        //        CurerCode = mr[0]["CurerCode"],
        //        CurerName = mr[0]["CurerName"],
        //        Area = mr[0]["Area"],
        //        //RecordType = mr[0]["RecordType"],
        //        GreenPriceCode = mr[0]["GreenPriceCode"],
        //        WeightBuy = double.Parse(mr[0]["BuyWeight"]),
        //        WeightReceived = double.Parse(mr[0]["RecWeight"]),
        //        AveragePriceBuy = double.Parse(mr[0]["AveragePriceBuy"]),
        //        AveragePriceRec = double.Parse(mr[0]["AveragePriceRec"]),
        //        TotalAmount = double.Parse(mr[0]["Amount"])
        //    };
        //}

        //public List<ReceivingMasterInfo> GetReceivingDocumentAll()
        //{
        //    var command = new SqlCommand("sp_SelAll_PS_RECEIVING_M") { CommandType = CommandType.StoredProcedure };
        //    var results = ExecuteCommand(command, new string[] { "C_REC_DOC_NO", "C_REC_NO", "N_CROP", "D_REC_DOC_DATE", "C_BUY_DOC_NO", "C_COMPANY", "C_TYPE", "C_SEASON", "C_CURER_CODE", "C_CURER_NAME", "C_AREA", "C_GREEN_PRICE_CODE", "C_REC_TYPE_RB", "C_PROG_ID" });
        //    if (!results.Any())
        //        return new List<ReceivingMasterInfo>();
        //    return results.Select(r => new ReceivingMasterInfo()
        //    {
        //        C_REC_DOC_NO = r["C_REC_DOC_NO"],
        //        C_REC_NO = r["C_REC_NO"],
        //        N_CROP = Convert.ToInt16(r["N_CROP"]),
        //        D_REC_DOC_DATE =DateTime.Parse(r["D_REC_DOC_DATE"]),
        //        C_BUY_DOC_NO = r["C_BUY_DOC_NO"],
        //        C_COMPANY = r["C_COMPANY"],
        //        C_TYPE = r["C_TYPE"],
        //        C_SEASON = r["C_SEASON"],
        //        C_CURER_CODE = r["C_CURER_CODE"],
        //        C_CURER_NAME = r["C_CURER_NAME"],
        //        C_AREA = r["C_AREA"],
        //        C_GREEN_PRICE_CODE = r["C_GREEN_PRICE_CODE"],
        //        C_REC_TYPE_RB = r["C_REC_TYPE_RB"],
        //        C_PROG_ID = r["C_PROG_ID"]
        //    }).ToList();
        //}

        //public List<ReceivingListNotDone> GetAllNotDoneReceiving()
        //{
        //    var command = new SqlCommand("sp_PS_Get_ReceivingNotDone") { CommandType = CommandType.StoredProcedure };
        //    var results = ExecuteCommand(command, new string[] { "crop", "docno", "N_NUM" });
        //    if (!results.Any())
        //        return new List<ReceivingListNotDone>();

        //    return results.Select(c => new ReceivingListNotDone()
        //    {
        //        DocNo = Convert.ToInt32(c["docno"]),
        //        Crop = Convert.ToInt16(c["crop"])
        //    }).ToList();
        //}
    }
}