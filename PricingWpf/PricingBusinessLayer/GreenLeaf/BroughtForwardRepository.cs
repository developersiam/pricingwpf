﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;
using PricingDomainModel;

namespace PricingBusinessLayer.GreenLeaf
{
    public class BroughtForwardRepository : GenericDataRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        public List<SubTypeInfo> GetSubTypeName()
        {
            var command = new SqlCommand("sp_Receiving_SEL_Subtype") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "subtype" });
            if (!results.Any())
                return new List<SubTypeInfo>();

            return results.Select(c => new SubTypeInfo()
            {
                subtype = c["subtype"]
            }).ToList();
        }
        public List<CompanyInfo> GetCompanies()
        {
            var command = new SqlCommand("sp_Receiving_SEL_Company") { CommandType = CommandType.StoredProcedure };
            var data = ExecuteCommand(command, new string[] { "code", "name", "remark", "dtrecord", "user", "FromBuyingSystem", "FromLoadBales" });
            List<CompanyInfo> lstComp = new List<CompanyInfo>();

            foreach (var r in data)
            {
                lstComp.Add(new CompanyInfo()
                {
                    Code = Int16.Parse(r["code"]),
                    Name = r["name"],
                    Remark = r["remark"],
                    Dtrecord = r["dtrecord"],
                    User = r["user"],
                    FromBuyingSystem = Boolean.Parse(r["FromBuyingSystem"]),
                    FromLoadBales = Boolean.Parse(r["FromLoadBales"])
                });
            }
            return lstComp;
        }
        public OnhandMatInfo GetOnhandMats(int crop,
            int company,
            string type,
            string subtype)
        {
            var rcdate = "31/12/" + Convert.ToString(crop).Substring(2,2);
            var command = new SqlCommand("SP_Sel_PS_ONHAND_CROP_COMPANY") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@company", company));
            command.Parameters.Add(new SqlParameter("@crop", crop));
            command.Parameters.Add(new SqlParameter("@type", type));
            command.Parameters.Add(new SqlParameter("@subtype", subtype));
            command.Parameters.Add(new SqlParameter("@date", rcdate));

            var oh = ExecuteCommand(command, new string[] { "onhand_crop", "onhand_bales", "onhand_weight", "onhand_amt", "onhand_amt_unit" });
            if (!oh.Any())
                return null;

            return new OnhandMatInfo()
            {
                onhand_crop = Convert.ToInt32(oh[0]["onhand_crop"]),
                onhand_bales = Convert.ToInt32(oh[0]["onhand_bales"]),
                onhand_amt = Convert.ToDouble(oh[0]["onhand_amt"]),
                onhand_weight = Convert.ToDouble(oh[0]["onhand_weight"]),
                onhand_amt_unit = Convert.ToDouble(oh[0]["onhand_amt_unit"]),
            };
        }
        public List<BroughtForwardInfo> GetBroughtForwardByCrop(int fromcrop, int tocrop)
        {
            var command = new SqlCommand("sp_PS_Get_BroughtForwardByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.AddWithValue("@fromcrop", fromcrop);
            command.Parameters.AddWithValue("@tocrop", tocrop);
            var data = ExecuteCommand(command, new string[] { "bfno", "fromcrop", "tocrop", "type", "subtype", "company", "locked", "dtrecord", "bales" });
            List<BroughtForwardInfo> lstbf = new List<BroughtForwardInfo>();
            foreach (var r in data)
            {
                lstbf.Add(new BroughtForwardInfo()
                {
                    bfno = r["bfno"],
                    fromcrop = Int16.Parse(r["fromcrop"]),
                    tocrop = Int16.Parse(r["tocrop"]),
                    type = r["type"],
                    season = r["subtype"],
                    company = r["company"],
                    locked = bool.Parse(r["locked"]),
                    dtrecord = Convert.ToDateTime(r["dtrecord"]),
                    total_bales = Int16.Parse(r["bales"])
                });
            }
            return lstbf;
        }
        public string InsertMatRCNo(int Crop, string User)
        {

            string Rcno;
            //Update to running no.
            var cmd = new SqlCommand("sp_PS_Insert_MatRcNo") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@crop", Crop);
            cmd.Parameters.AddWithValue("@user", User);
            cmd.Parameters.Add("@newRcno", SqlDbType.NVarChar,15).Direction = ParameterDirection.Output;
            var result = ExecuteCommand(cmd, new string[] { "max_rcno" });

            if (!result.Any()) return String.Empty;
            else Rcno =  Convert.ToString(Crop).Substring(2,2) + "-" + Int32.Parse(result[0]["max_rcno"]).ToString("D4");

            return Rcno;
        }

        public void InsertMatRC(string rcno, int Crop, string Type, string User)
        {
            using (var cmd = new SqlCommand("sp_Receiving_INS_MatRC"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rcno", rcno);
                cmd.Parameters.AddWithValue("@crop", Crop);
                cmd.Parameters.AddWithValue("@type", Type);
                cmd.Parameters.AddWithValue("@date", DateTime.Now);
                cmd.Parameters.AddWithValue("@place", "Picing");
                cmd.Parameters.AddWithValue("@truckno", "Stec-BF");
                cmd.Parameters.AddWithValue("@buyer", "GreenLeaf");
                cmd.Parameters.AddWithValue("@classifier", User);
                cmd.Parameters.AddWithValue("@starttime", DateTime.Now);
                cmd.Parameters.AddWithValue("@rcfrom", "BroughtForward");
                cmd.Parameters.AddWithValue("@remark", "");
                cmd.Parameters.AddWithValue("@machine", "BF");
                cmd.Parameters.AddWithValue("@user", User);
                cmd.Parameters.AddWithValue("@TransportationDocumentCode", "BroughtForward");
                cmd.Parameters.AddWithValue("@InvoiceNo", "");
                ExecuteCommand(cmd, new string[] { });
            }
        }

        public string InsertMatBF(int fromCrop, int toCrop, string User)
        {
            string bfno;
            var cmd = new SqlCommand("sp_PS_Insert_MatBFNo") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@fromcrop", fromCrop);
            cmd.Parameters.AddWithValue("@tocrop", toCrop);
            cmd.Parameters.AddWithValue("@user", User);
            cmd.Parameters.Add("@newBFno", SqlDbType.NVarChar, 15).Direction = ParameterDirection.Output;
            var result = ExecuteCommand(cmd, new string[] { "max_bfno" });

            if (!result.Any()) return String.Empty;
            else bfno = Convert.ToString(toCrop).Substring(2, 2) + "-" + Int32.Parse(result[0]["max_bfno"]).ToString("D4");

            return bfno;
        }

        public void UpdateAndInsertMat(string rcno, int tocrop, int fromcrop, string type, string subtype, int company, string bfno, string user)
        {
            //sp_PS_Upd_BroughtForwardMat
     //       @rcno nvarchar(15),
     //      @tocrop int,
     //      @fromcrop int,
     //      @type        nvarchar(5),
		   //@subtype nvarchar(5),
     //      @date datetime,
     //      @company     int,
     //      @bfno            nvarchar(15),
     //      @user nvarchar(50)

            using (var cmd = new SqlCommand("sp_PS_Upd_BroughtForwardMat"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rcno", rcno);
                cmd.Parameters.AddWithValue("@tocrop", tocrop);
                cmd.Parameters.AddWithValue("@fromcrop", fromcrop);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@subtype", subtype);
                cmd.Parameters.AddWithValue("@date", DateTime.Now);
                cmd.Parameters.AddWithValue("@company", company);
                cmd.Parameters.AddWithValue("@bfno", bfno);
                cmd.Parameters.AddWithValue("@user", user);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void UpdateDocnoMat(int tocrop,string rcno,string supplier, string subtype, int company)
        {
            using (var cmd = new SqlCommand("sp_Receiving_UPD_DocNo"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@crop", tocrop);
                cmd.Parameters.AddWithValue("@rcno", rcno);
                cmd.Parameters.AddWithValue("@supplier", supplier);
                cmd.Parameters.AddWithValue("@subtype", subtype);
                cmd.Parameters.AddWithValue("@company", company);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void CheckedLocked(string rcno, string user)
        {
            using (var cmd = new SqlCommand("sp_Receiving_UPD_CheckerLocked"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rcno", rcno);
                cmd.Parameters.AddWithValue("@checker", user);
                cmd.Parameters.AddWithValue("@checkerapproved", true);
                cmd.Parameters.AddWithValue("@checkerlocked", true);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public void FinishReceiving(string rcno)
        {
            using (var cmd = new SqlCommand("sp_Receiving_UPD_RCBFinished"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rcno", rcno);
                cmd.Parameters.AddWithValue("@locked", true);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        public List<MatInfo> GetMatByRCno(string rcno)
        {
            var command = new SqlCommand("sp_Receiving_SEL_MatByRcNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.AddWithValue("@rcno", rcno);
            var data = ExecuteCommand(command, new string[] { "rcno", "crop", "type", "subtype",  "company", "rcfrom", "bc", "supplier", "green", "classify", "docno", "issued" });
            List<MatInfo> lstbf = new List<MatInfo>();
            foreach (var r in data)
            {
                lstbf.Add(new MatInfo()
                {
                    rcno = r["rcno"],
                    crop = Int16.Parse(r["crop"]),
                    type = r["type"],
                    subtype = r["subtype"],
                    company = r["company"],
                    rcfrom = r["rcfrom"],
                    bc = r["bc"],
                    supplier = r["supplier"],
                    green = r["green"],
                    classify = r["classify"],
                    docno = r["docno"],
                    issued = Convert.ToBoolean(r["issued"])
                });
            }
            return lstbf;
        }
        public List<BroughtFWMatInfo> GetBroughtForwardDetails(string bfno)
        {
            var command = new SqlCommand("sp_PS_Get_BroughtForwardDetailByID") { CommandType = CommandType.StoredProcedure };
            command.Parameters.AddWithValue("@bfno", bfno);
            var data = ExecuteCommand(command, new string[] { "bfno","rcno", "fromcrop", "tocrop","bc", "type", "subtype","supplier", "company","green","classify", "weight","weightbuy", "price", "createdate" });
            List<BroughtFWMatInfo> lstbf = new List<BroughtFWMatInfo>();
            foreach (var r in data)
            {
                lstbf.Add(new BroughtFWMatInfo()
                {
                    bfno = r["bfno"],
                    rcno = r["rcno"],
                    fromcrop = Int16.Parse(r["fromcrop"]),
                    tocrop = Int16.Parse(r["tocrop"]),
                    bc = r["bc"],
                    type = r["type"],
                    subtype = r["subtype"],
                    supplier = r["supplier"],
                    company = r["company"],
                    green = r["green"],
                    classify = r["classify"],
                    weight = Convert.ToDouble(r["weight"]),
                    weightbuy = Convert.ToDouble(r["weightbuy"]),
                    price = Convert.ToDouble(r["price"]),
                    createdate = Convert.ToDateTime(r["createdate"])
                });
            }
            return lstbf;
        }


    }
}